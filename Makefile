# The recipes in this file are CI job steps that are easier to debug piecemeal
# in a local environment.
SELECTOR ?= t
LIBRARIES ?= $(sort $(wildcard test/erc-*.el))
EMACS ?= emacs
EMACSDIR ?= /root/.emacs.d
LIBDIRS ?= -L test -L test/resources -L test/resources/erc-d
BASECMD = $(EMACS) -Q -batch $(LIBDIRS) \
		-eval '(package-initialize)' \
		$(addprefix -l ,$(LIBRARIES)) \
		-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

export EMACS_TEST_DIRECTORY := $(realpath test)

ENDPOINT = https://emacs-erc.gitlab.io/bugs
ARCHIVE_ENDPOINT = $(ENDPOINT)/archive
BUG_URL ?= https://example.com

.PHONY: install test update clean clean-git noop prep-git add \
		update-readme do-build-cmd

noop:
	false

install:
	$(EMACS) -batch \
		-eval '(package-refresh-contents)' \
		-eval '(package-install-file default-directory)'

test:
	$(BASECMD)

test-socks: LIBDIRS += -L resources/lisp
test-socks: LIBRARIES = resources/lisp/erc-v3-test-socks.el
test-socks:
	$(BASECMD)

# https://github.com/doomemacs/doomemacs/issues/6494
ifeq ($(SELECTOR),t)
test-doom: SELECTOR = (quote (not (or \
	"\\`erc-fill-.*" \
	"\\`erc-reuse-frames-.*" \
	erc--switch-to-buffer \
	"\\`erc-keep-place-indicator-mode-.*" \
	erc--find-mode \
	erc--check-prompt-input-for-multiline-blanks \
	erc-button-alist--function-as-form \
	erc-scenarios-log--truncate \
	"\\`erc-timestamp-use-align-to--\\(nil\\|t\\)" \
	"\\`erc-scenarios-base-query-participants/.*" \
	erc-button--display-error-notice-with-keys \
	erc-dcc-handle-ctcp-send--turbo \
	erc-fools \
	erc-pals \
	erc-log-matches \
	erc-log-matches/legacy \
	"\\`erc-match-message/.*" \
	"\\`erc-match-functions/.*" \
	"\\`erc-match-types/api.*" \
	erc-scenarios-base-attach--ensure-target-buffer--disabled-query \
	erc-scenarios-base-auto-recon-no-proto \
	erc-scenarios-base-auto-recon-unavailable \
	erc-scenarios-base-reconnect-timer \
	erc-scenarios-base-split-line--ascii \
	erc-scenarios-base-split-line--utf-8 \
	erc-scenarios-join-auth-source--network \
	erc-scenarios-log--truncate/left-stamps \
	erc-scenarios-log--write-after-insert \
	erc-scenarios-v3-labeled-response--echo-message \
	erc-scenarios-v3-labeled-response--self-query \
	erc-scenarios-v3-sasl--unsupported-mechanism \
	erc-scenarios-v3-chathistory--catchup-fallback \
	erc-batch--ensure-info \
	"\\`erc-scenarios-scrolltobottom.*" \
	"\\`pcomplete/erc-mode/DCC--get-.*")))
endif
test-doom: $(EMACSDIR)/.doom-synced
	$(EMACS) -q --no-site-file -batch \
	-load $(EMACSDIR)/early-init \
	-load doom-start \
	-eval '(setenv "ERC_TESTS_INIT" "$(EMACSDIR)/early-init,doom-start")' \
	-eval '(push "$(realpath test)" load-path)' \
	-eval '(push "$(realpath test/resources)" load-path)' \
	-eval '(push "$(realpath test/resources/erc-d)" load-path)' \
	-eval '(push "$(realpath resources/lisp)" load-path)' \
	-eval '(load "$(realpath resources/lisp/erc-v3-test-doom.el)")' \
	-eval '(mapc (function load) (directory-files "test/" t "\\.el"))' \
	-eval '(ert-run-tests-batch-and-exit $(SELECTOR))'

$(EMACSDIR)/.doom-synced: $(EMACSDIR)/.doom-packages-patched \
							$(EMACSDIR)/.doom-config-patched
	$(EMACSDIR)/bin/doom sync
	touch $@

$(EMACSDIR)/.doom-packages-patched: /root/.doom.d/packages.el
	sed -i '3i(package! erc :recipe (:local-repo "erc-edge"))' $<
	touch $@

$(EMACSDIR)/.doom-config-patched: /root/.doom.d/config.el
	sed -i '2i(setq evil-default-state (quote emacs))' $<
	touch $@

prep-git:
	git config --global user.name || git config --global user.name "Your Name"
	git config --global user.email || git config --global user.email "you@example.com"

# Copy clean trunk version of tests over packaged version, which has a few
# modifications, e.g., suppression of compilation errors on older Emacsen.
update:
	test -d archive
	tar --strip-components=1 -xf archive/erc-edge.tar

add:
	git add -u
	git add -- './erc*.el' 'test/*'
	git status

update-readme:
	$(EMACS) -q --no-site-file -batch -l resources/lisp/update-readme.el \
		-f update-readme \
		"resources/readme-stub.org" \
		"resources/patches" \
		"README.org" \
		"$(BUG_URL)/-/raw/master/resources/patches/"

# Override BUILD_CMD.
do-build-cmd: update-readme
	test -f "$(BUG_README)"
	git status --porcelain | grep "^ M README.org"
	@git --no-pager diff
	bash scripts/create_elpa_package.bash make_erc_alpha

# Remove files that archive being untarred is expected to contain.
clean-git:
	rm -f $(shell git ls-files -- './erc*' './dir' './*.texi' './ERC-NEWS')
	rm -rf ./test

clean:
	git restore resources
	rm -vrf logs patches archive tests scripts lisp tests
