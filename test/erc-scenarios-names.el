;;; erc-scenarios-names.el --- erc-names-mode -*- lexical-binding: t -*-

;; Copyright (C) 2025 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-names)

(ert-deftest erc-scenarios-names ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/userhost-in-names")
       (erc-server-flood-penalty 0.1)
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (dumb-server-buffer (get-buffer "*erc-d-server*"))
       (erc-modules `(v3 names ,@erc-modules))
       (erc-autojoin-channels-alist '((ExampleOrg "#chan" "#spam")))
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "password123"
                                :full-name "tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
      (funcall expect 10 "#chan was created on"))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#spam"))
      (funcall expect 10 "#spam was created on")
      (erc-scenarios-common-say "/names"))

    (with-current-buffer "*#spam/NAMES*"
      (funcall expect 1 "mike")
      (funcall expect 1 "joe")
      (funcall expect 1 "SHA")
      (funcall expect 1 "focused-ne")
      (funcall expect 1 "RELAX")
      (funcall expect 1 "gallant")
      (funcall expect 1 "ecstatic")

      ;; Simulate pressing a key over some entry to PM them.
      (let ((inhibit-interaction nil))
        (ert-simulate-keys "Query\r"
          (erc-names--perform-action)))
      (should (get-buffer "ecstatic"))

      (funcall expect 1 "TRUST"))))

;;; erc-scenarios-names.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

