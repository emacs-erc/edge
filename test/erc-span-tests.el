;;; erc-span-tests.el --- Tests for erc-span  -*- lexical-binding:t -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; Code:
(require 'erc-span)

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(ert-deftest erc-span--range-insert ()
  (let ((r1 (make-erc-span--range :ts-lo "01:40"
                                  :ts-hi "03:19"
                                  :marker-hi (make-marker)))
        (r2 (make-erc-span--range :ts-lo "03:20"
                                  :ts-hi "04:59"
                                  :marker-hi (make-marker)))
        (r3 (make-erc-span--range :ts-lo "05:00"
                                  :ts-hi "06:39"
                                  :marker-hi (make-marker)))
        (r4 (make-erc-span--range :ts-lo "06:40")))

    ;; Errors.
    (erc-tests-common-should-fail-assertion
     (erc-span--range-insert ; lower's high is upper's low
      (make-erc-span--range :ts-lo "00:10" :ts-hi "00:20")
      (make-erc-span--range :ts-lo "00:20" :ts-hi "00:29")))

    ;; Insert a lower range.
    (erc-span--range-insert r1 r3)
    (should-not (erc-span--range-prev r1))
    (should-not (erc-span--range-next r3))
    (should (equal (erc-span--range-prev r3) r1))
    (should (equal (erc-span--range-next r1) r3))

    ;; Insert a higher range (also "live" range).
    (erc-span--range-insert (seq-random-elt (list r1 r3)) r4)
    (should-not (erc-span--range-prev r1))
    (should-not (erc-span--range-next r4))
    (should (equal (erc-span--range-prev r3) r1))
    (should (equal (erc-span--range-next r1) r3))
    (should (equal (erc-span--range-next r3) r4))
    (should (equal (erc-span--range-prev r4) r3))

    ;; Insert an intervening range.
    (erc-span--range-insert (seq-random-elt (list r1 r3 r4)) r2)
    (should-not (erc-span--range-prev r1))
    (should-not (erc-span--range-next r4))
    (should (equal (erc-span--range-prev r4) r3))
    (should (equal (erc-span--range-prev r3) r2))
    (should (equal (erc-span--range-prev r2) r1))
    (should (equal (erc-span--range-next r1) r2))
    (should (equal (erc-span--range-next r2) r3))
    (should (equal (erc-span--range-next r3) r4))))

;; The oldest range in the buffer is a singleton containing one message.
;; Its bounds are thus equal.  An attempt to insert a new range above
;; this fails because the new upper is greater is >= the old's lower.
(ert-deftest erc-span--range-insert/overlapping-error ()
  (let ((new (make-erc-span--range :ts-lo "03"
                                   :ts-hi "09"
                                   :marker-hi (make-marker)))

        (r2 (make-erc-span--range :ts-lo "03" :ts-hi "03"
                                  :marker-hi (make-marker)))

        (live (make-erc-span--range :ts-lo "10")))

    ;; Create two non-contiguous history ranges.
    (erc-span--range-insert live r2)
    (should (equal (erc-span--range-prev live) r2))
    (should (equal (erc-span--range-next r2) live))

    ;; "Overlapping ranges: (:left-hi "03" :right-lo "03" :left 3 :right 3)")
    (erc-tests-common-should-fail-assertion
     (erc-span--range-insert live new))))

(ert-deftest erc-span--range-add/distilled ()
  (erc-tests-common-make-server-buf)
  (with-current-buffer (erc--open-target "#chan")
    (erc-span--range-add "1970-01-01T00:00:01.000Z"
                         "1970-01-01T00:00:10.000Z")
    (let ((erc--insert-marker (erc-span--range-marker-hi erc-span--ranges)))
      (erc-display-message nil 'notice (current-buffer)
                           'JOIN ?n "foo" ?u "u" ?h "abc" ?c "#chan"))
    (erc-span--range-add "1970-01-01T00:00:21.000Z"
                         "1970-01-01T00:00:30.000Z")
    (let ((erc--insert-marker (erc-span--range-marker-hi erc-span--ranges)))
      (erc-display-message nil 'notice (current-buffer)
                           'JOIN ?n "bar" ?u "u" ?h "abc" ?c "#chan"))
    (erc-span--range-add "1970-01-01T00:00:41.000Z" nil)
    (let ((erc--insert-marker (erc-span--range-marker-hi erc-span--ranges)))
      (erc-display-message nil 'notice (current-buffer)
                           'JOIN ?n "baz" ?u "u" ?h "abc" ?c "#chan"))
    (erc-span--range-add "1970-01-01T00:00:11.000Z"
                         "1970-01-01T00:00:20.000Z")
    (let* ((prev (erc-span--range-nth-prev erc-span--ranges 2))
           (erc--insert-marker (erc-span--range-marker-hi prev)))
      (erc-display-message nil 'notice (current-buffer)
                           'JOIN ?n "spam" ?u "u" ?h "abc" ?c "#chan"))
    (should (string-match (rx bot "

>>> RANGE-BEG 1970-01-01T00:00:01.000Z
*** foo (u@abc) has joined channel #chan" (+ nonl) "
<<< RANGE-END 1970-01-01T00:00:01.000Z
>>> RANGE-BEG 1970-01-01T00:00:11.000Z
*** spam (u@abc) has joined channel #chan
<<< RANGE-END 1970-01-01T00:00:11.000Z
>>> RANGE-BEG 1970-01-01T00:00:21.000Z
*** bar (u@abc) has joined channel #chan
<<< RANGE-END 1970-01-01T00:00:21.000Z
>>> RANGE-BEG 1970-01-01T00:00:41.000Z
*** baz (u@abc) has joined channel #chan
ERC> " eot)
                          (buffer-substring-no-properties (point-min)
                                                          (point-max))))
    (goto-char (point-min))
    ;; Check that text properties point to the right range node and
    ;; that the node's markers point to the right bookends.
    (let (seen last)
      (while (search-forward ">>> " nil t)
        (let* ((beg (pos-bol))
               (node (get-text-property beg 'erc-span--range))
               (lo (erc-span--range-ts-lo node)))
          (should (= beg (erc-span--range-marker-lo node)))
          (when last
            (should (eq last (erc-span--range-prev node))))
          (setq last node)
          (push lo seen)
          (when (not (string= lo "1970-01-01T00:00:41.000Z"))
            (goto-char (erc-span--range-marker-hi node))
            (should (looking-at (rx-to-string `(: "<<< RANGE-END " ,lo)))))))
      (should (equal seen '("1970-01-01T00:00:41.000Z"
                            "1970-01-01T00:00:21.000Z"
                            "1970-01-01T00:00:11.000Z"
                            "1970-01-01T00:00:01.000Z"))))
    (when noninteractive
      (kill-buffer))))

(ert-deftest erc-span--range-add/regression ()
  (unless (>= emacs-major-version 29) (ert-skip "Needs ticks/hz pairs"))
  (erc-tests-common-make-server-buf)
  (with-current-buffer (erc--open-target "#chan")
    (erc-span--range-add "2023-10-22T07:16:45.815Z")
    (erc-display-message nil 'notice (current-buffer)
                         'JOIN ?n "bar" ?u "u" ?h "abc" ?c "#chan")
    (erc-span--range-add "2023-10-22T07:12:35.000Z" "2023-10-22T07:16:38.000Z")
    (when noninteractive
      (kill-buffer))))

(ert-deftest erc-span--range-find-by-time ()
  (let ((r1 (make-erc-span--range :ts-lo "01:40" :ts-hi "03:19"
                                  :marker-hi (make-marker)))
        (r2 (make-erc-span--range :ts-lo "03:20" :ts-hi "04:59"
                                  :marker-hi (make-marker)))
        (r3 (make-erc-span--range :ts-lo "05:00" :ts-hi "06:39"
                                  :marker-hi (make-marker))))

    (erc-span--range-insert r1 r3)

    ;; With gap
    (should (eq r1 (erc-span--range-find-by-time r3 "03:19")))
    (should-not (eq r1 (erc-span--range-find-by-time r3 "03:20")))
    (should-not (eq r3 (erc-span--range-find-by-time r3 "04:59")))
    (should (eq r3 (erc-span--range-find-by-time r3 "05:00")))

    ;; Without gap
    (erc-span--range-insert r1 r2)

    (should (erc-tests-common-should-fail-assertion
             (erc-span--range-find-by-time r1 "01:39")))

    (should-not (erc-span--range-find-by-time r3 "06:40"))
    (should (eq r3 (erc-span--range-find-by-time r3 "06:39")))
    (should (eq r3 (erc-span--range-find-by-time r3 "06:38")))
    (should (eq r3 (erc-span--range-find-by-time r3 "05:01")))
    (should (eq r3 (erc-span--range-find-by-time r3 "05:00")))

    (should (eq r2 (erc-span--range-find-by-time r3 "04:59")))
    (should (eq r2 (erc-span--range-find-by-time r3 "04:58")))
    (should (eq r2 (erc-span--range-find-by-time r3 "03:21")))
    (should (eq r2 (erc-span--range-find-by-time r3 "03:20")))

    (should (eq r1 (erc-span--range-find-by-time r3 "03:19")))
    (should (eq r1 (erc-span--range-find-by-time r3 "03:18")))
    (should (eq r1 (erc-span--range-find-by-time r3 "01:41")))
    (should (eq r1 (erc-span--range-find-by-time r3 "01:40")))
    (should-not (erc-span--range-find-by-time r3 "01:39"))))

(ert-deftest erc-span--progv ()
  (let ((mytable (map-into '((erc-server-current-nick . nil)
                             (erc-session-username . "bar"))
                           'hash-table)))

    (should (equal (erc-span--progv mytable
                     (setq erc-server-current-nick "FOO")
                     (list erc-server-current-nick erc-session-username))
                   '("FOO" "bar")))
    (should (equal (gethash 'erc-server-current-nick mytable) "FOO"))
    (should (equal (gethash 'erc-session-username mytable) "bar"))
    (should-not erc-session-username)
    (should-not erc-server-current-nick)))

(ert-deftest erc-span--with-locals ()
  (erc-tests-common-make-server-buf)
  (setq erc-server-current-nick "s-nick")
  (erc-add-server-user "s1" (make-erc-server-user :nickname "s1"))

  (let* ((my-alist ())
         (phony-server-users
          (map-into `(("c1" . ,(make-erc-server-user :nickname "c1")))
                    '(hash-table :test equal)))
         (my-table (map-into `((erc-server-users . ,phony-server-users)
                               (erc-server-current-nick . "c-nick"))
                             'hash-table)))

    (with-current-buffer (erc--open-target "#chan")
      (push (cons (erc-server-buffer) my-table) my-alist)
      (erc-span--with-locals my-alist ((erc-server-buffer))
        (should-not (erc-get-server-user "s1"))
        (should (erc-get-server-user "c1"))
        (should (equal (erc-current-nick) "c-nick"))
        (erc-add-server-user "a" (make-erc-server-user :nickname "a"))))

    (should-not (erc-get-server-user "c1"))
    (should-not (erc-get-server-user "a"))
    (should (equal erc-server-current-nick "s-nick"))
    (erc-add-server-user "s2" (make-erc-server-user :nickname "s2"))

    (with-current-buffer "#chan"
      (erc-span--with-locals my-alist ((erc-server-buffer))
        (should-not (erc-get-server-user "s1"))
        (should-not (erc-get-server-user "s2"))
        (should (erc-get-server-user "c1"))
        (should (equal (erc-current-nick) "c-nick"))
        (erc-add-server-user "c2" (make-erc-server-user :nickname "c2"))))

    (kill-buffer "#chan")))

;;; erc-span-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

