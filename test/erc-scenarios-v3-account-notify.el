;;; erc-scenarios-v3-account-notify.el --- account notify scenarios -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO add dedicated test case for `account-tag', perhaps in a
;; separate file.

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-scenarios-common)))

(require 'erc-scenarios-common)
(require 'erc-v3)

(defvar erc-sasl-mechanism)
(defvar erc-sasl-password)
(defvar erc-sasl-user)

(defun erc-scenarios-common--v3-account-notify ()
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/account-notify")
       (erc-d-linger-secs 0.5)
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        (cons `(now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))
              erc-d-tmpl-vars))
       (dumb-server (erc-d-run "localhost" t 'basic))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions `(sasl server-time batch echo-message
                                 ,@erc-v3-extensions))
       (erc-sasl-mechanism 'plain)
       (erc-sasl-user "iamtester")
       (erc-sasl-password "password123")
       (inhibit-message noninteractive)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :password "changeme"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (erc-d-t-wait-for 20 "server buffer ready" (get-buffer "foonet"))
    (with-current-buffer "foonet"
      (ert-info ("Join chan #chan")
        (funcall expect 5 "logged in as tester")
        (erc-cmd-JOIN "#chan")))

    (erc-d-t-wait-for 20 "#chan created" (get-buffer "#chan"))
    (with-current-buffer "#chan"
      (ert-with-message-capture messages
        (let ((inhibit-message t))
          (funcall expect 5 "You have joined channel #chan")
          (funcall expect 5 "fool (~u@8ch3ur22f9p4q.irc) has joined")
          (erc-scenarios-common--nick-at-point (match-beginning 0))
          (funcall expect 5 "dummy (~u@3pu3he22s9c4d.irc) has joined")
          (erc-scenarios-common--nick-at-point (match-beginning 0))
          (funcall expect 5 "<fool")
          (erc-scenarios-common--nick-at-point (1+ (match-beginning 0))))
        (funcall expect 5 "<alice> bob: Why do they run away ?")
        messages))))

;; This is slightly unrealistic because most servers send account=foo
;; regardless of whether `account-tag' has been negotiated, so long as
;; `message-tags' has.

(ert-deftest erc-scenarios-v3-account-notify--basic ()
  :tags '(:expensive-test)
  (let* ((erc-d-tmpl-vars '((a-cap . "")
                            (a-bob . "")
                            (a-dummy . "")
                            (a-alice . "")
                            (a-fool . "")
                            (a-iamtester . "")
                            (last . " sasl")
                            (rest . "server-time setname userhost-in-names")))
         (erc-v3-extensions nil)
         (messages (erc-scenarios-common--v3-account-notify))
         (expect (erc-d-t-make-expecter)))
    (with-temp-buffer
      (insert messages)
      (goto-char (point-min))
      (funcall expect 5 "*: ~u@8ch3ur22f9p4q.irc \"fool\"")
      (funcall expect 5 "*: ~u@3pu3he22s9c4d.irc \"dummy\"")
      (funcall expect 5 "fool: ~u@8ch3ur22f9p4q.irc \"fool\""))))

(ert-deftest erc-scenarios-v3-account-notify--account-tag ()
  :tags '(:expensive-test)
  (let* ((erc-d-tmpl-vars
          '((a-cap . " account-tag")
            (a-bob . ";account=bob")
            (a-dummy . ";account=dummy")
            (a-alice . ";account=alice")
            (a-fool . ";account=fool")
            (a-iamtester . ";account=iamtester")
            (last . "")
            (rest . "sasl server-time setname userhost-in-names")))
         (erc-v3-extensions '(account-tag))
         (messages (erc-scenarios-common--v3-account-notify))
         (expect (erc-d-t-make-expecter)))
    (with-temp-buffer
      (insert messages)
      (goto-char (point-min))
      (funcall expect 5 "*: ~u@8ch3ur22f9p4q.irc \"fool\"")
      (funcall expect 5 "dummy: ~u@3pu3he22s9c4d.irc \"dummy\"")
      (funcall expect 5 "fool: ~u@8ch3ur22f9p4q.irc \"fool\""))))

;; This also happens to test the `chghost' extension.
(ert-deftest erc-scenarios-v3-account-notify--join-quit ()
  :tags '(:expensive-test)
  (erc-scenarios-common-with-cleanup
      ((erc-scenarios-common-dialog "v3/account-notify")
       (erc-server-flood-penalty 0.1)
       (erc-d-tmpl-vars
        (cons `(now . ,(lambda () (format-time-string "%FT%T.%3NZ" nil t)))
              erc-d-tmpl-vars))
       (dumb-server (erc-d-run "localhost" t 'join-quit))
       (port (process-contact dumb-server :service))
       (erc-modules (cons 'v3 erc-modules))
       (erc-v3-extensions (remq 'account-tag erc-v3-extensions))
       (inhibit-message noninteractive)
       (expect (erc-d-t-make-expecter)))

    (ert-info ("Connect")
      (with-current-buffer (erc :server "127.0.0.1"
                                :port port
                                :nick "tester"
                                :full-name "Tester")
        (should (string= (buffer-name) (format "127.0.0.1:%d" port)))))

    (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "foonet"))
      (ert-info ("Join #chan")
        (funcall expect 5 "debug mode")
        (erc-cmd-JOIN "#chan")))

    (with-current-buffer (erc-d-t-wait-for 10 (get-buffer "#chan"))
      (funcall expect 5 "You have joined channel #chan")
      (funcall expect 5 "hacker (~u@xv3y4e51g8y0r.irc) has joined")
      (funcall expect 5 "fool (~u@8ch3ur22f9p4q.irc) has joined")
      (funcall expect 5 "dummy (~u@3pu3he22s9c4d.irc) has joined")
      (funcall expect 5 "<hacker> huh?")
      (ert-info ("Host and login successfully changed")
        (ert-with-message-capture messages
          (let ((inhibit-message t))
            (erc-scenarios-common--nick-at-point (1+ (match-beginning 0)))
            (should (string-search "jph: ~jph@gnu.org \"hacker\"" messages)))))
      (funcall expect 5 "<fool> hi")
      (funcall expect 5 "<alice> bob: Why do they run away ?"))))

;;; erc-scenarios-v3-account-notify.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

