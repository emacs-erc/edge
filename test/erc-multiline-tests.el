;;; erc-multiline-tests.el --- IRCv3 multiline -*- lexical-binding: t -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see
;; <https://www.gnu.org/licenses/>.

;;; Code:
(require 'erc-multiline)
(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(ert-deftest erc-v3--multiline-mode-map ()
  (should (keymapp (alist-get 'erc-v3--multiline minor-mode-map-alist))))

(ert-deftest erc-multiline--forge-response ()
  (let* ((parsed-opener (make-erc-response
                         :unparsed ":foonet BATCH +1 chathistory #chan"
                         :command "BATCH"
                         :command-args '("+1" "chathistory" "#chan")
                         :sender "foonet"
                         :contents "#chan")) ; nreversed
         (info (make-erc-multiline--batch
                :type 'chathistory
                :name "test"
                :ref "1"
                :first (make-erc-response
                        :unparsed ":bob PRIVMSG #chan :Hi."
                        :command "PRIVMSG"
                        :command-args '("#chan" "Hi")
                        :sender "bob"
                        :contents "Hi")
                :parts (list "\303\241" "\n" "b" "\n" "\303\247")))
         (resp (erc-multiline--forge-response parsed-opener info))
         (text (pop resp)))

    (ert-info ("Decodes properly and adds a leading newline")
      (should (equal text "\nç\nb\ná")))

    (ert-info ("Doesn't mutate `erc-multiline--message-bookends'")
      (should-not (text-property-any 0 (length "#+BEGIN_MULTILINE")
                                     'erc-multiline 'begin
                                     "#+BEGIN_MULTILINE")))

    (ert-info ("Borrows fields from `first' slot of batch info.")
      (should (equal (erc-response.unparsed resp) ":bob PRIVMSG #chan :Hi."))
      (should (equal (erc-response.sender resp) "bob"))
      (should (equal (erc-response.command resp) "PRIVMSG"))

      (unless (< emacs-major-version 29)
        (should (equal-including-properties
                 (erc-response.command-args resp)
                 '("#chan" #("#+BEGIN_MULTILINE#+END_MULTILINE"
                             0 17 (erc-multiline begin)
                             17 32 (erc-multiline end)))))
        (should (equal-including-properties
                 (erc-response.contents resp)
                 #("#+BEGIN_MULTILINE#+END_MULTILINE"
                   0 17 (erc-multiline begin)
                   17 32 (erc-multiline end))))))))

(ert-deftest erc--send-input-lines ()
  (erc-tests-common-make-server-buf)
  (setq erc-v3--caps (make-erc-v3--caps)
        erc-server-current-nick "tester")
  (let* ((inhibit-read-only t)
         ;; Normally buffer-local and shared among all buffers of a connection.
         (erc-v3--batch (erc-v3--batch))
         (erc--allow-empty-outgoing-lines-p t)
         (var (erc-v3--multiline :key 'draft/multiline)))
    (with-current-buffer (erc--open-target "#chan")
      (setq erc-v3--multiline var)
      (let* ((erc-multiline--send-p t)
             ;;
             calls
             (erc-send-input-line-function
              (lambda (&rest r)
                (push `(input ,erc-v3--unescaped-client-tags ,@(butlast r))
                      calls))))
        (cl-letf (((symbol-function 'erc-server-send)
                   (lambda (&rest r) (push (cons 'send (butlast r 2)) calls))))

          (erc--send-input-lines (make-erc--input-split
                                  :string "abc\ndef"
                                  :sendp t
                                  :lines (list "abc" "def")))

          (should (equal calls '((send "BATCH -1")
                                 (input ((batch . "1")) "#chan" "def")
                                 (input ((batch . "1")) "#chan" "abc")
                                 (send "BATCH +1 draft/multiline #chan"))))

          (ert-info ("Sends blank lines as empty messages") ; doesn't pad
            (setq calls nil)
            (erc--send-input-lines (make-erc--input-split
                                    :string "abc\n\ndef\n"
                                    :sendp t
                                    :lines (list "abc" "" "def" "")))
            (should (equal calls
                           '((send "BATCH -2")
                             (input ((batch . "2")) "#chan" "")
                             (input ((batch . "2")) "#chan" "def")
                             (input ((batch . "2")) "#chan" "")
                             (input ((batch . "2")) "#chan" "abc")
                             (send "BATCH +2 draft/multiline #chan")))))

          (ert-info ("Handles continuations properly")
            (setq calls nil)
            (erc--send-input-lines (make-erc--input-split
                                    :string "abc\n\ndef\n"
                                    :sendp t
                                    :lines (list "ab" "c" "" "d" "ef" "")))

            (should (equal calls
                           '((send "BATCH -3")
                             (input ((batch . "3")) "#chan" "")
                             (input ((batch . "3") (draft/multiline-concat))
                                    "#chan" "ef")
                             (input ((batch . "3")) "#chan" "d")
                             (input ((batch . "3")) "#chan" "")
                             (input ((batch . "3") (draft/multiline-concat))
                                    "#chan" "c")
                             (input ((batch . "3")) "#chan" "ab")
                             (send "BATCH +3 draft/multiline #chan")))))))
      (when noninteractive (kill-buffer)))))

(defun erc-multiline-tests--send-current-line (test)
  (when (bound-and-true-p erc-noncommands-mode)
    (erc-noncommands-mode -1))
  (when (bound-and-true-p erc-ring-mode)
    (erc-ring-mode -1))
  (should (equal erc--input-review-functions
                 '(erc--split-lines
                   erc--run-input-validation-checks
                   erc--discard-trailing-multiline-nulls
                   erc--inhibit-slash-cmd-insertion)))
  (erc-mode)
  (erc--initialize-markers (point) nil)
  (setq erc-server-process (start-process "sleep"
                                          (current-buffer) "sleep" "1")
        erc-server-current-nick "tester"
        erc-server-users (map-into `(("tester" . ,(make-erc-server-user
                                                   :nickname "tester"
                                                   :host "localhost"
                                                   :login "~u"
                                                   :full-name "Tester")))
                                   '(hash-table :test equal))
        erc-v3--caps (make-erc-v3--caps)
        erc-v3--multiline (erc-v3--multiline :max-bytes 128 :max-lines 10))
  (set-process-query-on-exit-flag erc-server-process nil)

  (let ((proc erc-server-process)
        (obj erc-v3--multiline)
        (orig-review-fns (default-toplevel-value 'erc--input-review-functions))
        erc-accidental-paste-threshold-seconds
        erc-insert-modify-hook
        erc-send-completed-hook
        calls)
    (with-temp-buffer
      (erc-mode)
      (erc--initialize-markers (point) nil)
      (setq erc-server-process proc
            erc--target (erc--target-from-string "tester")
            erc-v3--multiline obj)
      (cl-letf (((symbol-function 'erc-my-foo)
                 (lambda (&rest _)
                   (push (cons 'ERC-MY-FOO erc--input-review-functions)
                         calls)))
                ((symbol-function 'erc--send-input-lines)
                 (lambda (&rest r)
                   (push (cons 'ERC--SEND-INPUT-LINES r) calls)))
                ((symbol-function 'erc-send-input-line)
                 (lambda (&rest r)
                   (push (cons 'ERC-SEND-INPUT-LINE r) calls))))
        (set-default-toplevel-value 'erc--input-review-functions
                                    (cons 'erc-my-foo orig-review-fns))
        (unwind-protect
            (funcall test (lambda () (pop calls)))
          (set-default-toplevel-value 'erc--input-review-functions
                                      orig-review-fns))))))

(ert-deftest erc-multiline--send-current-line/multi ()
  (erc-multiline-tests--send-current-line
   (lambda (next)
     (goto-char erc-input-marker)
     (insert "test\n123")
     (ert-simulate-keys "y\r"
       (erc-multiline--send-current-line))
     (should (string-empty-p (erc-user-input)))
     (should (equal (funcall next)
                    '(ERC--SEND-INPUT-LINES
                      #s(erc--input-split "test\n123" t t nil nil
                                          ("test" "123") nil nil))))
     (should-not (funcall next)))))

(ert-deftest erc-multiline--send-current-line/no-multi ()
  (erc-multiline-tests--send-current-line
   (lambda (next)
     (goto-char erc-input-marker)
     (insert "test")
     (erc-multiline--send-current-line)
     (should (string-empty-p (erc-user-input)))
     (should (equal (funcall next)
                    '(ERC--SEND-INPUT-LINES
                      #s(erc--input-split "test" t t nil nil
                                          ("test") nil nil))))
     (should (equal (funcall next)
                    '(ERC-MY-FOO
                      erc-my-foo
                      erc--split-lines
                      erc--run-input-validation-checks
                      erc--discard-trailing-multiline-nulls
                      erc--inhibit-slash-cmd-insertion)))
     (should-not (funcall next)))))

(ert-deftest erc-multiline--send-current-line/no-multi-local-hook ()
  (let* ((local-calls nil)
         ;; See `erc-v3--echo-message-on-pre-send'.
         (my-local-hook (lambda (&rest r) (push r local-calls))))
    (erc-multiline-tests--send-current-line
     (lambda (next)
       (add-hook 'erc--input-review-functions my-local-hook 90 t)
       (goto-char erc-input-marker)
       (insert "test")
       (erc-multiline--send-current-line)
       (should (string-empty-p (erc-user-input)))
       (should (equal (funcall next)
                      '(ERC--SEND-INPUT-LINES
                        #s(erc--input-split "test" t t nil nil
                                            ("test") nil nil))))
       ;; Global top-level default hooks still run.
       (should (equal (funcall next) `(ERC-MY-FOO t ,my-local-hook)))
       (should-not (funcall next))
       ;; Buffer-local hook runs.
       (should (equal local-calls
                      '((#s(erc--input-split "test" t t nil nil
                                             ("test") nil nil)))))))))

(ert-deftest erc-multiline--send-current-line/multi-local-hook ()
  (let* ((local-calls nil)
         (my-local-hook (lambda (&rest r) (push r local-calls))))
    (erc-multiline-tests--send-current-line
     (lambda (next)
       (add-hook 'erc--input-review-functions my-local-hook 90 t)
       (goto-char erc-input-marker)
       (insert "test\n123")
       (ert-simulate-keys "y\r"
         (erc-multiline--send-current-line))
       (should (string-empty-p (erc-user-input)))
       (should (equal (funcall next)
                      '(ERC--SEND-INPUT-LINES
                        #s(erc--input-split "test\n123" t t nil nil
                                            ("test" "123") nil nil))))

       (should-not (funcall next))
       (should (equal local-calls
                      '((#s(erc--input-split "test\n123" t t nil nil
                                             ("test" "123") nil nil)))))))))

;;; erc-multiline-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

