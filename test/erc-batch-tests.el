;;; erc-batch-tests.el --- Tests for erc-batch.  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2022 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'erc-batch)

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(ert-deftest erc-batch--enqueue ()
  (let ((info (make-erc-batch--info :type nil :name "" :ref ""
                                    :queue (current-buffer))))
    (erc-batch--queue-mode)
    (erc-batch--enqueue info '((a) (time . "1") (b . "z"))
                        "@a,time=1,b=z :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((time . "2") (a . "x"))
                        "@time=2,a=x :alice!~u@user PRIVMSG #chan :two")
    (erc-batch--enqueue info '((time . "3") (c . "y"))
                        "@time=3,c=y :alice!~u@user PRIVMSG #chan :three")
    (should (erc-tests-common-equal-with-props
             #("\
@a,time=1,b=z :bob!~u@oper PRIVMSG #chan :one\r
@time=2,a=x :alice!~u@user PRIVMSG #chan :two\r
@time=3,c=y :alice!~u@user PRIVMSG #chan :three\r
" 45 47 (ts "1" length 45 tags ((a) (time . "1") (b . "z")))
92 94 (ts"2" length 45 tags ((time . "2") (a . "x")))
141 143 (ts "3" length 47 tags ((time . "3") (c . "y"))))
             (buffer-string)))
    (should (= 3 (erc-batch--info-length info)))))

(ert-deftest erc-batch--enqueue/reorder ()
  (let ((info (make-erc-batch--info :type nil :name "" :ref ""
                                    :queue (current-buffer))))
    (erc-batch--queue-mode)
    (erc-batch--enqueue info '((time . "3") (c . "y"))
                        "@time=3,c=y :alice!~u@user PRIVMSG #chan :three")
    (erc-batch--enqueue info '((a) (time . "1") (b . "z"))
                        "@a,time=1,b=z :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((time . "2") (a . "x"))
                        "@time=2,a=x :alice!~u@user PRIVMSG #chan :two")
    (should (erc-tests-common-equal-with-props
             #("\
@a,time=1,b=z :bob!~u@oper PRIVMSG #chan :one\r
@time=2,a=x :alice!~u@user PRIVMSG #chan :two\r
@time=3,c=y :alice!~u@user PRIVMSG #chan :three\r
" 45 47 (ts "1" length 45 tags ((a) (time . "1") (b . "z")))
92 94 (ts"2" length 45 tags ((time . "2") (a . "x")))
141 143 (ts "3" length 47 tags ((time . "3") (c . "y"))))
             (buffer-string)))
    (should (= 3 (erc-batch--info-length info)))))

(ert-deftest erc-batch--remove ()
  (let* ((buf (get-buffer-create "#chan"))
         (info (make-erc-batch--info :type nil :name "" :ref "" :queue buf)))
    (with-current-buffer "#chan" (set-buffer-multibyte nil))
    (erc-batch--enqueue info '((a) (b . "c"))
                        "@a,b=c :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((a . "x"))
                        "@a=x :alice!~u@user PRIVMSG #chan :two")
    (should (equal (erc-batch--remove info)
                   '(((a) (b . "c"))
                     . "@a,b=c :bob!~u@oper PRIVMSG #chan :one")))
    (should (equal (erc-batch--remove info)
                   '(((a . "x"))
                     . "@a=x :alice!~u@user PRIVMSG #chan :two")))
    (should (string-empty-p (with-current-buffer "#chan" (buffer-string))))
    (should (zerop (erc-batch--info-length info)))
    (should-not (erc-batch--remove info))
    (with-current-buffer "#chan"
      (widen)
      (should (equal (buffer-substring-no-properties (point-min) (point-max))
                     (concat "@a,b=c :bob!~u@oper PRIVMSG #chan :one\r\n"
                             "@a=x :alice!~u@user PRIVMSG #chan :two\r\n"))))
    (kill-buffer "#chan")))

(ert-deftest erc-batch--remove-last ()
  (let* ((buf (get-buffer-create "#chan"))
         (info (make-erc-batch--info :type nil :name "" :ref "" :queue buf)))
    (with-current-buffer "#chan" (set-buffer-multibyte nil))
    (erc-batch--enqueue info '((a) (b . "c"))
                        "@a,b=c :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((a . "x"))
                        "@a=x :alice!~u@user PRIVMSG #chan :two")
    (should (equal (erc-batch--remove-last info)
                   '(((a . "x"))
                     . "@a=x :alice!~u@user PRIVMSG #chan :two")))
    (should (equal (erc-batch--remove-last info)
                   '(((a) (b . "c"))
                     . "@a,b=c :bob!~u@oper PRIVMSG #chan :one")))
    (should (string-empty-p (with-current-buffer "#chan" (buffer-string))))
    (should (zerop (erc-batch--info-length info)))
    (should-not (erc-batch--remove-last info))
    (kill-buffer "#chan")))

(ert-deftest erc-batch--peek-last ()
  (let* ((buf (get-buffer-create "#chan"))
         (info (make-erc-batch--info :type nil :name "" :ref "" :queue buf)))
    (with-current-buffer "#chan" (set-buffer-multibyte nil))
    (erc-batch--enqueue info '((a) (b . "c"))
                        "@a,b=c :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((a . "x"))
                        "@a=x :alice!~u@user PRIVMSG #chan :two")
    (should (equal (erc-batch--peek-last info)
                   '(((a . "x"))
                     . "@a=x :alice!~u@user PRIVMSG #chan :two")))
    (should (erc-batch--remove info))
    (should (equal (erc-batch--peek-last info)
                   '(((a . "x"))
                     . "@a=x :alice!~u@user PRIVMSG #chan :two")))
    (should (erc-batch--remove info))
    (should-not (erc-batch--peek-last info))
    (should (string-empty-p (with-current-buffer "#chan" (buffer-string))))
    (should (zerop (erc-batch--info-length info)))
    (kill-buffer "#chan")))

(ert-deftest erc-batch--peek ()
  (let* ((buf (get-buffer-create "#chan"))
         (info (make-erc-batch--info :type nil :name "" :ref "" :queue buf)))
    (with-current-buffer "#chan" (set-buffer-multibyte nil))
    (erc-batch--enqueue info '((a) (b . "c"))
                        "@a,b=c :bob!~u@oper PRIVMSG #chan :one")
    (erc-batch--enqueue info '((a . "x"))
                        "@a=x :alice!~u@user PRIVMSG #chan :two")
    (should (equal (erc-batch--peek info)
                   '(((a) (b . "c"))
                     . "@a,b=c :bob!~u@oper PRIVMSG #chan :one")))
    (should (erc-batch--remove info))
    (should (equal (erc-batch--peek info)
                   '(((a . "x"))
                     . "@a=x :alice!~u@user PRIVMSG #chan :two")))
    (should (erc-batch--remove info))
    (should-not (erc-batch--peek info))
    (should (string-empty-p (with-current-buffer "#chan" (buffer-string))))
    (should (zerop (erc-batch--info-length info)))
    (kill-buffer "#chan")))

(ert-deftest erc-batch--count-oldest ()
  (let* ((buf (with-current-buffer (get-buffer-create "#chan")
                (erc-batch--queue-mode)
                (current-buffer)))
         (info (make-erc-batch--info :name "" :type '? :ref "" :queue buf)))
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :bob!~b@user PRIVMSG #chan :1")
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :alice!~a@user PRIVMSG #chan :1")
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :alice!~a@user PRIVMSG #chan :1")
    (erc-batch--enqueue info '((time . "2"))
                        "@time=2 :bob!~b@user PRIVMSG #chan :2")
    (erc-batch--enqueue info '((time . "3"))
                        "@time=3 :bob!~b@user PRIVMSG #chan :3")
    (should (= 3 (erc-batch--count-oldest info)))
    (should (erc-batch--remove info))
    (should (= 2 (erc-batch--count-oldest info)))
    (should (erc-batch--remove info))
    (should (= 1 (erc-batch--count-oldest info)))
    (should (erc-batch--remove info))
    (should (= 1 (erc-batch--count-oldest info)))
    (should (erc-batch--remove info))
    (should (= 1 (erc-batch--count-oldest info)))
    (should (erc-batch--remove info))
    (should-not (erc-batch--count-oldest info))
    (should-not (erc-batch--remove info))
    (kill-buffer "#chan")))

(ert-deftest erc-batch--visit-stamp-cohorts ()
  (let* ((buf (with-current-buffer (get-buffer-create "#chan")
                (erc-batch--queue-mode)
                (current-buffer)))
         (info (make-erc-batch--info :type nil :name "" :ref "" :queue buf))
         result
         (fn (lambda (tags beg end)
               (push (cons (buffer-substring beg end) tags) result))))
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :bob!~b@user PRIVMSG #chan :1a")
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :alice!~a@user PRIVMSG #chan :1b")
    (erc-batch--enqueue info '((time . "1"))
                        "@time=1 :alice!~a@user PRIVMSG #chan :1c")
    (erc-batch--enqueue info '((time . "2"))
                        "@time=2 :bob!~b@user PRIVMSG #chan :2")
    (erc-batch--enqueue info '((time . "3"))
                        "@time=3 :bob!~b@user PRIVMSG #chan :3")

    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should (equal '(("@time=1 :alice!~a@user PRIVMSG #chan :1c" (time . "1"))
                     ("@time=1 :alice!~a@user PRIVMSG #chan :1b" (time . "1"))
                     ("@time=1 :bob!~b@user PRIVMSG #chan :1a" (time . "1")))
                   result))
    (setq result nil)

    (should (erc-batch--remove info))
    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should (equal '(("@time=1 :alice!~a@user PRIVMSG #chan :1c" (time . "1"))
                     ("@time=1 :alice!~a@user PRIVMSG #chan :1b" (time . "1")))
                   result))
    (setq result nil)

    (should (erc-batch--remove info))
    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should (equal '(("@time=1 :alice!~a@user PRIVMSG #chan :1c" (time . "1")))
                   result))
    (setq result nil)

    (should (erc-batch--remove info))
    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should (equal '(("@time=2 :bob!~b@user PRIVMSG #chan :2" (time . "2")))
                   result))
    (setq result nil)

    (should (erc-batch--remove info))
    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should (equal '(("@time=3 :bob!~b@user PRIVMSG #chan :3" (time . "3")))
                   result))
    (setq result nil)

    (should (erc-batch--remove info))
    (with-current-buffer "#chan" (erc-batch--visit-stamp-cohorts fn))
    (should-not result)

    (should-not (erc-batch--remove info))

    (kill-buffer "#chan")))

(ert-deftest erc-batch--sequester ()
  (let ((r1 (make-erc-span--range
             :ts-lo "2020-01-01T00:00:00.000Z"
             :ts-hi "2020-01-31T23:59:59.999Z"
             :marker-hi (make-marker)))
        (r3 (make-erc-span--range
             :ts-lo "2020-03-01T00:00:00.000Z"
             :ts-hi nil))
        (info (make-erc-batch--chathistory :type nil :ref "" :name "#c"
                                           :parsed (make-erc-response))))
    (erc-batch--create-queue info)
    (erc-span--range-insert r1 r3)
    (setq erc-span--ranges r3)
    ;; Below lower bound
    (erc-batch--enqueue
     info '((time . "2020-01-20T00:00:00.000Z"))
     "@time=2020-01-20T00:00:00.000Z :bob PRIVMSG #c :1/20/2020")
    (erc-batch--enqueue
     info '((time . "2020-01-31T23:59:59.999Z"))
     "@time=2020-01-31T23:59:59.999Z :bob PRIVMSG #c :1/31/2020")
    ;; Within span bounds
    (erc-batch--enqueue
     info '((time . "2020-02-01T00:00:00.000Z"))
     "@time=2020-02-01T00:00:00.000Z :bob PRIVMSG #c :2/01/2020")
    (erc-batch--enqueue
     info '((time . "2020-02-29T23:59:59.999Z"))
     "@time=2020-02-29T23:59:59.999Z :bob PRIVMSG #c :2/29/2020")
    ;; Above upper bound
    (erc-batch--enqueue
     info '((time . "2020-03-02T00:00:00.000Z"))
     "@time=2020-03-02T00:00:00.000Z :bob PRIVMSG #c :3/02/2020")

    (erc-batch--sequester-lower info)
    (should
     (equal
      (erc-batch--chathistory-below-lower info)
      '((((time . "2020-01-31T23:59:59.999Z"))
         . "@time=2020-01-31T23:59:59.999Z :bob PRIVMSG #c :1/31/2020")
        (((time . "2020-01-20T00:00:00.000Z"))
         . "@time=2020-01-20T00:00:00.000Z :bob PRIVMSG #c :1/20/2020"))))
    (should
     (equal (erc-batch--peek info)
            '(((time . "2020-02-01T00:00:00.000Z"))
              . "@time=2020-02-01T00:00:00.000Z :bob PRIVMSG #c :2/01/2020")))

    (erc-batch--sequester-upper info)
    (should
     (equal
      (erc-batch--chathistory-above-upper info)
      '((((time . "2020-03-02T00:00:00.000Z"))
         . "@time=2020-03-02T00:00:00.000Z :bob PRIVMSG #c :3/02/2020"))))
    (should
     (equal (erc-batch--peek-last info)
            '(((time . "2020-02-29T23:59:59.999Z"))
              . "@time=2020-02-29T23:59:59.999Z :bob PRIVMSG #c :2/29/2020")))

    (kill-buffer (erc-batch--info-queue info))))

(ert-deftest erc-batch--ensure-info ()
  (erc-tests-common-make-server-buf)
  (setq erc-server-current-nick "tester"
        erc-v3--batch (erc-v3--batch))

  (ert-info ("Error on unknown type")
    (cl-letf (((symbol-function 'erc-display-message) #'ignore))
      (let ((err (should-error
                  (erc-batch--ensure-info
                   (make-erc-response :command "BATCH"
                                      :command-args '("+baz" "nonsense"))))))
        (should (string-match "unknown" (cadr err)))))
    (should-not (erc-v3--batch-es erc-v3--batch)))

  (ert-info ("Initialize object for new channel")
    (with-current-buffer (erc--open-target "#dummy"))
    (let* ((parsed (make-erc-response
                    :command "BATCH"
                    :command-args '("+dummy" "chathistory" "#dummy")))
           (rv (erc-batch--ensure-info parsed)))
      (should (equal rv (make-erc-batch--chathistory
                         :type 'chathistory
                         :ref "dummy"
                         :name "#dummy"
                         :queue (erc-batch--info-queue rv)
                         :parsed parsed
                         :locals (erc-batch--chathistory-locals rv))))
      (should (equal (erc-v3--batch-es erc-v3--batch) (list rv)))))

  ;; Called by `erc-batch--handle-nested-batch-start'.
  (ert-info ("Initialize object for new inner batch bar")
    (erc-batch--ensure-info (make-erc-response
                             :command "BATCH"
                             :command-args '("+inner" "chathistory" "#dummy")))
    (should (= (length (erc-v3--batch-es erc-v3--batch)) 2)))

  (ert-info ("Transition and return existing batch foo")
    (let ((rv (erc-batch--ensure-info (make-erc-response
                                       :command "BATCH"
                                       :command-args '("-dummy")))))
      (should (eq (erc-batch--info-state rv) '-))))

  (ert-info ("Initialize object for nonexistent query")
    (should-not (get-buffer "bob"))
    (let ((rv (erc-batch--ensure-info
               (make-erc-response
                :command "BATCH"
                :command-args '("+query" "chathistory" "bob")))))
      ;; Buffer not created until handle-enqueued stage.
      (should-not (get-buffer "bob"))
      (should (equal (car (erc-v3--batch-es erc-v3--batch)) rv))))

  (should (= (length (erc-v3--batch-es erc-v3--batch)) 3))

  ;; Kill queue buffers as well as `erc-mode' targets.
  (let ((bufs (match-buffers '(major-mode . erc-batch--queue-mode))))
    (should (= (length bufs) 3))
    (apply #'erc-tests-common-kill-buffers bufs))

  (should-not erc-v3--batch))

;; Also tests `erc--parse-message-tags' method
(ert-deftest erc-parse-server-response--erc-v3--batch ()
  (let* ((erc-v3--batch (erc-v3--batch))
         (work-buf (get-buffer-create " *#chan*"))
         (info (make-erc-batch--info :queue work-buf
                                     :ref "1"
                                     :type nil
                                     :name ""
                                     :state '+))
         (erc-v3--batch (erc-v3--batch :es (list info)))
         erc-batch--parsed-tags
         erc-active-buffer
         (raw (concat "@time=2020-11-23T09:10:33.088Z;batch=1"
                      " :bob!~bob@abc-172-0-64-90.def.example.org"
                      " PRIVMSG #chan :hi"))
         (erc-v3-mode t)
         calls)
    (cl-letf (((symbol-function #'erc-handle-parsed-server-response)
               (lambda (_ p) (push p calls))))

      (ert-info ("Parsing operation intercepted")
        (should (erc-response-p (erc-parse-server-response nil raw)))
        (should-not calls)
        (should (= 1 (erc-batch--info-length info))))

      (ert-info ("Payload intact")
        (let ((job (erc-batch--remove info)))
          (should (equal (car job) '((time . "2020-11-23T09:10:33.088Z")
                                     (batch . "1"))))
          (should (equal (cdr job) raw))
          (setq erc-batch--parsed-tags (car job))))

      (ert-info ("Dispatch skipped when parsed-tags non-nil")
        (let* ((fake-raw "@foo=bar :bob PRIVMSG #chan :hi")
               (rv (erc-parse-server-response nil fake-raw)))
          (should (eq rv (car calls)))
          (should (equal calls
                         (list (make-erc-response
                                :unparsed fake-raw
                                :tags '((time . "2020-11-23T09:10:33.088Z")
                                        (batch . "1"))
                                :sender "bob"
                                :command-args '("#chan" "hi")
                                :command "PRIVMSG"
                                :contents "hi")))))))
    (kill-buffer work-buf)))

;;; erc-batch-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

