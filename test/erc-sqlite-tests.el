;;; erc-sqlite-tests.el --- Tests for erc-sqlite  -*- lexical-binding:t -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; Code:

(require 'erc-span)
(when (>= emacs-major-version 29)
  (require 'erc-sqlite))

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(declare-function erc-sqlite--ensure-network "erc-sqlite" (&rest _))
(declare-function erc-sqlite-mode "erc-sqlite" (arg))

(declare-function sqlite-open "sqlite.c" &optional (file))
(declare-function sqlite-select "sqlite.c" (db query &optional values rettype))

(ert-deftest erc-span--on-live-privmsg ()
  (unless (and (fboundp 'sqlite-available-p) (sqlite-available-p))
    (ert-skip "SQLite missing"))

  (let ((erc-sqlite--connection (sqlite-open))
        (erc-modules (cons 'sqlite erc-modules))
        (servertimep (cl-evenp (time-convert nil 'integer)))
        (start (float-time)))
    (erc-tests-common-make-server-buf)
    (erc-sqlite-mode +1)
    (erc-sqlite--ensure-network)
    (with-current-buffer (erc--open-target "#chan")
      (erc-update-channel-member "#chan" "bob" "bob" 'add)
      (erc-update-channel-member "#chan" "alice" "alice" 'add))

    (should-not (buffer-local-value 'erc-sqlite--span-id (get-buffer "#chan")))
    (let* ((ts "2021-01-01T04:44:04.123Z")
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":bob PRIVMSG #chan :123"
                                  :sender "bob"
                                  :command "PRIVMSG"
                                  :command-args '("#chan" "123")
                                  :contents "123"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))

    (should (buffer-local-value 'erc-sqlite--span-id (get-buffer "#chan")))
    (should (equal '((1)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    ;; Ignored (not inserted into database) because it's older than
    ;; the start of the span.
    (let* ((ts "2021-01-01T04:44:00.000Z")
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":bob PRIVMSG #chan :hi"
                                  :sender "bob"
                                  :command "PRIVMSG"
                                  :command-args '("#chan" "hi")
                                  :contents "hi"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))
    (should (equal '((1)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    (let* ((ts "2021-01-01T04:44:12.456Z") ; 8 seconds later
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":bob PRIVMSG #chan :456"
                                  :sender "bob"
                                  :command "PRIVMSG"
                                  :command-args '("#chan" "456")
                                  :contents "456"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))
    (should (equal '((2)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    (with-current-buffer "#chan"
      ;; Messages retain references to row IDs
      (goto-char (text-property-any (point-min) (point-max) 'erc--msg
                                    'span-range-bookend-begin))
      (should (looking-at ">>> RANGE-BEG "))
      (goto-char (text-property-any (point-min) (point-max) 'erc-sqlite--id 1))
      (should (looking-at (rx "<bob> 123")))
      (forward-line)
      (should (looking-at (rx "<bob> hi")))
      (goto-char (text-property-any (point-min) (point-max) 'erc-sqlite--id 2))
      (should (looking-at (rx "<bob> 456"))))

    (pcase-exhaustive
        (sqlite-select erc-sqlite--connection "SELECT * FROM Messages;")
      (`((1 1 ,span "2021-01-01T04:44:04.123Z" nil
            "bob" nil "PRIVMSG" "bob" "123" nil)
         (2 1 ,span "2021-01-01T04:44:12.456Z" nil
            "bob" nil "PRIVMSG" "bob" "456" nil))
       (should (< start (/ span 1000.0) (float-time)))))

    (when noninteractive
      (kill-buffer "#chan"))))

(ert-deftest erc-span--on-live-join/part ()
  (unless (and (fboundp 'sqlite-available-p) (sqlite-available-p))
    (ert-skip "SQLite missing"))

  (let ((erc-sqlite--connection (sqlite-open))
        (erc-modules (cons 'sqlite erc-modules))
        (servertimep (cl-evenp (time-convert nil 'integer)))
        (start (float-time)))
    (erc-tests-common-make-server-buf)
    (erc-set-current-nick "tester")
    (erc-sqlite-mode +1)
    (erc-sqlite--ensure-network)

    (should-not (buffer-local-value 'erc-sqlite--span-id
                                    (save-excursion
                                      (erc--open-target "#chan"))))
    (let* ((ts "2021-01-01T04:44:04.123Z")
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":bob!~u@123 JOIN #chan"
                                  :sender "bob!~u@123"
                                  :command "JOIN"
                                  :command-args '("#chan")
                                  :contents "#chan"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))

    (should (buffer-local-value 'erc-sqlite--span-id (get-buffer "#chan")))
    (should (equal '((1)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    (let* ((ts "2021-01-01T04:44:08.456Z")
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":alice!~u@456 JOIN #chan"
                                  :sender "alice!~u@456"
                                  :command "JOIN"
                                  :command-args '("#chan")
                                  :contents "#chan"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))
    (should (equal '((2)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    (let* ((ts "2021-01-01T04:44:12.789Z")
           (erc--current-time-pair (erc-compat--iso8601-to-time ts t))
           (erc--parsed-response (make-erc--zPRIVMSG
                                  :unparsed ":bob!~u@123 PART #chan :789"
                                  :sender "bob!~u@123"
                                  :command "PART"
                                  :command-args '("#chan" "789")
                                  :contents "789"
                                  :tags (and servertimep `((time . ,ts))))))
      (erc-call-hooks erc-server-process erc--parsed-response))
    (should (equal '((3)) (sqlite-select erc-sqlite--connection
                                         "SELECT COUNT (*) FROM Messages;")))

    (with-current-buffer "#chan"
      ;; Messages retain references to row IDs
      (goto-char (text-property-any (point-min) (point-max) 'erc--msg
                                    'span-range-bookend-begin))
      (should (looking-at ">>> RANGE-BEG "))
      (goto-char (text-property-any (point-min) (point-max) 'erc-sqlite--id 1))
      (should (looking-at (rx "*** bob (~u@123) has joined channel #chan")))
      (forward-line)
      (should (looking-at (rx "*** alice (~u@456) has joined channel #chan")))
      (goto-char (text-property-any (point-min) (point-max) 'erc-sqlite--id 3))
      (should
       (looking-at (rx "*** bob (~u@123) has left channel #chan: 789"))))

    (pcase-exhaustive
        (sqlite-select erc-sqlite--connection "SELECT * FROM Messages;")
      (`((1 1 ,span "2021-01-01T04:44:04.123Z" nil
            "bob" nil "JOIN" "~u@123" nil nil)
         (2 1 ,span "2021-01-01T04:44:08.456Z" nil
            "alice" nil "JOIN" "~u@456" nil nil)
         (3 1 ,span "2021-01-01T04:44:12.789Z" nil
            "bob" nil "PART" "~u@123" "789" nil))
       (should (< start (/ span 1000.0) (float-time)))))

    (when noninteractive
      (kill-buffer "#chan"))))

;;; erc-sqlite-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

