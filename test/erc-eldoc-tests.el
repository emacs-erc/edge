;;; erc-eldoc-tests.el --- Tests for erc-eldoc.  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2022 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO: This file is just a demo.  Rewrite or delete.

;;; Code:

(require 'ert-x)
(eval-and-compile
  (let ((load-path (cons (ert-resource-directory) load-path)))
    (require 'erc-tests-common)))

(require 'erc-eldoc)
(require 'erc-button)

(ert-deftest erc-eldoc-nick-at-point ()
  (let ((erc-insert-modify-hook erc-insert-modify-hook)
        ;;
        user
        erc-kill-channel-hook erc-kill-server-hook erc-kill-buffer-hook)
    (cl-pushnew #'erc-button-add-buttons erc-insert-modify-hook)

    (erc-tests-common-make-server-buf (buffer-name))
    (setq erc-server-users (make-hash-table :test 'equal))
    (erc-button--initialize-users)
    (setq user (make-erc-server-user
                :nickname "Bob"
                :account "bobzilla"
                :full-name "Bob Zilla"
                :host "localhost"
                :login "~bob"))
    (erc-button--ensure-data-prop "bob" user nil)
    (puthash "bob" user erc-server-users)

    (with-current-buffer (erc--open-target "#chan")
      (erc-button--initialize-users)
      (puthash "bob" (cons user (make-erc-channel-user :voice t))
               erc-channel-members)
      (let ((msg (erc-format-privmessage "Bob" "hi" nil t)))
        (erc-display-message nil nil (current-buffer) msg)
        (goto-char (point-min))
        (should (search-forward "<B" nil t))
        (forward-char)
        (should (equal (erc-eldoc-nick-at-point #'list)
                       '(#("v ~bob@localhost \"Bob Zilla\""
                           0 1 (help-echo "voice" face erc-nick-prefix-face)
                           1 2 (face erc-nick-prefix-face)
                           2 28 (face erc-notice-face))
                         :thing "bobzilla" :face erc-notice-face)))

        ;; Removing user changes face.
        (remhash "bob" erc-channel-users)
        (should (equal (erc-eldoc-nick-at-point #'list)
                       '(#("v ~bob@localhost \"Bob Zilla\""
                           0 1 (help-echo "voice" face erc-nick-prefix-face)
                           1 2 (face erc-nick-prefix-face)
                           2 28 (face erc-error-face))
                         :thing "bobzilla" :face erc-error-face)))))

    (when noninteractive
      (kill-buffer "#chan"))))

;;; erc-eldoc-tests.el ends here


;; XXX this is a temporary measure to prevent compilation warnings in
;; preview packages built from these bug sets.  It is not present in the
;; actual patches for emacs.git. These tests depend on trunk-only names
;; that would otherwise require shims to load on older Emacs versions.

;; Local Variables:
;; no-byte-compile: t
;; End:

