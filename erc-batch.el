;;; erc-batch.el --- IRCv3 batch for ERC -*- lexical-binding: t -*-

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library provides a facility for stashing and processing IRCv3
;; batch messages.
;;
;; TODO
;;
;; - Don't depend on `server-time' for the `chathistory' batch type,
;;   and warn if it's not enabled
;; - Support `netsplit' and `netjoin' by integrating with
;;   `erc-netsplit'

;;; Code:

(require 'erc-v3)
(require 'erc-span)

(erc-v3--define-capability batch
  :slots (( es nil :type (list-of erc-batch--info)
            ;; Objects with the same ref can be members, but only one
            ;; can correspond to the batch being enqueued at a given
            ;; time (represented by a `+' symbol in the `state' slot
            ;; of a `erc-batch--info' object.
            :documentation "Active batches being received or processed.")
          ( counter 0 :type natnum
            :documentation "Counter for issuing client batch IDs."))
  (if erc-v3--batch
      (progn
        (erc--module-active-p 'stamp 'ensurep)
        (unless erc-span-mode
          (erc-span-mode +1)))
    (erc-span-mode -1)))

(cl-defstruct erc-batch--info
  "Data for processing a single batch response."
  ( type (error "Required `:type' keyword missing") :type symbol
    :documentation "Type of batch, like `chathistory'.")
  ( ref (error "Required `string' keyword missing") :type string
    :documentation "Reference tag minus the +/- prefix.")
  ( name (error "Required `name' keyword missing") :type string
    :documentation "Human identifier, such as a target buffer name.")
  ( queue nil :type (or null buffer)
    :documentation "Buffer of unprocessed lines.")
  ( count nil :type (or null integer)
    :documentation "Total number of IRC lines (messages) received in batch.")
  ( length 0 :type integer
    :documentation "Number of IRC lines remaining in queue.")
  ( timer nil :type (or null timer)
    :documentation "Worker timer for handling the next queued message.")
  ( reporter nil :type (or null function)
    :documentation "Progress reporter.")
  ( state '+ :type symbol
    :documentation "One of receiving (+), processing (-), or done (t).")
  ( child nil :type (or null erc-batch--info)
    :documentation "Current child batch and synchronization flag.")
  ( parent nil :type (or null erc-batch--info)
    :documentation "Parent object, if any."))

(defvar erc-batch--parsed-tags nil
  "Cached tags value used by `erc--parse-message-tags' wrapper.
Although this is bad practice, ERC currently relies on this variable
being non-nil to indicate deferred response processing influenced by
this module is occurring.")

;; Servers reuse batch refs straight away, even when they're not
;; derived from the target name.  For naming queue buffers, we
;; therefore need something unique that's more informative than
;; foo<1>.
(defun erc-batch--info-gen-qual-name (info &optional idp)
  "Return a string for use as a buffer name referencing INFO.
With IDP, suffix the string with INFO's hash in order to resist
collisions."
  (concat (and-let* ((parent (erc-batch--info-parent info)))
            ;; Don't include hash for nested prefix.
            (concat (erc-batch--info-gen-qual-name parent nil) "."))
          (erc-batch--info-name info)
          (format ".%.8s" (erc-batch--info-ref info))
          (and idp (format ".%d" (sxhash-eq info)))))

(defun erc-batch--es () ; used mainly in testing
  "Return active batches."
  (erc-v3--batch-es erc-v3--batch))


;;;; General batch methods for `span' interface

(cl-defmethod erc-span--on-live-privmsg
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-join
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-part
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-quit
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-nick
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-kick
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-mode
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--on-live-topic
  (_ &context (erc-v3--batch erc-v3--batch)))

(cl-defmethod erc-span--inserter-get-length ((info erc-batch--info))
  (erc-batch--info-length info))

(cl-defmethod erc-span--inserter-set-timer ((info erc-batch--info) val)
  (setf (erc-batch--info-timer info) val))

(cl-defmethod erc-span--inserter-get-name ((info erc-batch--info))
  (erc-batch--info-name info))


;;;; Queuing

;; For now, this is implemented as a simple append log that lives in a
;; separate buffer.  This allows for easy debugging and emergency
;; recovery.  Since messages almost always arrive in order, scanning
;; backwards when necessary to splice in late arrivals is likely a
;; nonissue and seems preferable to adopting a fancy data structure.

(defvar-local erc-batch--queue-last-timestamp nil)

(define-derived-mode erc-batch--queue-mode fundamental-mode
  "ERC Batch Queue"
  "Major mode for ERC batch buffers containing unprocessed lines.
Lines are raw byte sequences delimited by CRLF endings.  Those
endings have a `tags' text property whose value is a list of tags
as returned by the function `erc--parse-message-tags'."
  :interactive nil
  (set-buffer-multibyte nil))

(defun erc-batch--gen-queue (name)
  "Create a `erc-batch--queue-mode' buffer based on NAME."
  ;; Enforce leading space to suppress undo history.
  (with-current-buffer (get-buffer-create (format " *%s*" name))
    (erc-batch--queue-mode)
    (current-buffer)))

(defun erc-batch--create-queue (info)
  "Create a queue buffer for INFO and return it."
  (let ((name (erc-batch--info-gen-qual-name info 'idp)))
    (setf (erc-batch--info-queue info) (erc-batch--gen-queue name))))

(defun erc-batch--enqueue (info tags line)
  "Insert LINE into INFO's queue and store TAGs in `tags' text prop.
Store LINE's character length in a `length' text prop, and have
text props only span CRLF line endings."
  (with-current-buffer (erc-batch--info-queue info)
    (cl-assert (not (buffer-narrowed-p)))
    (cl-assert (= (point-max) (point)))
    (let ((time (alist-get 'time tags)))
      (if (and time erc-batch--queue-last-timestamp
               (string-lessp time erc-batch--queue-last-timestamp))
          (while-let (((not (bobp)))
                      (last (get-text-property (- (point) 2) 'ts))
                      ((string-lessp time last))
                      (length (get-text-property (- (point) 2) 'length)))
            (goto-char (- (point) 2 length))
            (cl-assert (or (bobp) (= (char-before (point)) ?\n))))
        (when time
          (setq erc-batch--queue-last-timestamp time)))
      (insert line "\r\n")
      (add-text-properties (- (point) 2) (point)
                           (list 'tags tags 'length (length line) 'ts time))
      (goto-char (point-max)))
    (cl-incf (erc-batch--info-length info))))

(defun erc-batch--remove-last (info)
  "Remove and return newest cons pair (TAGS . LINE) in INFO's queue.
LINE does not include CRLF ending."
  (with-current-buffer (erc-batch--info-queue info)
    (unless (= (point) (point-min))
      (save-excursion
        (goto-char (- (point) 2))
        (when-let* ((beg (- (point) (get-text-property (point) 'length)))
                    (line (buffer-substring beg (point)))
                    (tags (get-text-property (point) 'tags)))
          (narrow-to-region (point-min) beg)
          (cl-decf (erc-batch--info-length info))
          (cons tags line))))))

(defun erc-batch--remove (info)
  "Remove and return oldest cons pair (TAGS . LINE) in INFO's queue.
Remove by narrowing to hide the returned LINE, which is an IRC
message without a CRLF ending."
  (with-current-buffer (erc-batch--info-queue info)
    (save-excursion
      (goto-char (point-min))
      ;; Otherwise buffer is empty
      (when-let* ((end (next-property-change (point)))
                  (line (buffer-substring (point-min) end))
                  (tags (get-text-property end 'tags)))
        (narrow-to-region (+ 2 end) (point-max))
        (cl-decf (erc-batch--info-length info))
        (cons tags line)))))

(defun erc-batch--peek-last (info)
  "Return but don't remove newest pair of (TAGS . LINE) from INFO's queue.
Don't include CRLF ending.  Behave similarly to (ring-ref my-ring 0)."
  (with-current-buffer (erc-batch--info-queue info)
    (unless (= (point) (point-min))
      (save-excursion
        (goto-char (- (point) 2))
        (when-let* ((beg (- (point) (get-text-property (point) 'length)))
                    (line (buffer-substring beg (point)))
                    (tags (get-text-property (point) 'tags)))
          (cons tags line))))))

(defun erc-batch--peek (info)
  "Return but don't remove oldest INFO queue cons pair of (TAGS . LINE).
Don't include CRLF ending.  Behave similarly to (ring-ref my-ring -1)."
  (with-current-buffer (erc-batch--info-queue info)
    (unless (= (point) (point-min))
      (save-excursion
        (goto-char (point-min))
        (when-let* ((end (next-property-change (point)))
                    (line (buffer-substring (point-min) end))
                    (tags (get-text-property end 'tags)))
          (cons tags line))))))

(defun erc-batch--visit-stamp-cohorts (fn)
  "Visit the oldest message and subsequent ones sharing its stamp.
Call FN with (TAGS LINE-BEG LINE-END)."
  (save-excursion
    (goto-char (point-min))
    (let (first)
      (while-let ((end (next-property-change (point)))
                  (tags (get-text-property end 'tags))
                  (time (alist-get 'time tags))
                  ((or (null first) (string= time first))))
        (setq first time)
        (funcall fn tags (- end (get-text-property end 'length)) end)
        (goto-char (+ end 2))))))

(defun erc-batch--count-oldest (info)
  "Count the number of oldest tags in INFO sharing the same stamp."
  (with-current-buffer (erc-batch--info-queue info)
    (unless (= (point) (point-min))
      (let ((count 0))
        (erc-batch--visit-stamp-cohorts (lambda (&rest _) (cl-incf count)))
        count))))


;;;; Batch "type" handlers

(cl-defgeneric erc-batch--create-info (_type _parsed _ref _rest) nil)

(cl-defgeneric erc-batch--handle-enqueued (info)
  "Run insertion on \"BATCH\" done message.
Expect INFO to be an `erc-batch--info' object."
  (ignore info))

(cl-defgeneric erc-batch--massage-enqueued (info)
  "Possibly override INFO object on \"BATCH\" done message.
Expect INFO to be an `erc-batch--info' object."
  info)

(cl-defgeneric erc-batch--handle-done (info)
  "Clean up batch represented by `erc-batch--info' object INFO."
  (ignore info))

(cl-defmethod erc-batch--handle-enqueued :around (info)
  "Copy `length' field of `erc-batch--info' INFO to `count'.
Short-circuit dispatch by diverting to `erc-batch--handle-done' if the
batch is empty."
  (cl-assert (null (erc-batch--info-count info)))
  (if-let* ((count (setf (erc-batch--info-count info)
                         (erc-batch--info-length info)))
            ((zerop count)))
      (erc-batch--handle-done info)
    (cl-call-next-method)))


;;;; Type: chathistory (and znc.in/playback)

(cl-defstruct (erc-batch--chathistory (:include erc-batch--info))
  "Data for processing a single `chathistory' batch response.
Not to be confused with the `chathistory'extension.  Assumes all
messages encountered are destined for the same target buffer, which it
creates if necessary.  Child batches can display messages to a parent's
context by setting `erc-span--active-inserter' to the parent's instance,
and then calling `erc-display-message', typically indirectly."
  ( range nil :type (or null erc-span--range)
    :documentation "Active range to insert history in.")
  ( locals nil :type (list-of cons)
    :documentation "Local vars in an alist of (BUFFER . HASHMAP).")
  ( below-lower nil :type list)
  ( above-upper nil :type list)
  ( parsed (error "Required `:parsed' keyword missing") :type erc-response))

(defun erc-batch--chathistory-prep-buffer (info)
  (let* ((parsed (erc-batch--chathistory-parsed info))
         (buffer (erc-batch--ensure-target-buffer parsed info)))
    (cl-assert (eq (erc-server-buffer) (current-buffer)))
    (setf (erc-batch--chathistory-locals info)
          (erc-span--populate-locals nil (current-buffer) buffer))
    buffer))

(defvar erc-batch--chathistory-header-line-prefix
  #("Receiving..." 0 12 (font-lock-face (erc-italic-face erc-input-face))))

(defun erc-batch--chathistory-create-info (parsed ref rest)
  "Return a new `erc-batch--chathistory' object."
  (cl-assert (not (erc-batch--get-info ref)) t)
  (let* ((target (pop rest))
         (info (make-erc-batch--chathistory :type 'chathistory
                                            :parsed parsed
                                            :ref ref
                                            :name target))
         (buffer (erc-get-buffer target erc-server-process)))
    (when buffer
      (let ((erc-header-line-format
             (concat erc-batch--chathistory-header-line-prefix
                     erc-header-line-format)))
        (erc-update-mode-line buffer)))
    (erc-batch--create-queue info)
    info))

(cl-defmethod erc-batch--create-info ((_ (eql chathistory)) parsed ref rest)
  (erc-batch--chathistory-create-info parsed ref rest))

(cl-defmethod erc-batch--create-info ((_ (eql znc.in/playback))
                                      parsed ref rest)
  (erc-batch--chathistory-create-info parsed ref rest))

(defun erc-batch--chathistory-get-reporter-name (info)
  (if (erc-batch--info-parent info)
      (erc-batch--info-gen-qual-name info)
    (erc-batch--info-name info)))

(defvar erc-batch--display-nested nil)

(cl-defmethod erc--insert-line
  ((_ string) &context
   (erc-v3--batch erc-v3--batch)
   (erc-span--active-inserter erc-batch--chathistory)
   (erc--insert-marker null)
   (erc-batch--display-nested null))
  "Run `erc--insert-line' with marker at historical buffer position."
  (let* ((range (erc-batch--chathistory-range erc-span--active-inserter))
         (time (erc--current-time-pair))
         ;; We could store this in `erc-batch--chathistory-locals' and
         ;; dispense with this method entirely, but response handlers
         ;; may need to insert things elsewhere.
         (erc--insert-marker (erc-span--range-get-insert-marker range time))
         (erc-v3--server-time-display-nested-p t)
         (erc-batch--display-nested t))
    (cl-assert (marker-buffer erc--insert-marker))
    (cl-call-next-method)))

(defun erc-batch--chathistory-process (info)
  "Process one queued message for `erc-batch--chathistory' object INFO.
When INFO is a child batch, wait until its parent acknowledges it as
such.  Conversely, if INFO is a parent, wait until the child finishes
processing its batch completely."
  (cl-assert (null erc--target))
  (when-let* ((parent (erc-batch--info-parent info)))
    (cl-assert (erc-batch--info-child parent)))
  (unless (erc-batch--info-child info)
    (cl-assert (erc-batch--state-p info '-))
    (cl-assert (not (zerop (erc-batch--info-length info))))
    (pcase-let ((`(,tags . ,latest) (erc-batch--remove info))
                (erc-auto-query nil)) ; don't create a query window
      (let ((erc-batch--parsed-tags tags))
        (erc-span--with-locals (erc-batch--chathistory-locals info)
                               ((erc-server-buffer)
                                (erc-get-buffer (erc-batch--info-name info) erc-server-process))
                               (erc-parse-server-response erc-server-process latest)))
      (when-let* ((reporter (erc-batch--chathistory-reporter info)))
        (funcall reporter info)))))

(declare-function erc-button--drain-phantom-cmems "erc-button" nil)

(cl-defmethod erc-span--inserter-insert-one ((info erc-batch--chathistory))
  (erc-batch--chathistory-process info))

(cl-defmethod erc-span--inserter-finalize ((info erc-batch--chathistory))
  (erc-span--range-hide-bookend-hi (erc-batch--chathistory-range info))
  (unless (erc-batch--info-parent info)
    (erc-with-buffer ((erc-batch--info-name info))
      (erc-span--finalize-destructive-target-locals)))
  (erc-batch--handle-done info))

;; Messages in a batch are destined for a span range that doesn't yet
;; exist, so if any belongs to an existing one, it must be removed prior
;; to insertion.
(defun erc-batch--sequester-lower (info)
  (cl-assert erc-span--ranges)
  (while-let ((time (alist-get 'time (car (erc-batch--peek info))))
              ((erc-span--range-find-by-time erc-span--ranges time))
              (pre (erc-batch--remove info)))
    (push pre (erc-batch--chathistory-below-lower info))))

(defun erc-batch--sequester-upper (info)
  (cl-assert erc-span--ranges)
  (while-let ((time (alist-get 'time (car (erc-batch--peek-last info))))
              ((erc-span--range-find-by-time erc-span--ranges time))
              (post (erc-batch--remove-last info)))
    (push post (erc-batch--chathistory-above-upper info))))

(cl-defmethod erc-batch--massage-enqueued ((info erc-batch--chathistory))
  "Stash messages falling outside the interval to be filled by the batch."
  (cl-assert (null (erc-batch--info-count info)))
  (when-let* (((not (zerop (erc-batch--info-length info))))
              (buffer (erc-get-buffer (erc-batch--info-name info)
                                      erc-server-process)))
    (with-current-buffer buffer
      (when erc-span--ranges
        (erc-batch--sequester-lower info)
        (erc-batch--sequester-upper info))))
  (cl-call-next-method))

(defun erc-batch--make-reporter (info)
  (let ((reporter (make-progress-reporter
                   (erc-batch--chathistory-get-reporter-name info) 0
                   (erc-batch--info-length info) 0)))
    (lambda (info)
      (if-let* ((range (erc-batch--chathistory-range info))
                (length (erc-batch--info-length info))
                (count (erc-batch--info-count info))
                ((or (zerop length) (= length count))))
          (progn
            (setcar (erc-span--range-display range) "")
            (progress-reporter-done reporter))
        (setcar (erc-span--range-display range)
                (format " (%d/%d)" (- count length) count))
        (progress-reporter-update reporter (- count length))))))

(cl-defmethod erc-batch--handle-enqueued ((info erc-batch--chathistory))
  (cl-assert (null erc--target))
  (cl-assert (erc-batch--info-count info)) ; initializedp
  (cl-assert (not (zerop (erc-batch--info-length info))))
  (when-let* ((buffer (erc-batch--chathistory-prep-buffer info))
              (server-buffer (current-buffer)))
    (with-current-buffer buffer
      (when-let*
          ;; Expect that queue has already been trimmed of messages
          ;; older than lower bound.
          ((oldest-ts (alist-get 'time (car (erc-batch--peek info))))
           ((always (cl-assert
                     (or (null erc-span--ranges)
                         (thread-first erc-span--ranges
                                       (erc-span--range-get-oldest)
                                       (erc-span--range-ts-lo)
                                       (string> oldest-ts))
                         (null (erc-span--range-find-by-time
                                erc-span--ranges oldest-ts))))))
           (newest-ts (alist-get 'time (car (erc-batch--peek-last info))))
           (range (erc-span--range-add oldest-ts newest-ts)))
        (let ((reporter (and (not noninteractive)
                             (erc-batch--make-reporter info))))
          (setf (erc-batch--chathistory-range info) range
                (erc-batch--chathistory-reporter info) reporter))
        (when (eq (window-buffer) buffer)
          (erc-update-mode-line))
        (with-current-buffer server-buffer
          (erc-span--run-inserter server-buffer info))))))

;; FIXME when replayed messages have `msgid's, perform deduplication.
;; When a new message we haven't seen belongs to a span, insert it as
;; necessary.
(cl-defmethod erc-batch--handle-done ((info erc-batch--chathistory))
  (when-let* (((not (erc-batch--info-parent info)))
              ((fboundp 'erc-button--drain-phantom-cmems))
              (buf (erc-get-buffer (erc-batch--info-name info)))
              ((buffer-live-p buf)))
    (with-current-buffer buf
      (erc-button--drain-phantom-cmems)))
  (erc-batch--common-teardown info))


;;;; Common

(defun erc-batch--transition (info)
  "Advance batch represented by INFO to next state."
  (cl-callf (lambda (v) (if (eq v '+) '- t)) (erc-batch--info-state info)))

(defun erc-batch--state-p (info state)
  "Whether batch described by INFO is currently in STATE."
  (eq (erc-batch--info-state info) state))

(defun erc-batch--ensure-target-buffer (parsed info)
  "Ensure buffer for INFO exists and return it.
Expect PARSED to be an `erc-response' object.  For queries,
possibly change content of `target' slot to sender."
  (let* ((target (erc-batch--info-name info))
         (buf (erc-get-buffer target erc-server-process)))
    (unless buf
      ;; Channel playback often happens immediately after a JOIN, and
      ;; that handler creates the buffer if necessary.  However,
      ;; that's not the case for queries.
      (cl-assert (not (erc-channel-p target)))
      ;; If the recipient is our current nick, and the sender is
      ;; someone else, create a buffer for that someone else instead,
      ;; and make them the target.
      (save-excursion ; `erc-open' sets the buffer!
        (when-let* (((erc-nick-equal-p target (erc-current-nick)))
                    (sender (erc-extract-nick (erc-response.sender parsed)))
                    ((not (erc-nick-equal-p sender target))))
          (setf (erc-batch--info-name info) sender
                target sender))
        (let ((erc-join-buffer 'bury))
          (setq buf (erc--open-target target)))))
    (cl-assert (buffer-local-value 'erc--target buf))
    buf))

(defun erc-batch--common-teardown (info)
  "Finalize batch represented by INFO."
  (setf (erc-batch--info-timer info) nil)
  ;; If we're a child, unpause parent.
  (when-let* ((parent (erc-batch--info-parent info)))
    (cl-assert (eq info (erc-batch--info-child parent)))
    (setf (erc-batch--info-child parent) nil))
  (cl-assert (erc-batch--state-p info '-))
  (erc-batch--transition info)
  (kill-buffer (erc-batch--info-queue info))
  (cl-callf (lambda (v) (delq info v)) (erc-v3--batch-es erc-v3--batch)))

(define-inline erc-batch--get-info (ref)
  "Return active `erc-batch--info' object for REF or nil."
  (inline-quote
   (catch 'found (dolist (info (erc-v3--batch-es erc-v3--batch))
                   (when (and (string= ,ref (erc-batch--info-ref info))
                              (erc-batch--state-p info '+))
                     (throw 'found info))))))

(defun erc-batch--ensure-info (parsed)
  "Initialize handling for \"BATCH\" `erc-response' PARSED.
Return a newly registered or fully enqueued `erc-batch--info' object,
updating `erc-v3--batch-es' as appropriate to ensure newly arriving
messages have a queue."
  (cl-assert (erc--server-buffer-p))
  (cl-assert (string= "BATCH" (erc-response.command parsed)))
  (pcase-exhaustive (erc-response.command-args parsed)
    (`(,ref)
     ;; This batch's messages have all been queued.  Mark it as ready
     ;; for processing.
     (cl-assert (= ?- (aref ref 0)))
     (let ((existing (erc-batch--get-info (substring ref 1))))
       (cl-assert existing)
       (erc-batch--transition existing)
       existing))
    (`(,ref ,type . ,rest)
     (setq ref (substring ref 1))
     (if-let* ((info (erc-batch--get-info ref)))
         (prog1 info ; start of a nested batch
           (cl-assert (erc-batch--state-p info '+))
           (cl-assert (assq 'batch (erc-response.tags parsed))))
       (if-let* ((new (erc-batch--create-info (intern type) parsed ref rest)))
           (progn
             (cl-assert (erc--server-buffer-p)) ; regression
             (car (push new (erc-v3--batch-es erc-v3--batch))))
         (erc-display-error-notice parsed
                                   (format "Unknown batch type %s" type))
         (error "Unknown batch type: %s" type))))))

(define-erc-response-handler (BATCH)
  "Handle \"BATCH\" command." nil
  (cl-assert (erc--server-buffer-p))
  (when-let* ((info (erc-batch--ensure-info parsed)))
    (if (erc-batch--state-p info '-)
        (progn
          ;; Pause processing on a parent batch if its inserter called
          ;; this handler.  IOW, `info' is its child, and `parsed' is
          ;; a closing "BATCH -inner-ref" message whose `batch' tag is
          ;; the parent's ref.
          (when-let* ((erc-span--active-inserter)
                      (parent erc-span--active-inserter)
                      ((eq parent (erc-batch--info-parent info))))
            (cl-assert (not (erc-batch--info-child parent)))
            (setf (erc-batch--info-child parent) info))
          (erc-batch--handle-enqueued (erc-batch--massage-enqueued info)))
      (cl-assert (null (erc-batch--info-timer info)))))
  nil)

(cl-defmethod erc--parse-message-tags
  ((_ string)
   &context (erc-v3--batch erc-v3--batch) (erc-batch--parsed-tags cons))
  erc-batch--parsed-tags)

(defvar erc-batch--parsing-nested-p nil)

(defun erc-batch--handle-nested-batch-start (ref)
  "Look for nested REF in active batches and initialize when found.
Visit every active batch in `erc-v3--batch-es' and parse most recent
entry, looking for a nested \"BATCH\" command tagged with REF.  If
found, create a new batch in `erc-v3--batch-es'."
  (catch 'found
    (dolist (info (erc-v3--batch-es erc-v3--batch))
      (when-let* (((erc-batch--state-p info '+))
                  ((not (zerop (erc-batch--info-length info))))
                  (raw (erc-batch--peek-last info))
                  (erc-batch--parsing-nested-p t)
                  (parsed (erc-parse-server-response erc-server-process
                                                     (cdr raw)))
                  ((string= "BATCH" (erc-response.command parsed)))
                  (raw-ref (car (erc-response.command-args parsed)))
                  (child-info (erc-batch--ensure-info parsed))
                  ((string= ref (substring raw-ref 1))))
        ;; Handler has created new entry.
        (setf (erc-batch--info-parent child-info) info)
        (cl-assert (memq child-info (erc-v3--batch-es erc-v3--batch)))
        (throw 'found child-info)))))

(cl-defmethod erc--finish-parsing-server-response
  (_ string message &context
     (erc-v3--batch erc-v3--batch) (erc-batch--parsing-nested-p (eql t)))
  "Finish parsing a nested batch."
  (cl-assert (null erc-batch--parsed-tags))
  (erc--parse-traditional-response string message)
  message)

(cl-defmethod erc--finish-parsing-server-response
  (_ _ message &context
     (erc-v3--batch erc-v3--batch) (erc-batch--parsed-tags null))
  "Stash batch lines and exit, or parse and run handlers, like normal."
  (if-let* ((tags (erc-response.tags message))
            (batch (assq 'batch tags))
            (ref (cdr batch))
            (info (or (erc-batch--get-info ref)
                      (erc-batch--handle-nested-batch-start ref))))
      ;; TODO after adding sqlite or other storage backend, save
      ;; messages here.
      (erc-batch--enqueue info tags (erc-response.unparsed message))
    (cl-call-next-method))
  message)


;;;; Client batches

(defun erc-batch--get-client-batch-id ()
  "Issue a new opaque string to send as a client batch identifier."
  (number-to-string (cl-callf (lambda (v) (% (1+ v) 1024))
                        (erc-v3--batch-counter erc-v3--batch))))

(provide 'erc-batch)
;;; erc-batch.el ends here
