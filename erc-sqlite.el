;;; erc-sqlite.el -- Local message storage -*- lexical-binding: t; -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; FIXME move this to its own patch, apart from `span'.

;; This library offers a minor mode and methods conforming to the
;; `erc-span' backend interface.  It keeps a local backlog of messages
;; in a database.
;;
;; TODO keep a simple, permanent index of date intervals for messages
;; in the store.  Luckily, insertions are sequential, so we only pay
;; full price for the first one.  For merges, we compare a handful of
;; consecutive messages bounding the batch newly arrived with existing
;; ones from adjacent regions.  Absent a sufficient overlap, we assume
;; a gap remains to be back-filled.


;;; Code:
(require 'erc-span)
(require 'json)
(require 'sqlite nil t)

(declare-function sqlite-execute "sqlite.c" (db query &optional values))
(declare-function sqlite-next "sqlite.c" (set))
(declare-function sqlite-open "sqlite.c" &optional (file))
(declare-function sqlite-pragma "sqlite.c" (db pragma))
(declare-function sqlite-select "sqlite.c" (db query &optional values rettype))
(declare-function erc-v3--merge-client-tags "erc-v3" (tags))
(declare-function erc-button--phantom-users-mode "erc-button" (arg))

(defgroup erc-sqlite nil
  "Message storage for ERC"
  :package-version '(ERC . "5.7") ; FIXME sync on release
  :group 'erc)

;; FIXME change this to `locate-user-emacs-file' when ready.
;; Until then calling code should do something like:
;;
;;   (setopt erc-sqlite-file
;;           (expand-file-name "erc-sqlite.db" temporary-file-directory))
;;
(defcustom erc-sqlite-file nil
  "Database file for message storage."
  :type '(choice (const nil) file))

(defconst erc-sqlite-schema-version 1
  "Database schema version for `erc-sqlite'.")

(defvar erc-sqlite-max-fill-on-join 20) ; FIXME make this a user option

(defvar erc-sqlite--include-unparsed-tags-p nil)

(defvar erc-sqlite--syncing-p nil)
(defvar erc-sqlite--connection nil "Global connection to the SQLite database.")

;; Cache foreign key references so we can avoid JOINs that create
;; automatic indexes.
(defvar-local erc-sqlite--network-id nil)
(defvar-local erc-sqlite--target-id nil)
(defvar-local erc-sqlite--span-id nil)
(defvar-local erc-sqlite--span-beg nil)

;;;###autoload
(define-minor-mode erc-sqlite--span-mode
  "This minor mode stores and replays messages in an SQLite database.
It's only meant for use by modules like `batch' and `chathistory'.
Do not activate it directly."
  :interactive nil
  (if erc-sqlite--span-mode
      (cond
       ((not (sqlite-available-p))
        (erc-button--display-error-notice-with-keys
         "SQLite support missing but required for module `store'.")
        (erc-sqlite--span-mode -1))
       ((null erc--target) ; server
        (unless erc-sqlite--connection
          (setq erc-sqlite--connection (sqlite-open erc-sqlite-file)))
        (erc-sqlite--ensure-tables erc-sqlite--connection)
        (erc--restore-initialize-priors erc-sqlite--span-mode
          erc-sqlite--network-id nil)
        (add-hook 'erc-after-connect 'erc-sqlite--ensure-network nil t))
       (t
        (erc--restore-initialize-priors erc-sqlite--span-mode
          erc-sqlite--target-id nil) ; don't restore span
        (cl-assert erc-sqlite--connection)
        (erc-sqlite--ensure-target)))
    (remove-hook 'erc-after-connect 'erc-sqlite--ensure-network t)
    (mapc #'kill-local-variable '(erc-sqlite--network-id
                                  erc-sqlite--target-id
                                  erc-sqlite--span-id
                                  erc-sqlite--span-beg))))

(defmacro erc-sqlite--execute (db values &rest lines)
  "Execute concatenated statement given by LINES."
  (declare (indent 1))
  `(sqlite-execute ,db ,(if (seq-every-p #'stringp lines)
                            (apply #'concat lines)
                          (cons 'concat lines))
                   ,values))

(defun erc-sqlite--migrate-from (_db from)
  "DDL/DML migrations for database DB FROM version."
  (error "No migration exists for v%d to v%d." from erc-sqlite-schema-version))

(defvar erc-sqlite--ddl-init-statements
  '("\
CREATE TABLE Networks (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    UNIQUE(name)
);"
    "\
CREATE TABLE Targets (
    id INTEGER PRIMARY KEY,
    network INTEGER NOT NULL,
    name TEXT NOT NULL,
    FOREIGN KEY(network) REFERENCES Networks(id),
    UNIQUE(network, name)
);"
    ;; `id' is a millisecond prec. UNIX timestamp scaled up by 1000.
    "\
CREATE TABLE Spans (
    id INTEGER PRIMARY KEY,
    target INTEGER NOT NULL,
    count INTEGER NOT NULL,
    beg TEXT NOT NULL,
    end TEXT NOT NULL,
    FOREIGN KEY(target) REFERENCES Targets(id),
    UNIQUE(target, beg)
);"
    ;; `text' is the chat text.  `tags' are stripped of `msgid' and
    ;; `time' items and stored as JSON (and NULL instead of "null"
    ;; when empty).  TODO: figure out whether it's possible to
    ;; overload `text' for multiple commands (e.g., a QUIT message)
    ;; and still have it eligible for fts content.
    "\
CREATE TABLE Messages (
    id INTEGER PRIMARY KEY,
    target INTEGER NOT NULL,
    span INTEGER NOT NULL,
    time TEXT NOT NULL,
    msgid TEXT,
    nick TEXT NOT NULL,
    tags TEXT,
    cmd TEXT,
    userhost TEXT,
    text TEXT,
    taint INTEGER,
    FOREIGN KEY(span) REFERENCES Spans(id),
    FOREIGN KEY(target) REFERENCES Targets(id)
);"
    ;; Whenever a new "live" message arrives, it's inserted as a new
    ;; row in Messages, and the `end' of the current span is synced.
    "\
CREATE TRIGGER SetSpanEnd AFTER INSERT ON Messages
WHEN NEW.time > (SELECT end FROM Spans WHERE id = NEW.span)
BEGIN UPDATE Spans SET end = NEW.time WHERE id = NEW.span; END;"
    "\
CREATE TRIGGER SetSpanCount AFTER INSERT ON Messages
BEGIN UPDATE Spans SET count = count + 1 WHERE id = NEW.span; END;"

    "CREATE INDEX SpanIndex ON Spans (target, beg);"
    "CREATE INDEX MessagesSpanTimeIndex ON Messages (span, time);"
    ;; NULLs are distinct, so networks that don't support message IDs
    ;; won't violate this uniqueness constraint.
    "CREATE UNIQUE INDEX MsgIdIndex ON Messages (target, msgid);"
    "CREATE INDEX MessageIndex ON Messages (target, time);")
  "SQL statements for initializing `erc-sqlite' database tables.")

(defun erc-sqlite--ensure-network (&rest _)
  "Add current network to the `erc-sqlite' database unless present."
  (when-let*
      ((db erc-sqlite--connection)
       ((null erc-sqlite--network-id))
       (net (erc-network))
       (net-name (erc-network-name))
       ((not (eq net erc-networks--name-missing-sentinel)))
       (val (erc-sqlite--execute db
              (list net-name)
              "INSERT OR IGNORE into Networks(name) VALUES(?) RETURNING id;")))
    (when (eq 0 val)
      (setq val (sqlite-select erc-sqlite--connection
                               "SELECT id FROM Networks WHERE name = ?;"
                               (list net-name))))
    (pcase-let ((`((,val)) val))
      (cl-assert val)
      (setq erc-sqlite--network-id val))))

(defun erc-sqlite--ensure-target (&optional db)
  "Add current buffer's target to the `erc-sqlite' database unless present."
  (when-let* ((db (or db erc-sqlite--connection))
              ((null erc-sqlite--target-id))
              (target (erc-target))
              (net-id (erc-with-server-buffer erc-sqlite--network-id)))
    (erc-sqlite--execute db
      (list net-id target)
      "INSERT OR IGNORE into Targets(network, name) VALUES(?, ?);")
    (pcase-let ((`((,val))
                 (sqlite-select
                  erc-sqlite--connection
                  "SELECT id FROM Targets WHERE network = ? AND name = ?;"
                  (list net-id target))))
      (cl-assert val)
      (setq erc-sqlite--target-id val))))

(defun erc-sqlite--get-latest-span (target-id)
  (pcase-let ((`((,val))
               (sqlite-select
                erc-sqlite--connection
                (concat "SELECT end FROM Spans WHERE id = ("
                        " SELECT max(id) from Spans where target = ?"
                        ")")
                (list target-id))))
    val))

(defun erc-sqlite--init-span (target-id time)
  "Add current buffer's span ID to the `erc-sqlite' database unless present.
Return newly minted span identifier for live messages."
  (cl-assert erc--target) ; this is a target buffer
  (cl-assert target-id)
  (cl-assert erc-sqlite--connection)
  (let ((last-end (erc-sqlite--get-latest-span target-id)))
    (when (or (null last-end) (string-lessp last-end time))
      (erc-sqlite--execute erc-sqlite--connection
        (list target-id 0 time time)
        (concat "INSERT OR IGNORE into Spans(id, target, count, beg, end)"
                " VALUES((unixepoch('now','subsec') * 1000), ?, ?, ?, ?);"))
      (pcase-let ((`((,val))
                   (sqlite-select
                    erc-sqlite--connection
                    "SELECT id FROM Spans WHERE target = ? AND beg = ?;"
                    (list target-id time))))
        (cl-assert val)
        val))))

(defun erc-sqlite--ensure-tables (db)
  "Create tables and evolve schema if necessary."
  ;; For now, instead of maintaining a "settings" or "config" table,
  ;; use the provided version metadata slot.
  (pcase-let ((`((,val)) (sqlite-select db "PRAGMA user_version")))
    (cl-assert (natnump val))
    (if (> val 0)
        (unless (= val erc-sqlite-schema-version)
          (erc-sqlite--migrate-from db val))
      (sqlite-pragma db (format "user_version = %d" erc-sqlite-schema-version))
      (dolist (stmt erc-sqlite--ddl-init-statements)
        (sqlite-execute db stmt)))))

(cl-defmethod erc-span--mint-live-range-id
  (ts &context (erc-sqlite--span-mode (eql t)))
  (setq erc-sqlite--span-id (erc-sqlite--init-span erc-sqlite--target-id ts)
        erc-sqlite--span-beg ts))

(defun erc-sqlite--assert-common ()
  (cl-assert erc-sqlite--connection)
  (cl-assert (not erc-sqlite--syncing-p))
  (cl-assert (null erc-span--active-inserter))
  (cl-assert erc--target)
  (cl-assert erc-sqlite--target-id)
  (cl-assert erc-sqlite--span-beg))

(defun erc-sqlite--encode-args-as-text (args)
  (setq args (reverse (if (string= (car args) (erc-target)) (cdr args) args)))
  (string-join (nreverse (cons (concat ":" (car args)) (cdr args))) " "))

(defun erc-sqlite--handle-live-target-with-trailing (parsed)
  "Commit a PARSED command that takes a target plus trailing args.
Discard any late-arriving message whose stamp indicates it was sent
before the start of `erc-sqlite--span-id'."
  (erc-sqlite--assert-common)
  (cl-assert (erc--zresponse-p parsed))
  (when-let*
      ((erc-sqlite--syncing-p t)
       (time (erc--zresponse-time-str parsed))
       ((not (string-lessp time erc-sqlite--span-beg)))
       (stmt (concat
              "INSERT INTO Messages ("
              " target, span, time, msgid, nick, tags, cmd, userhost, text"
              ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id;"))
       (rv (let* ((msgid (alist-get 'msgid (erc-response.tags parsed)))
                  (stags (cl-remove-if (lambda (v) (memq v '(msgid time)))
                                       (erc-response.tags parsed)
                                       :key #'car))
                  (tags (json-encode stags))
                  (tags (and (not (string= "null" tags)) tags))
                  (cmd (erc-response.command parsed))
                  (text (pcase-exhaustive cmd
                          ((or "PRIVMSG" "NOTICE" "PART" "QUIT" "TOPIC" "NICK")
                           (erc-response.contents parsed))
                          ("JOIN" nil)
                          ((or "MODE" "KICK")
                           (erc-sqlite--encode-args-as-text
                            (erc-response.command-args parsed)))))
                  (nick (erc--zresponse-nick-d parsed))
                  (uh (erc--zresponse-userhost parsed)))
             (sqlite-execute erc-sqlite--connection stmt
                             (list erc-sqlite--target-id erc-sqlite--span-id
                                   time msgid nick tags cmd uh text))))
       (id (caar rv)))
    (cl-assert erc-sqlite--span-id)
    (push (cons 'erc-sqlite--id id) erc--msg-prop-overrides))
  nil)

;; FIXME pass text-getter from these methods to handler.
(cl-defmethod erc-span--on-live-privmsg
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zPRIVMSG' PARSED."
  (cl-assert (erc--zPRIVMSG-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-join
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zJOIN' PARSED."
  (cl-assert (erc--zJOIN-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-part
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zPART' PARSED."
  (cl-assert (erc--zPART-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-quit
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zQUIT' PARSED."
  (cl-assert (erc--zQUIT-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-topic
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zTOPIC' PARSED."
  (cl-assert (erc--zTOPIC-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-mode
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zMODE' PARSED."
  (cl-assert (erc--zMODE-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-nick
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zNICK' PARSED."
  (cl-assert (erc--zNICK-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))

(cl-defmethod erc-span--on-live-kick
  (parsed &context (erc-sqlite--span-mode (eql t)))
  "Commit incoming message described by `erc--zKICK' PARSED."
  (cl-assert (erc--zKICK-p parsed))
  (erc-sqlite--handle-live-target-with-trailing parsed))


;;;; Timer-based worker management

(oclosure-define erc-sqlite-inserter
  "Incremental handler for inserting historical messages."
  (name :type string)
  (range :type erc-span--range)
  (size :type integer)
  (length :type integer :mutable t)
  (timer :type timer :mutable t)
  (reporter :type cons)
  (iterator :type sqlite)
  (finalizer :type function)
  (locals :type hash-table))

(defun erc-sqlite--insert-history-line-done (inst)
  (when (bound-and-true-p erc-button--phantom-users-mode)
    (erc-button--phantom-users-mode -1))
  (when-let* ((reporter (erc-sqlite-inserter--reporter inst)))
    (progress-reporter-done reporter)))

(defun erc-sqlite--reconstitute (cmd sender tags &rest args)
  (let* ((command-args args)
         (tagstr (and tags erc-sqlite--include-unparsed-tags-p
                      (require 'erc-v3)
                      (erc-v3--merge-client-tags tags)))
         (parts (list cmd " " sender (if tagstr " :" ":") tagstr))
         trailing)
    (while-let ((arg (pop args)))
      (push (if (or args (not (string-search " " arg))) " " " :") parts)
      (push arg parts))
    (setq trailing (if command-args (car parts) ""))
    (make-erc-response :unparsed (apply #'concat (nreverse parts))
                       :sender sender
                       :command cmd
                       :command-args command-args
                       :contents trailing
                       :tags tags)))

(defun erc-sqlite--reconstitute-from-partial (cmd sender tags target text)
  (let* ((partial (concat ":" sender " " cmd " " target " " text))
         (tagstr (and tags erc-sqlite--include-unparsed-tags-p
                      (require 'erc-v3)
                      (erc-v3--merge-client-tags tags)))
         (unparsed (if tagstr (concat tagstr " " partial) partial))
         (msg (make-erc-response :unparsed unparsed :tags tags)))
    (erc--parse-traditional-response partial msg)))

(defun erc-sqlite--ensure-cmem-from-sender (sender)
  (pcase-let ((`(,nick ,login ,host) (erc-parse-user sender)))
    (erc-update-current-channel-member nick nick t nil nil nil nil nil
                                       host login)))

(defun erc-sqlite--inserter-create (name size range iterator reporter)
  (oclosure-lambda (erc-sqlite-inserter
                    (name name) (size size) (length size) (range range)
                    (reporter reporter) (iterator iterator)
                    (finalizer #'erc-sqlite--insert-history-line-done)
                    (locals (erc-span--populate-locals nil (current-buffer)
                                                       (erc-server-buffer))))
      (&rest _)
    (pcase-let*
        ((`(,id ,tgt-id ,span ,time ,msgid ,nick ,tags ,cmd ,uh ,text ,taintp)
          (sqlite-next iterator))
         (target (erc-target))
         (sender (if uh (concat nick "!" uh) nick))
         (tags `(,@(and tags (json-parse-string tags :object-type 'alist))
                 ,@(and msgid `((msgid . ,msgid)))
                 ,@(and time `((time . ,time)))))
         (ensure-cmem-p nil)
         (parsed (pcase-exhaustive cmd
                   ((or "PRIVMSG" "NOTICE" "PART" "TOPIC")
                    (erc-sqlite--reconstitute cmd sender tags target text))
                   ("JOIN" (erc-sqlite--reconstitute cmd sender tags target))
                   ((or "QUIT" "NICK")
                    (setq ensure-cmem-p t)
                    (erc-sqlite--reconstitute cmd sender tags text))
                   ((or "MODE" "KICK")
                    (erc-sqlite--reconstitute-from-partial cmd sender tags
                                                           target text))))
         (erc--insert-marker (erc-span--range-marker-hi range))
         ;; FIXME ensure this works when truncation hook rewrites IDs.
         (erc--msg-prop-overrides `((erc-sqlite--id . ,id)
                                    ,@erc--msg-prop-overrides))
         (erc--current-time-pair (erc-compat--iso8601-to-time time t))
         (erc-timer-hook nil) ; skip timer hooks
         (erc-auto-query nil)) ; don't create a query window
      (cl-decf length)
      (cl-assert (not taintp))
      (cl-assert (= tgt-id erc-sqlite--target-id))
      (cl-assert (= span (erc-span--range-ts-lo range)))
      (erc-span--with-locals locals ((erc-server-buffer)
                                     (erc-get-buffer name erc-server-process))
        (when ensure-cmem-p
          (erc-sqlite--ensure-cmem-from-sender sender))
        (erc-with-server-buffer
          (erc-call-hooks erc-server-process parsed)))
      (when reporter
        (progress-reporter-update reporter (- size length))))))


;;;; Backend `erc-span' interface

(cl-defmethod erc-span--inserter-finalize
  ((inserter erc-sqlite-inserter) &context (erc-sqlite--span-mode (eql t)))
  (funcall (erc-sqlite-inserter--finalizer inserter) inserter))

(cl-defmethod erc-span--inserter-get-name
  ((inserter erc-sqlite-inserter) &context (erc-sqlite--span-mode (eql t)))
  (erc-sqlite-inserter--name inserter))

(cl-defmethod erc-span--inserter-get-length
  ((inserter erc-sqlite-inserter) &context (erc-sqlite--span-mode (eql t)))
  (erc-sqlite-inserter--length inserter))

(cl-defmethod erc-span--inserter-set-timer
  ((inserter erc-sqlite-inserter) value
   &context (erc-sqlite--span-mode (eql t)))
  (setf (erc-sqlite-inserter--timer inserter) value))

(cl-defmethod erc-span--inserter-insert-one ((inserter erc-sqlite-inserter))
  (funcall inserter))


;;;; Standalone module

(define-erc-module sqlite nil
  "Standalone ERC-store backend for SQLite.  Currently experimental."
  ((if (erc--module-active-p 'v3)
       (progn
         (erc-button--display-error-notice-with-keys
          "Module `sqlite' is incompatible with module `v3'.")
         (erc-sqlite-mode -1))
     (erc-span-mode +1)
     (erc-sqlite--span-mode +1)))
  ((erc-sqlite--span-mode -1)
   (erc-span-mode -1))
  localp)

(cl-defmethod erc--send-input-lines :before
  (lines-obj &context (erc-sqlite-mode (eql t)))
  ;; Spoof `erc-response'.
  (when-let* (((erc--input-split-sendp lines-obj))
              ((not (erc--input-split-cmdp lines-obj)))
              (nick (erc-current-nick))
              (user (erc-get-server-user nick))
              (sender (erc-user-spec user))
              (target (erc-target)))
    (dolist (line (erc--input-split-lines lines-obj))
      (erc-sqlite--handle-live-target-with-trailing
       (make-erc--zresponse :sender sender
                            :command "PRIVMSG"
                            :command-args (list target line)
                            :contents line)))))

(defun erc-sqlite--backend-get-latest-id-and-count ()
  "Return cons of latest span identifier as a time string and count."
  (cl-assert (null erc-sqlite--span-id))
  (when-let*
      ((erc-sqlite--target-id)
       (q-cnt (concat "SELECT id, count FROM Spans WHERE target = ? AND"
                      " id = (SELECT MAX(id) FROM Spans WHERE target = ?1)"))
       (rv-cnt (car (sqlite-select erc-sqlite--connection q-cnt
                                   (list erc-sqlite--target-id))))
       (latest-span (erc-span--ensure-time (cons (nth 0 rv-cnt) 1000)))
       (count (min (nth 1 rv-cnt) erc-sqlite-max-fill-on-join))
       ((> count 0)))
    (cons latest-span count)))

(defun erc-sqlite--backend-get-range-time-bounds (latest-span-id count)
  "Return a cons of low and high time bounds as lisp-time objects.
Expect LATEST-SPAN-ID to be an integer from a monotonically increasing
source.  Expect COUNT to be the number of messages available from the
backend."
  (when (stringp latest-span-id)
    (setq latest-span-id (car (erc-span--ensure-time latest-span-id))))
  (when-let*
      ((q-mm (concat "SELECT MIN(time) AS old_time, MAX(time) AS new_time"
                     " FROM (SELECT time FROM Messages WHERE span = ?"
                     " ORDER BY time DESC LIMIT ?) AS LimitedSet;"))
       (rv-min-max (car (sqlite-select erc-sqlite--connection q-mm
                                       (list latest-span-id count)))))
    (cons (nth 0 rv-min-max) ; should be greater than Spans.beg
          (nth 1 rv-min-max)))) ; should match Spans.end

(defun erc-sqlite--backend-create-inserter (target start count range-node)
  "Create inserter instance and run it.
Expect TARGET to be a valid target-buffer name, SIZE to be the number of
messages to insert, and RANGE-NODE to be the active `erc-span--range'
instance."
  (cl-assert erc--target)
  (let* ((q-itr (concat "SELECT * FROM Messages WHERE span = ? AND time >= ?"
                        " ORDER BY time LIMIT ?"))
         (latest-span (car (erc-span--ensure-time
                            (erc-span--range-ts-lo range-node))))
         (iterator (sqlite-select erc-sqlite--connection q-itr
                                  (list latest-span start count) 'set))
         (reporter (and (not noninteractive)
                        (make-progress-reporter target 0 count 0))))
    (erc-sqlite--inserter-create target count range-node iterator reporter)))

(defun erc-sqlite--ensure-initial-span ()
  (cl-assert erc--target)
  (when-let*
      ((info (erc-sqlite--backend-get-latest-id-and-count))
       (span-id (car info))
       (count (cdr info))
       ((or (null erc-span--ranges)
            (null (erc-span--range-find-by-time erc-span--ranges span-id))))
       (bounds (erc-sqlite--backend-get-range-time-bounds span-id count))
       (node (erc-span--range-add (car bounds) (cdr bounds)))
       (inserter (erc-sqlite--backend-create-inserter (erc-target) (car bounds)
                                                      count node)))
    (when (bound-and-true-p erc-button-mode)
      (defvar erc-button--phantom-users-mode)
      (unless erc-button--phantom-users-mode
        (erc-button--phantom-users-mode +1)))
    ;; MUST run on its own stack because `erc-span--handle-live-join'
    ;; is called by `erc-server-JOIN-functions', and the playback may
    ;; include JOIN messages.
    (run-at-time 0 nil #'erc-span--run-inserter (current-buffer) inserter)))

(cl-defmethod erc-span--backfill-on-join (&context (erc-sqlite-mode (eql t)))
  (erc-span--seal-last-live)
  (erc-sqlite--ensure-initial-span))


(provide 'erc-sqlite)

;;; erc-sqlite.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
