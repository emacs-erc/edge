;;; erc-eldoc.el --- eldoc integration for ERC -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Free Software Foundation, Inc.
;;
;; This file is part of GNU Emacs.
;;
;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO delete this and/or replace with something nice.

;; This is a throwaway module meant to demo some IRCv3 features like
;; live account awareness and live away status.

;;; Code:

(require 'erc-v3)
(require 'erc-button)

;; Can't use `erc-format-@nick' because `erc-get-user-mode-prefix'
;; relies on global hash table which may no longer contain the entry
;; we want

(defun erc-eldoc-nick-at-point (callback &rest _ignored)
  "Document nick at point.
When point contains a nick added by `erc-button-add-nickname-buttons' or
a name in a 353 user list, call CALLBACK.  For non-eldoc use, see
`erc-nick-at-point'."
  (when-let* ((data (get-text-property (point) 'erc-data))
              (server-user (nth 1 data))
              ((or (eq (get-text-property (point) 'erc-callback)
                       'erc-button--perform-nick-popup)
                   (pcase (get-text-property (point) 'font-lock-face)
                     ((and (pred consp) v)
                      (memq 'erc-notice-face (flatten-list v)))
                     ('erc-notice-face t)))))
    (let* ((cuser (nth 2 data))
           (statusp (not (zerop (erc-channel-user-status cuser))))
           (account (or (erc-server-user-account server-user) "*"))
           (nick (erc-server-user-nickname server-user))
           (partedp (not (erc-get-channel-user nick)))
           (pfx (if statusp (concat (erc-v3--format-prefix cuser t) " ") ""))
           (info (erc-v3--assemble-server-user-info-string server-user))
           (noti-fa (if partedp 'erc-error-face 'erc-notice-face)))
      (when pfx
        (put-text-property 0 (length pfx) 'face
                           (if partedp 'erc-error-face 'erc-nick-prefix-face)
                           pfx))
      (put-text-property 0 (length info) 'face noti-fa
                         (if (string-empty-p info) "?" info))
      (funcall callback (concat pfx info)
               :thing account
               :face noti-fa))))

;;;###autoload(autoload 'erc-eldoc-mode "erc-eldoc" nil t)
(define-erc-module eldoc nil
  "Toy eldoc mode integration demoing some IRCv3 features for ERC.
To use, add the symbol `eldoc' to `erc-modules' or run M-x
erc-eldoc-mode RET in some target buffer."
  ((require 'eldoc)
   (eldoc-add-command 'erc-button-previous)
   (eldoc-add-command 'erc-button-next)
   (when erc--target
     (add-hook 'eldoc-documentation-functions #'erc-eldoc-nick-at-point nil t)
     (eldoc-mode +1)))
  ((eldoc-remove-command 'erc-button-previous)
   (eldoc-remove-command 'erc-button-next)
   (when erc--target
     (remove-hook 'eldoc-documentation-functions #'erc-eldoc-nick-at-point t)
     (eldoc-mode -1)))
  localp)

;; FIXME use or lose (maybe `erc-once-with-server-event' on 329, dunno)

(defun erc-eldoc-maybe-whine-about-requirements ()
  "Maybe print notice saying requirements unsatisfied."
  (erc-with-server-buffer
    (unless (< 1 (seq-count #'identity (list erc-v3--extended-join
                                             erc-v3--account-notify
                                             erc-v3--whox
                                             erc-v3--account-tag)))
      (erc-display-error-notice
       nil "Required IRCv3 support for eldoc module not found."))))

(provide 'erc-eldoc)

;;; erc-eldoc.el ends here
