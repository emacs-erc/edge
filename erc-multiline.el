;;; erc-multiline.el -- Multiline IRCv3 extension for ERC  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; TODO:
;;
;; - Add option to collapse excess lines on receipt and maybe allow
;;   toggling between three levels.

;;; Code
(require 'erc-batch)

(defface erc-multiline-body-face
  '((t :inherit fixed-pitch))
  "Default face for multiline messages."
  :group 'erc-v3
  :group 'erc-faces)

(defface erc-multiline-meta-line-face
  '((t :inherit font-lock-comment-face))
  "Default face for markup-style begin/end lines."
  :group 'erc-v3
  :group 'erc-faces)

(defcustom erc-multiline-hide-meta-lines t
  "Whether to hide Org-like bookends around multiline messages.
These appear as the Org-style markup delimiters
\"#+BEGIN_MULTILINE\" and \"#+END_MULTILINE\".  When the `button'
module is active, the first serves as a visible handle for
\\`TAB'ing between buttons.  When this option is nil, \\`TAB'ing
instead lands on the beginning of the visible message body."
  :group 'erc-v3
  :type 'boolean
  :package-version '(ERC . "5.7")) ; FIXME sync on release

(defvar erc-button-mode)
(defvar erc-button-alist)
(defvar erc-button-keymap)
(defvar erc-labeled-response--inhibit-label)

(declare-function truncate-string-ellipsis "mule-util" nil)
(declare-function erc-match--toggle-hidden "erc-match" (prop arg))
(declare-function erc-labeled-response--compose-tags
                  "erc-labeled-response" nil)

(defvar erc-multiline--button-spec
  `(,(rx "#+" (| "BEGIN" "END") "_MULTILINE")
    0 erc-multiline--buttonize erc-multiline--button))

(defvar-keymap erc-v3--multiline-mode-map
  :doc "Keymap for ERC's `multiline' IRCv3 extension."
  "<remap> <erc-send-current-line>" #'erc-multiline--send-current-line)

;;;###autoload(put 'draft/multiline 'erc--feature 'erc-multiline)
(erc-v3--define-capability multiline
  :depends '(batch)
  :supports '(labeled-response)
  :aliases '(draft/multiline)
  :enablep #'erc-multiline--extract-cap-values
  :slots ((max-bytes nil :type (or null integer))
          (max-lines nil :type (or null integer)))
  :keymap erc-v3--multiline-mode-map
  (if erc--target
      (if erc-v3--multiline
          (progn
            (add-function :around (local 'erc-send-input-line-function)
                          #'erc-multiline--send-input-line)
            (add-to-invisibility-spec 'multiline)
            (when erc-multiline-hide-meta-lines
              (add-to-invisibility-spec 'multiline-meta)))
        (remove-function (local 'erc-send-input-line-function)
                         #'erc-multiline--send-input-line)
        (mapc #'remove-from-invisibility-spec '(multiline multiline-meta)))
    (if erc-v3--multiline
        (when (or erc-button-mode (memq 'button erc-modules))
          (cl-pushnew erc-multiline--button-spec erc-button-alist))
      (when erc-button-mode
        (setq erc-button-alist
              (delq erc-multiline--button-spec erc-button-alist))))))

(defun erc-multiline--buttonize (from to fun _ &optional _ _)
  (when erc-v3--multiline
    (add-text-properties from to
                         (list 'erc-callback fun 'keymap erc-button-keymap))))

(defun erc-multiline--extract-cap-values (object)
  (and (erc-v3--inactive-p object)
       (erc-v3--cap-to-slot-values object (lambda (_ v)
                                            (string-to-number v)))))

(cl-defstruct (erc-multiline--batch (:include erc-batch--info))
  "Data for processing a single `multiline' batch response."
  ( marker (make-marker) :type marker
    :documentation "Marker to remember place at time of arrival.")
  ( first nil :type (or null erc-response) ; or NOTICE
    :documentation "Parsed but undecoded `erc-response' obj of 1st PRIVMSG.")
  ( parts nil :type (list-of string)
    :documentation "List of raw byte strings minus line endings.")
  ( parsed nil :type (or null erc-response)
    :documentation "Original `erc-response' object from batch opener."))

;; FIXME implement progress reporter
(defun erc-multiline--create-info (parsed ref rest type)
  (let* ((target (pop rest))
         (info (make-erc-multiline--batch :type type
                                          :ref ref
                                          :name target
                                          :parsed parsed)))
    (erc-batch--create-queue info)
    (with-current-buffer (erc-batch--ensure-target-buffer parsed info)
      (set-marker (erc-multiline--batch-marker info) erc-insert-marker))
    info))

(cl-defmethod erc-batch--create-info ((_ (eql draft/multiline))
                                      parsed ref rest)
  (erc-multiline--create-info parsed ref rest 'draft/multiline))

(cl-defmethod erc-batch--create-info ((_ (eql multiline)) parsed ref rest)
  (erc-multiline--create-info parsed ref rest 'multiline))

;; We could optionally try to fontify the body based on MIME type,
;; similar to what `mm-display-inline-fontify' does.
(defvar erc-multiline--message-bookends
  '("#+BEGIN_MULTILINE" . "#+END_MULTILINE")
  "String sentinels bounding a multiline message body.")

(defvar erc-multiline--active-content nil)

;; We could split this in two and put the insertion business at the
;; head of `erc-insert-modify-hook' and the `line-prefix' stuff at the
;; very end.  That would allow third-party code access to the content.
(defun erc-multiline--on-insert ()
  "Inject pre-formatted multiline text between placeholder.
Arrange for the entire message body and bookend decorations to
have a `field' property whose value is `erc-multiline', as
well as a text property of the same name whose value moves from
`begin' to `body' to `end'."
  (goto-char (point-min))
  (when-let* ((beg (text-property-any (point) (point-max)
                                      'erc-multiline 'begin))
              (end (text-property-any (goto-char beg) (pos-eol)
                                      'erc-multiline 'end))
              (line-end (or (next-single-property-change end 'erc-multiline)
                            (pos-eol))))
    (goto-char end)
    (add-text-properties beg line-end
                         '( font-lock-face erc-multiline-meta-line-face
                            field erc-multiline))
    (insert-and-inherit erc-multiline--active-content "\n")
    (setq line-end (next-single-property-change (point) 'erc-multiline))
    (add-text-properties (1+ end) (1- (point))
                         '( erc-multiline body
                            font-lock-face erc-multiline-body-face))
    ;; Final newline of body is part of #+END_MULTILINE
    (put-text-property (1- (point)) (point) 'erc-multiline 'end)
    (put-text-property (1- (point)) line-end 'invisible 'multiline-meta)
    (put-text-property beg (1+ end) 'invisible 'multiline-meta)
    (when-let* ((val (and (get-text-property end 'line-prefix)
                          (get-text-property end 'wrap-prefix))))
      ;; We could use a temporary marker for this instead.
      (put-text-property (1+ end) line-end 'line-prefix val))))

;; This is obviously roundabout and wasteful.  We should instead
;; provide a separate utility for decoding that operates on any number
;; of strings.
(defun erc-multiline--forge-response (parsed-opener info)
  "Create an `erc-response' object from PARSED-OPENER and INFO.
Return the decoded, concatenated multiline text body and the new
object as a cons cell.  Permanently reverse the order of members
of the `erc-multiline--batch-parts' field."
  (let* ((text (string-join (nreverse (erc-multiline--batch-parts info))))
         (sentinel (concat (car erc-multiline--message-bookends)
                           (cdr erc-multiline--message-bookends)))
         (parsed-msg (copy-erc-response (erc-multiline--batch-first info)))
         (dex (1- (length (erc-response.command-args parsed-msg)))))
    ;; A single line split across multiple messages won't begin with a
    ;; newline.
    (unless (or (zerop (length text)) (= ?\n (aref text 0)))
      (setq text (concat "\n" text)))
    (setf
     (nth dex (erc-response.command-args parsed-msg)) (concat sentinel text)
     (erc-response.tags parsed-msg) (erc-response.tags parsed-opener)
     (erc-response.contents parsed-msg) (concat sentinel text))
    ;; Decoding returns a new string.
    (erc-decode-parsed-server-response parsed-msg)
    (setf text (substring (erc-response.contents parsed-msg) (length sentinel))
          sentinel (substring (erc-response.contents parsed-msg)
                              0 (length sentinel))
          (nth dex (erc-response.command-args parsed-msg)) sentinel
          (erc-response.contents parsed-msg) sentinel)
    ;; Our means of detection depends on text props, which is a
    ;; little risky because third-party code can affect the outcome.
    (put-text-property 0 (length (car erc-multiline--message-bookends))
                       'erc-multiline 'begin sentinel)
    (put-text-property (length (car erc-multiline--message-bookends))
                       (length sentinel) 'erc-multiline 'end sentinel)
    (cons text parsed-msg)))

(cl-defmethod erc-span--inserter-insert-one ((info erc-multiline--batch))
  (pcase-let* ((`(,tags . ,latest) (erc-batch--remove info))
               (beg (string-search " " latest))
               (msg (make-erc-response :unparsed latest :tags tags)))
    (unless (erc-multiline--batch-first info)
      (setf (erc-multiline--batch-first info) msg))
    (erc--parse-traditional-response (substring latest (1+ beg)) msg)
    (unless (assq (intern
                   (concat (symbol-name (erc-batch--info-type info))
                           "-concat"))
                  tags)
      (push "\n" (erc-multiline--batch-parts info)))
    (push (erc-response.contents msg)
          (erc-multiline--batch-parts info))))

(cl-defmethod erc-span--inserter-finalize ((info erc-multiline--batch))
  (let* ((erc-insert-modify-hook
          `(,@erc-insert-modify-hook erc-multiline--on-insert))
         (parent (erc-batch--info-parent info))
         (marker (erc-multiline--batch-marker info))
         (erc--insert-marker (and (not parent) (marker-buffer marker)
                                  marker))
         ;; During playback, set active batch to parent temporarily,
         ;; so that `erc--insert-line' restores environment.
         (erc-span--active-inserter (if (erc-batch--chathistory-p parent)
                                        parent
                                      erc-span--active-inserter))
         (parsed (erc-multiline--batch-parsed info))
         (msg (erc-multiline--forge-response parsed info))
         (erc--insert-line-function #'insert-before-markers)
         (erc-multiline--active-content (pop msg)))
    (erc-handle-parsed-server-response erc-server-process msg))
  (set-marker (erc-multiline--batch-marker info) nil)
  (erc-batch--common-teardown info))

(cl-defmethod erc-batch--handle-enqueued ((info erc-multiline--batch))
  (erc-span--run-inserter (current-buffer) info))

(defun erc-multiline--get-body-bounds ()
  (when-let* (((eq 'erc-multiline (field-at-pos (point))))
              (beg (text-property-not-all (field-beginning (point))
                                          (field-end (point))
                                          'erc-multiline 'begin))
              (end (text-property-any beg (field-end (point))
                                      'erc-multiline 'end)))
    (cons beg end)))

(defun erc-multiline--fold ()
  ;; Subtracting bookend lengths would give the same result.
  (require 'mule-util)
  (when-let* ((end (erc-multiline--get-body-bounds))
              (beg (save-excursion
                     (let ((b (goto-char (pop end)))
                           (inhibit-read-only t))
                       (skip-syntax-forward "-")
                       (unless (= (point) b)
                         (if (= (count-lines b (pos-eol)) 1)
                             (goto-char b)
                           (put-text-property b (point) 'display ""))))
                     (+ (point) (min 30 (- (pos-eol) (pos-bol))))))
              (o (make-overlay beg end nil)))
    (overlay-put o 'isearch-open-invisible #'delete-overlay)
    (overlay-put o 'isearch-open-invisible-temporary
                 (let (disp)
                   (lambda (o hide-p)
                     (if hide-p
                         (progn (overlay-put o 'display disp)
                                (overlay-put o 'invisible 'multiline))
                       (setq disp (overlay-get o 'display))
                       (overlay-put o 'display nil)
                       (overlay-put o 'invisible nil)))))
    (overlay-put o 'invisible 'multiline)
    (overlay-put o 'display (format "%s(%d)" (truncate-string-ellipsis)
                                    (count-lines beg end)))))

(defun erc-multiline--button (&rest _)
  "Call `erc-multiline-toggle-fold' on \\`RET' or `mouse-2'."
  (erc-multiline-toggle-fold))

(defun erc-multiline-toggle-fold ()
  "Show or hide multiline message at point."
  (interactive)
  (when (eq 'erc-multiline (field-at-pos (point)))
    (if-let* ((all (overlays-in (field-beginning (point)) (field-end (point))))
              (os (seq-filter (lambda (o)
                                (eq (overlay-get o 'invisible) 'multiline))
                              all))
              (inhibit-read-only t))
        (dolist (o os)
          (remove-text-properties (field-beginning (point)) (overlay-start o)
                                  '(display nil))
          (delete-overlay o))
      (erc-multiline--fold))))

(defun erc-multiline-copy-message-as-kill ()
  (interactive)
  (when-let* ((end (erc-multiline--get-body-bounds))
              (beg (pop end)))
    (copy-region-as-kill beg end)))

(defvar erc-multiline--send-p nil)

;; At least one server (ergo-v2.11.1) treats `max-lines' as the total
;; number of lines in the batch, including continuation lines tagged
;; with `multiline-concat', rather than logical (printed) lines.
;;
;; The same server treats `max-bytes' as the total payload, i.e., the
;; sum of all trailing message parameters in the batch.  IOW, it
;; disregards the usual routing-oriented syntax elements, like
;;
;;   :sender COMMAND #target :
;;
;; If the latter assumption proves to be wrong, we can do something
;; like add the *encoded* string length to the product of the length
;; of `erc--input-split-lines' and `erc-split-line-length', which is
;; bound to the napkin arith in `erc-multiline--send-current-line'.

(defun erc-multiline--on-review (state)
  (cl-assert (not erc-multiline--send-p))
  (when (and erc--target
             (cdr (erc--input-split-lines state))
             (y-or-n-p "Send as multiline message?"))
    (when-let* ((max (erc-v3--multiline-max-lines erc-v3--multiline))
                (lines (erc--input-split-lines state))
                ((> (length lines) max)))
      (user-error "Too many lines (over by %d)" (- (length lines) max)))
    (when-let* ((max (erc-v3--multiline-max-bytes erc-v3--multiline))
                (rawc (erc-coding-system-for-target
                       (erc--target-string erc--target)))
                (coding (coding-system-change-eol-conversion
                         (if (consp rawc) (car rawc) rawc) 'unix))
                (encoded (with-temp-buffer
                           (set-buffer-multibyte nil) ; necessary?
                           (encode-coding-string (erc--input-split-string state)
                                                 coding nil (current-buffer))))
                ((> encoded max))
                (msg (format "Message likely too long (est. over by %d bytes). "
                             (- encoded max)))
                ((not (y-or-n-p (concat msg "Send anyway?")))))
      (user-error "Send aborted"))
    (setf (erc--input-split-cmdp state) nil)
    ;; XXX this function should only be run by this module.
    (cl-assert (not erc-multiline--send-p))
    (setq erc-multiline--send-p t
          erc--allow-empty-outgoing-lines-p t)))

(defvar erc-multiline--current-batch-id nil)

(defun erc-multiline--send-input-line (inner &rest args)
  (if (bound-and-true-p erc-v3--labeled-response)
      (let ((erc-labeled-response--inhibit-label
             erc-multiline--current-batch-id))
        (apply inner args))
    (apply inner args)))

(cl-defmethod erc--send-input-lines
  (lines-obj &context (erc--target erc--target)
             (erc-v3--multiline erc-v3--multiline)
             (erc-multiline--send-p (eql t)))
  (cl-assert erc--allow-empty-outgoing-lines-p)
  (when-let*
      ((erc--input-split-sendp (setq lines-obj
                                     (erc--run-send-hooks lines-obj)))
       (bid (erc-batch--get-client-batch-id))
       (key (erc-v3--multiline-key erc-v3--multiline))
       (concat-key (intern (format "%s-concat" key)))
       (string (erc--input-split-string lines-obj))
       (target (erc--target-string erc--target))
       (chardex 0))
    ;; Technically, `erc-display-msg' doesn't need to run when
    ;; `echo-message' is active and `labeled-response' isn't.
    (when (erc--input-split-insertp lines-obj)
      (let ((erc-multiline--active-content (concat "\n" string))
            (begin (propertize (car erc-multiline--message-bookends)
                               'erc-multiline 'begin))
            (end (propertize (cdr erc-multiline--message-bookends)
                             'erc-multiline 'end))
            (erc-send-modify-hook
             `(,@erc-send-modify-hook erc-multiline--on-insert)))
        (erc-display-msg (concat begin end))))
    (let ((erc-v3--unescaped-client-tags
           (if (bound-and-true-p erc-v3--labeled-response)
               (erc-labeled-response--compose-tags)
             erc-v3--unescaped-client-tags)))
      (erc-server-send (format "BATCH +%s %s %s" bid key target) nil target))
    (let ((erc-multiline--current-batch-id bid)
          prev)
      (dolist (line (erc--input-split-lines lines-obj))
        (let* ((next (cl-incf chardex (length line)))
               (done (>= next (length string)))
               (newp (and (not done) (= ?\n (aref string next))))
               (erc-v3--unescaped-client-tags
                (cons `(batch . ,bid) prev)))
          (setq prev (and (not newp) (not done) `((,concat-key))))
          (when newp
            (cl-incf chardex))
          (funcall erc-send-input-line-function target line nil))))
    (erc-server-send (format "BATCH -%s" bid) nil target)))

(defvar erc-multiline--default-review-functions nil)

;; TODO find out how bad it is to run nested hooks in this way.
(defun erc-multiline--finish-default-review (state)
  "For multiline batches, only run local review functions."
  (setq erc--input-review-functions
        (cdr erc-multiline--default-review-functions))
  (let* ((locals (car erc-multiline--default-review-functions))
         (erc--input-review-functions (if erc-multiline--send-p
                                          (remq t locals)
                                        (or locals
                                            erc--input-review-functions))))
    (run-hook-with-args 'erc--input-review-functions state)))

;; The userhost and target consist entirely of ASCII chars, so
;; encoding them to get their outgoing byte length is useless.
(defun erc-multiline--send-current-line ()
  "Optionally send multiline message in target buffers.
On detecting multiline input, ask users whether to send the whole
thing as a single IRCv3 multiline message."
  (interactive)
  (let* ((user (erc-get-server-user (erc-current-nick)))
         (target (and erc--target (erc--target-string erc--target)))
         (erc-split-line-length
          (if (and target user)
              (- 512
                 1 ; ":<sender> " added by server
                 (length (erc-user-spec user))
                 9 ; " PRIVMSG <target>"
                 (length (erc--target-string erc--target))
                 12) ; " :" (+ trailing) + "\r\n" + safety
            erc-split-line-length)))
    ;; This must be a `let*'! `erc-multiline--on-review' sets the
    ;; `erc-multiline--send-p' and `erc--allow-empty-outgoing-lines-p'
    ;; flags to non-nil when this module's implementation of
    ;; `erc--send-input-lines' should run.
    (let* ((erc-multiline--send-p nil)
           (erc--allow-empty-outgoing-lines-p nil)
           (erc-multiline--default-review-functions
            (cons (and (local-variable-p 'erc--input-review-functions)
                       erc--input-review-functions)
                  (default-toplevel-value 'erc--input-review-functions)))
           (erc--input-review-functions
            (if target
                `(erc--split-lines
                  erc-multiline--on-review
                  erc-multiline--finish-default-review)
              erc--input-review-functions)))
      (call-interactively #'erc-send-current-line))))

(defun erc-multiline-toggle-meta-lines (arg)
  "Toggle meta-line bookend visibility."
  (interactive "P")
  (require 'erc-match)
  (erc-match--toggle-hidden 'multiline-meta arg))

(defun erc-multiline--expalin-recalling-input ()
  (and (bound-and-true-p erc-ring-mode)
       (concat " If possible, attempt to identify the offending input"
               " by running \\[erc-previous-command] in the input area.")))

(cl-defmethod erc-v3--standard-replies-handle
  ((_ (eql FAIL)) (_ (eql BATCH)) (_ (eql MULTILINE_MAX_LINES)) _rest parsed)
  (let ((max-lines (if erc-v3--multiline
                       (erc-v3--multiline-max-lines erc-v3--multiline)
                     '\?)))
    (erc-button--display-error-notice-with-keys
     "Server rejected multiline message because number of lines"
     " (possibly after splitting) exceeded %s."
     " Max supported originally advertised was %s."
     (erc-multiline--expalin-recalling-input)
     " Please report this as a bug with \\[erc-bug]."
     (string-to-number (erc-response.contents parsed)) max-lines)))

(cl-defmethod erc-v3--standard-replies-handle
  ((_ (eql FAIL)) (_ (eql BATCH)) (_ (eql MULTILINE_MAX_BYTES)) _rest parsed)
  (let ((max-bytes (if erc-v3--multiline
                       (erc-v3--multiline-max-bytes erc-v3--multiline)
                     '\?)))
    (erc-button--display-error-notice-with-keys
     "Server rejected multiline message because it exceeded %s bytes."
     " Max supported originally advertised was %s."
     (erc-multiline--expalin-recalling-input)
     " Please report this as a bug with \\[erc-bug]."
     (string-to-number (erc-response.contents parsed)) max-bytes)))

(provide 'erc-multiline)
;;; erc-multiline.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
