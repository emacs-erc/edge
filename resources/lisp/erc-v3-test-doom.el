;;; erc-v3-test-doom.el --- Doom -*- lexical-binding: t; coding: utf-8; -*-

;;; Commentary:
;;
;;; Code:

(require 'ert)
(require 'erc-v3)
(require 'erc-d-t)

(defvar erc-v3-test-doom-user (getenv "ERC_V3_TEST_DOOM_USER"))
(defvar erc-v3-test-doom-password (getenv "ERC_V3_TEST_DOOM_PASSWORD"))

(ert-deftest erc-v3-test-doom-connect ()
  (unless erc-v3-test-doom-user (ert-skip "User missing"))
  (unless erc-v3-test-doom-password (ert-skip "Password missing"))
  (unless noninteractive (erc-toggle-debug-irc-protocol))
  (message "locate-library: %s" (list :erc (locate-library "erc")
                                      :v3 (locate-library "erc-v3")))
  (let* (erc-server-auto-reconnect
         (nick erc-v3-test-doom-user)
         (erc-email-userid nick)
         (erc-modules (cons 'v3 erc-modules))
         (erc-v3-extensions (cons 'sasl erc-v3-extensions))
         (expect (erc-d-t-make-expecter))
         (server-buffer (erc-tls :server "testnet.ergo.chat"
                                 :port 6697
                                 :nick nick
                                 :user nick
                                 :password erc-v3-test-doom-password
                                 :full-name "ERC v3 CI Bot"))
         (failsafe
          (run-at-time 30 nil (lambda ()
                                (with-current-buffer server-buffer
                                  (when (erc-server-buffer-live-p)
                                    (delete-process erc-server-process)))))))

    (with-current-buffer (erc-d-t-wait-for 30 (get-buffer "ErgoTestnet"))

      (ert-info ("Get session info")
        (funcall expect 10 (concat "You are now logged in as " nick))
        (funcall expect 10 "User modes for")
        (erc-cmd-WHOAMI)
        (funcall expect 10 (concat nick " is logged in as " nick))
        (funcall expect 10 erc-prompt))

      (ert-info ("Quit")
        (erc-cmd-QUIT nil)
        (erc-d-t-wait-for 10 "Server proc dies"
          (not (erc-server-process-alive server-buffer)))
        (erc-d-t-search-for 10 "ERC finished")))
    (cancel-timer failsafe)))

;;; erc-v3-test-doom.el ends here
