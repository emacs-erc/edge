;;; erc-v3-test-socks.el --- Connect to Libera over Tor -*- lexical-binding: t -*-

;;; Commentary:

;; This used to test various hacks related to getting socks.el to play nice
;; with TLS.  See e6c6271 "Update blacklist for Doom job" or earlier.

;; Code:

(require 'ert)
(require 'erc-d-t)
(require 'erc-v3)
(require 'erc-sasl)
(require 'socks)

(defvar erc-v3-test-socks-tor-service
  (and-let* ((present (getenv "ERC_V3_TEST_SOCKS"))
             (parts (split-string present ":")))
    (list (car parts) (string-to-number (cadr parts))))
  "Tor SOCKS service as a TCP host:port pair.")

(defvar erc-v3-test-socks-tempdir
  (file-name-as-directory
   (or (getenv "ERC_V3_TEST_TEMPDIR")
       (expand-file-name "erc-v3-test" temporary-file-directory))))

(defvar erc-v3-test-socks-libera-ident (getenv "ERC_V3_TEST_LIBERA_IDENT"))
(defvar erc-v3-test-socks-libera-cert (getenv "ERC_V3_TEST_LIBERA_CERT"))
(defvar erc-v3-test-socks-libera-onion-domain
  "libera75jm6of4wxpxt4aynol3xjmbtxgfyjpu34ss4d7r7q2v5zrpyd.onion")

(defvar erc-v3-test-socks--report
  (and-let* ((head-summary (getenv "HEAD_SUMMARY"))
             (branch (getenv "CI_COMMIT_BRANCH"))
             (desc (getenv "CI_COMMIT_TITLE")))
    (format "\u2771\u2771 \002%s\002 %s %s \u25cf %s"
            branch emacs-version (string-trim-right head-summary) desc)))

(defmacro erc-v3-test-with-tempdir (name &rest body)
  "Create directory NAME and run BODY in it."
  (declare (indent 1))
  (let ((actual (make-symbol "actual"))
        (well-known (make-symbol "well-known")))
    `(progn
       (should-not (member 'v3 erc-modules))
       (should (directory-name-p erc-v3-test-socks-tempdir))
       (should (string-prefix-p temporary-file-directory
                                erc-v3-test-socks-tempdir))
       (unless (file-exists-p erc-v3-test-socks-tempdir)
         (make-directory erc-v3-test-socks-tempdir))
       (let* ((default-directory erc-v3-test-socks-tempdir)
              (,well-known (expand-file-name ,name))
              (,actual (make-temp-file (concat
                                        ,well-known
                                        (format-time-string "-%Y%m%d-%H%M%S-"))
                                       t)))
         (should (file-name-absolute-p ,actual))
         (make-symbolic-link (file-name-base ,actual) ,name t)
         (let (erc-v3-extensions
               erc-debug-irc-protocol
               ;;
               (default-directory (file-name-as-directory ,actual))
               (erc-modules (cons 'v3 (remq 'autojoin erc-modules)))
               ;; XXX this doesn't work when set as global either
               (kill-buffer-query-functions
                (cons (lambda ()
                        (should-not (string= (buffer-name) "*erc-protocol*"))
                        (should (not view-mode))
                        t)
                      kill-buffer-query-functions)))
           (unwind-protect ,@body
             (when noninteractive
               (when (and (boundp 'trace-buffer) (get-buffer trace-buffer))
                 (with-current-buffer trace-buffer
                   (message "%S" (buffer-string))
                   (kill-buffer)))
               (erc-d-t-kill-related-buffers))))))))

(ert-deftest erc-open-socks-tls-stream ()
  :tags '(io external socks)
  (unless (zerop (ert-stats-completed-unexpected ert--current-run-stats))
    (ert-fail "Preempted by a prior failure"))
  (unless erc-v3-test-socks-tor-service (ert-skip "SOCKS service missing"))
  (unless erc-v3-test-socks-libera-ident (ert-skip "User ident missing"))
  (unless erc-v3-test-socks-libera-cert (ert-skip "User cert missing"))

  (erc-v3-test-with-tempdir "tor-socks-connect"
    (should-not (file-exists-p "cert.pem"))

    (let* ((erc-server-auto-reconnect nil)
           (socks-password "")
           (target "##erc-v3-ci")
           (nick erc-v3-test-socks-libera-ident)
           (cert-file (if (file-exists-p erc-v3-test-socks-libera-cert)
                          (expand-file-name erc-v3-test-socks-libera-cert)
                        (with-temp-file "cert.pem"
                          (insert erc-v3-test-socks-libera-cert))
                        (expand-file-name "cert.pem")))
           (socks-server `("tor" ,@erc-v3-test-socks-tor-service 5))
           (erc-v3-extensions '(sasl))
           (erc-sasl-mechanism 'external)
           (erc-server-connect-function #'erc-open-socks-tls-stream)
           (expect (erc-d-t-make-expecter)))

      (ert-info ("Connect")
        (with-current-buffer
            (erc-tls :server erc-v3-test-socks-libera-onion-domain
                     :port 6697
                     :nick nick
                     :user nick
                     :full-name nick
                     :client-certificate (list cert-file cert-file))
          (funcall expect 20 '(| "End of /MOTD command" "changed mode for"))))

      (ert-info ("Get session info")
        (with-current-buffer (erc-d-t-wait-for 20 (get-buffer "Libera.Chat"))
          (erc-cmd-WHOAMI)
          (funcall expect 10 (concat "@gateway/tor-sasl/" nick))
          (erc-cmd-JOIN target)))

      (ert-info ("Join #chan")
        (with-current-buffer (erc-d-t-wait-for 20 (get-buffer target))
          (funcall expect 20 " was created on")
          (when erc-v3-test-socks--report
            (erc-cmd-NOTICE target erc-v3-test-socks--report))))

      (ert-info ("Quit")
        (with-current-buffer "Libera.Chat"
          (erc-cmd-QUIT "")
          (funcall expect 10 "ERC finished"))))))


(provide 'erc-v3-test-socks)
;;; erc-v3-test-socks.el ends here
