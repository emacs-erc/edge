From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Mon, 26 Jul 2021 05:40:59 -0700
Subject: [PATCH 22/39] [5.x] Clearly define erc-server-user info field as an
 alist

FIXME add news entry explaining change in erc-server-user schema.

* lisp/erc/erc-backend.el (erc-server-317): Drop nonstandard usage of
`info' slot for misc informational message that's never recalled for
later use.  This is printed to the buffer anyway, so there's no reason
to store degenerate data in the table.  Instead, preserve structured
data as `idle' entry in erc-server-user-info alist.

* lisp/erc/erc-common.el (erc-server-user): Redefine rarely used,
confusing `info' slot, formerly an opaque value, and it repurpose as
an alist.  In the handful of instances where it was used, it was
largely misused.

* lisp/erc/erc-speedbar.el (erc-speedbar-italicize-away,
erc-speedbar--italicize-away): Add var and function to facilitate
italicizing of away users.
(erc-speedbar--expand-info): New function to optionally show various
items from the `erc-server-user' `info' alist.
(erc-speedbar-expand-user): Appeal to `erc-speedbar--expand-info' for
displaying info-derived tags.
(erc-speedbar--emulate-sidebar): Conditionally wrap
`erc-speedbar--nick-face-function'.

* lisp/erc/erc.el (erc-add-server-user): Add optional `info' argument
and maybe update `info' field before committing user.
(erc--server-user-away-p): Add function to extract
away field from `info' slot of `erc-server-user'.
(erc--server-user-info-collect-p, erc--ensure-server-user-info): New
function and internal flag variable to massage obsolete values to be
added to the `info' field of an `erc-server-user' object.
(erc-update-user): Take special pains to shoehorn `info' into an alist
and emit a warning if necessary.

* test/lisp/erc/erc-tests.el (erc--ensure-server-user-info): New test.
(Bug#49860)
---
 lisp/erc/erc-backend.el    |  2 +-
 lisp/erc/erc-common.el     |  6 ++++-
 lisp/erc/erc-speedbar.el   | 36 ++++++++++++++++++++++----
 lisp/erc/erc.el            | 49 ++++++++++++++++++++++++++++++++---
 test/lisp/erc/erc-tests.el | 53 ++++++++++++++++++++++++++++++++++++++
 5 files changed, 135 insertions(+), 11 deletions(-)

diff --git a/lisp/erc/erc-backend.el b/lisp/erc/erc-backend.el
index 5806da3492e..51b1b3cc909 100644
--- a/lisp/erc/erc-backend.el
+++ b/lisp/erc/erc-backend.el
@@ -2547,7 +2547,7 @@ erc--with-isupport-data
                  (format-time-string erc-server-timestamp-format
                                      (string-to-number on-since))))
     (erc-update-user-nick nick nick nil nil nil
-                          (and time (format "on since %s" time)))
+                          (cons 'idle time))
     (if time
         (erc-display-message
          parsed 'notice 'active 's317-on-since
diff --git a/lisp/erc/erc-common.el b/lisp/erc/erc-common.el
index f5d1a34b42b..456c4280f70 100644
--- a/lisp/erc/erc-common.el
+++ b/lisp/erc/erc-common.el
@@ -80,7 +80,11 @@ erc-input
 
 (cl-defstruct (erc-server-user (:type vector) :named)
   ;; User data
-  nickname host login full-name info
+  nickname host login full-name
+  ;; `info' was repurposed to accommodate forthcoming user metadata
+  ;; extensions.  It's now an alist of conses whose keys are compared
+  ;; `eql'.  See `erc--ensure-server-user-info' for fallback behavior.
+  (info nil :type (list-of cons))
   ;; Buffers
   (buffers nil))
 
diff --git a/lisp/erc/erc-speedbar.el b/lisp/erc/erc-speedbar.el
index 0ae5ebb1641..acb206dcdb3 100644
--- a/lisp/erc/erc-speedbar.el
+++ b/lisp/erc/erc-speedbar.el
@@ -298,6 +298,18 @@ erc-speedbar--highlight-self-and-ops
                erc-button-nickname-face)
           'erc-default-face))))
 
+(defvar erc-speedbar-italicize-away t
+  "Show channel users who are away in italics.")
+
+(defun erc-speedbar--italicize-away (orig buffer user cuser)
+  (let ((others (funcall orig buffer user cuser)))
+    (if-let* ((info (erc-server-user-info user))
+              ((assq 'away info)))
+        (if others
+            (cons 'erc-italic-face (ensure-list others))
+          'erc-italic-face)
+      others)))
+
 (defun erc-speedbar--on-click (nick sbtoken _indent)
   ;; 0: finger, 1: name, 2: info, 3: buffer-name
   (with-current-buffer (nth 3 sbtoken)
@@ -353,6 +365,20 @@ erc-speedbar-update-channel
 	(speedbar-delete-subblock 1)
 	(erc-speedbar-expand-channel "+" buffer 1)))))
 
+(defun erc-speedbar--expand-info (info indent)
+  "Create a tag from items in INFO that have a `erc-speedbar-tag' prop.
+Expect INFO to be an alist associating symbols with opaque
+objects.  If the property is a function, call it with the item's
+key and value and also the indent level."
+  (pcase-dolist (`(,key . ,value) info)
+    (when-let* ((fn (get key 'erc-speedbar-tag))
+                ;; FIXME figure out the right way to do this
+                ;; using the `speedbar' API.
+                (tag (if (functionp fn)
+                         (funcall fn key value indent)
+                       (format "%s: %s" key value))))
+      (speedbar-make-tag-line nil nil nil nil tag nil nil nil (1+ indent)))))
+
 (defun erc-speedbar-expand-user (text token indent)
   (cond ((string-search "+" text)
 	 (speedbar-change-expand-button-char ?-)
@@ -373,10 +399,7 @@ erc-speedbar-expand-user
 		  name nil nil nil
 		  (1+ indent)))
 	       (when info
-		 (speedbar-make-tag-line
-		  nil nil nil nil
-		  info nil nil nil
-		  (1+ indent)))))))
+                 (erc-speedbar--expand-info info indent))))))
 	((string-search "-" text)
 	 (speedbar-change-expand-button-char ?+)
 	 (speedbar-delete-subblock indent))
@@ -485,7 +508,10 @@ erc-speedbar--emulate-sidebar
   (when (memq 'nicks erc-modules)
     (with-current-buffer speedbar-buffer
       (add-function :around (local 'erc-speedbar--nick-face-function)
-                    #'erc-speedbar--compose-nicks-face))))
+                    #'erc-speedbar--compose-nicks-face)))
+  (when erc-speedbar-italicize-away
+    (add-function :around (local 'erc-speedbar--nick-face-function)
+                  #'erc-speedbar--italicize-away '((depth . 50)))))
 
 (defun erc-speedbar--handle-delete-frame (event)
   "Disable the nickbar if EVENT is deleting the proxy frame."
diff --git a/lisp/erc/erc.el b/lisp/erc/erc.el
index b6382448e61..c47137fbccf 100644
--- a/lisp/erc/erc.el
+++ b/lisp/erc/erc.el
@@ -547,11 +547,13 @@ erc--casemapping-rfc1459
     (aset cup ?~ ?~)
     tbl))
 
-(defun erc-add-server-user (nick user)
+(defun erc-add-server-user (nick user &optional info)
   "This function is for internal use only.
-
+Optionally massage INFO with a la carte updates and deletions.
 Adds USER with nickname NICK to the `erc-server-users' hash table."
   (erc-with-server-buffer
+    (when info
+      (erc--ensure-server-user-info user info))
     (puthash (erc-downcase nick) user erc-server-users)))
 
 (defvar erc--decouple-query-and-channel-membership-p nil
@@ -7338,6 +7340,40 @@ erc-update-user-nick
   (erc-update-user (erc-get-server-user nick) new-nick
                    host login full-name info))
 
+(defun erc--server-user-away-p (user)
+  "Return a possibly empty away message when USER is away.
+Expect USER to be a `erc-server-user' object."
+  (alist-get 'away (erc-server-user-info user)))
+
+(defvar erc--server-user-info-collect-p nil
+  "Escape hatch for pre-ERC 5.7 `erc-server-user-info' interface.
+When non-nil, ERC adds non-list `info' slot update values to a
+list of existing ones stored in the `info' item in the
+`erc-server-user-info' alist.  Otherwise, it replaces the current
+`info' alist value.") ; FIXME sync version on release
+
+(defun erc--ensure-server-user-info (user info)
+  "Normalize INFO to conform to `info' slot of `erc-server-user'.
+When INFO is a list whose CAR is a symbol, update or create
+`erc-server-user-info' instead, removing matching members if
+INFO's CDR is nil."
+  (cond
+   ((consp (car-safe info))
+    (setf (erc-server-user-info user) info))
+   ((consp info)
+    (setf (alist-get (car info) (erc-server-user-info user) nil t #'eq)
+          (cdr info)))
+   ((progn (erc-button--display-error-notice-with-keys-and-warn
+            "Non-alist value given for `info' field of `erc-server-user' "
+            (erc-server-user-nickname user)
+            "; storing as alist `info' item "
+            (if erc--server-user-info-collect-p "(info %S ...)" "(info . %S)")
+            " instead." (or info (make-symbol "nil")))
+           nil))
+   (erc--server-user-info-collect-p
+    (push info (alist-get 'info (erc-server-user-info user))))
+   (t (setf (alist-get 'info (erc-server-user-info user)) info))))
+
 (defun erc-update-user (user &optional new-nick
                              host login full-name info)
   "Update user info for USER.
@@ -7346,6 +7382,10 @@ erc-update-user
 existing values for USER are used to replace the stored values in
 USER.
 
+INFO can also be a single cons pair indicating a member of the
+existing `info' field to update.  In such cases, a nil CDR will
+cause the member to be deleted.
+
 If, and only if, a change is made,
 `erc-channel-members-changed-hook' is run for each channel for
 which USER is a member, and t is returned."
@@ -7372,7 +7412,7 @@ erc-update-user
       (when (and info
                  (not (equal (erc-server-user-info user) info)))
         (setq changed t)
-        (setf (erc-server-user-info user) info))
+        (erc--ensure-server-user-info user info))
       (if changed
           (dolist (buf (erc-server-user-buffers user))
             (if (buffer-live-p buf)
@@ -7407,7 +7447,8 @@ erc--create-current-channel-member
                                        :full-name full-name
                                        :login login
                                        :info nil
-                                       :buffers (list (current-buffer))))))
+                                       :buffers (list (current-buffer))))
+                           info))
     (let ((cusr (erc-channel-user--make
                  :status (or status 0)
                  :last-message-time (and timep
diff --git a/test/lisp/erc/erc-tests.el b/test/lisp/erc/erc-tests.el
index dec88f70b9f..bee5cfc58d1 100644
--- a/test/lisp/erc/erc-tests.el
+++ b/test/lisp/erc/erc-tests.el
@@ -4184,4 +4184,57 @@ erc-retrieve-catalog-entry
     ;; Terminates.
     (should-not (erc-retrieve-catalog-entry 'ghi (intern "test0")))))
 
+(ert-deftest erc--ensure-server-user-info ()
+  (with-current-buffer (get-buffer-create "*erc-tests*")
+    (erc-mode)
+    (erc--initialize-markers (point) nil)
+    (should-not erc--server-user-info-collect-p)
+
+    (let ((user (make-erc-server-user :nickname "tester" :login "testeroni"
+                                      :info '((a . 1))))
+          (erc--server-user-info-collect-p t))
+
+      (cl-letf (((symbol-function 'display-warning) #'ignore))
+        (ert-info ("Collects when `erc--server-user-info-collect-p' non-nil")
+          (erc--ensure-server-user-info user 42)
+          (should (equal (erc-server-user-info user) '((info 42) (a . 1))))
+          (erc--ensure-server-user-info user 'foo)
+          (should
+           (equal (erc-server-user-info user) '((info foo 42) (a . 1)))))
+
+        (ert-info ("Replaces when `erc--server-user-info-collect-p' nil")
+          (setq erc--server-user-info-collect-p nil)
+          (erc--ensure-server-user-info user 'bar)
+          (should (equal (erc-server-user-info user) '((info . bar) (a . 1))))
+          (erc--ensure-server-user-info user 'nil)
+          (should (equal (erc-server-user-info user) '((info . nil) (a . 1))))
+          (should (equal (pop (erc-server-user-info user)) '(info)))))
+
+      (erc--ensure-server-user-info user '((b . 2)))
+      (should (equal (erc-server-user-info user) '((b . 2))))
+
+      (erc--ensure-server-user-info user '(b . 3))
+      (should (equal (erc-server-user-info user) '((b . 3))))
+
+      (erc--ensure-server-user-info user '(c . 4))
+      (should (equal (erc-server-user-info user) '((c . 4) (b . 3))))
+
+      (erc--ensure-server-user-info user '(b . 5))
+      (should (equal (erc-server-user-info user) '((c . 4) (b . 5))))
+
+      (erc--ensure-server-user-info user '(b . nil))
+      (should (equal (erc-server-user-info user) '((c . 4))))
+
+      (erc--ensure-server-user-info user '(c . nil))
+      (should-not (erc-server-user-info user))
+
+      (goto-char (point-min))
+      (should (search-forward "Non-alist value given" nil t))
+      (should (search-forward "Non-alist value given" nil t))
+      (should (search-forward "Non-alist value given" nil t))
+      (should (search-forward "Non-alist value given" nil t))
+      (should-not (search-forward "Non-alist value given" nil t)))
+
+    (when noninteractive (kill-buffer))))
+
 ;;; erc-tests.el ends here
-- 
2.48.1

