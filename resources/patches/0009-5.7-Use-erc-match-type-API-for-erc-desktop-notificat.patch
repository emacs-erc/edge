From 0000000000000000000000000000000000000000 Mon Sep 17 00:00:00 2001
From: "F. Jason Park" <jp@neverwas.me>
Date: Sat, 12 Oct 2024 17:44:30 -0700
Subject: [PATCH 09/39] [5.7] Use erc-match-type API for
 erc-desktop-notifications

* etc/ERC-NEWS: New section for 5.7 and new entries for the
`erc-match-type' API and options `erc-notifications-focused-context' and
`erc-desktop-notifications-skip-predicates'.
* lisp/erc/erc-desktop-notifications.el
(erc-notifications-focused-contexts): New option.
(erc-desktop-notifications-skip-predicates): New option.
(erc-notifications-notify): Address ancient comment regarding PRIVP
parameter possibly being unneeded when the current target matches the
nick.
(erc-notifications-PRIVMSG): Deprecate.
(erc-desktop-notifications-skip-untracked)
(erc-desktop-notifications-skip-fools): New functions
(erc-notifications-notify-on-match): Account for new options.
(erc-notifications-mode, erc-notifications-enable)
(erc-notifications-disable): Instead of the "PRIVMSG" response-handler
hook, use the `erc-match-type' API.
(erc-desktop-notifications--setup): New function
(erc-desktop-notifications-match-query-commands): New variable.
(erc-desktop-notifications--match-type-query): New struct type.
(erc-desktop-notifications--query-p): New function.
(erc-desktop-notification--query-notify): New function.
* test/lisp/erc/erc-desktop-notifications-tests.el: New file.
(Bug#73798)
---
 etc/ERC-NEWS                                  |  27 ++++
 lisp/erc/erc-desktop-notifications.el         |  98 ++++++++++--
 .../erc/erc-desktop-notifications-tests.el    | 140 ++++++++++++++++++
 3 files changed, 253 insertions(+), 12 deletions(-)
 create mode 100644 test/lisp/erc/erc-desktop-notifications-tests.el

diff --git a/etc/ERC-NEWS b/etc/ERC-NEWS
index dd960994b4f..9d3c216b408 100644
--- a/etc/ERC-NEWS
+++ b/etc/ERC-NEWS
@@ -11,6 +11,33 @@ This file is about changes in ERC, the powerful, modular, and
 extensible IRC (Internet Relay Chat) client distributed with
 GNU Emacs since Emacs version 22.1.
 
+
+* Changes in ERC 5.7
+
+** An extensibility focused 'match' API.
+Users have often expressed frustration over ERC's lack of a simple API
+for matching, highlighting, and filtering based on a message's content
+and metadata, like the sender or associated IRC command.  While it's
+true that discussions have been ongoing for a more powerful message
+formatting and construction API that will hopefully one day offer access
+to the various parts of a message before they're assembled, users will
+be needing something practical and effective in the interim.  Enter the
+'erc-match-type' API, which is based on a simple hook-like handler
+system.  You subscribe by enrolling a function that takes a special
+'erc-match-type' object with useful fields to help with matching,
+filtering, and applying faces.  See Info node 'Match API' to find out
+more.
+
+** Exercise more control in skipping certain desktop notifications.
+Elect to skip select notifications that ERC would otherwise emit with
+new hook option 'erc-desktop-notifications-skip-predicates'.  Its
+default members skip messages by fools and those in untracked buffers.
+
+** Opt out of desktop notifications from the active buffer.
+Option 'erc-desktop-notifications-focused-contexts' can help spare you
+from seeing desktop alerts for messages you're reading or those inserted
+while you're typing.
+
 
 * Changes in ERC 5.6.1
 
diff --git a/lisp/erc/erc-desktop-notifications.el b/lisp/erc/erc-desktop-notifications.el
index 6ef2f0d8ea1..b10ecfea683 100644
--- a/lisp/erc/erc-desktop-notifications.el
+++ b/lisp/erc/erc-desktop-notifications.el
@@ -47,6 +47,19 @@ erc-notifications-icon
   "Icon to use for notification."
   :type '(choice (const :tag "No icon" nil) file))
 
+(defcustom erc-desktop-notifications-focused-contexts '(query mention)
+  "Where to notify even if a match appears in the selected window."
+  :package-version '(ERC . "5.7") ; FIXME sync on release
+  :type '(set (const query) (const mention)))
+
+(defcustom erc-desktop-notifications-skip-predicates
+  '(erc-desktop-notifications-skip-untracked
+    erc-desktop-notifications-skip-fools)
+  "Abnormal hook whose members return non-nil to suppress notification.
+Called in match buffer with a matching `erc-match-user' object."
+  :package-version '(ERC . "5.7") ; FIXME sync on release
+  :type 'hook)
+
 (defcustom erc-notifications-bus :session
   "D-Bus bus to use for notification."
   :version "25.1"
@@ -60,14 +73,16 @@ dbus-debug
 (defun erc-notifications-notify (nick msg &optional privp)
   "Notify that NICK send some MSG, where PRIVP should be non-nil for PRIVMSGs.
 This will replace the last notification sent with this function."
-  ;; TODO: can we do this without PRIVP? (by "fixing" ERC's not
-  ;; setting the current buffer to the existing query buffer)
   (dbus-ignore-errors
     (setq erc-notifications-last-notification
-          (let* ((channel (if privp (erc-get-buffer nick) (current-buffer)))
-                 (title (format "%s in %s"
-                                (erc-compat--xml-escape-string nick t)
-                                channel))
+          (let* ((channel (or (and privp (not (equal nick (erc-target)))
+                                   (erc-get-buffer nick))
+                              (current-buffer)))
+                 (title (if (or privp (equal nick (erc-target)))
+                            (erc-compat--xml-escape-string nick t)
+                          (format "%s in %s"
+                                  (erc-compat--xml-escape-string nick t)
+                                  channel)))
                  (body (erc-compat--xml-escape-string (erc-controls-strip msg)
                                                       t)))
             (funcall (cond ((featurep 'android)
@@ -85,6 +100,7 @@ erc-notifications-notify
                                   (pop-to-buffer channel)))))))
 
 (defun erc-notifications-PRIVMSG (_proc parsed)
+  (declare (obsolete "switched to `erc-match-type' API" "31.1"))
   (let ((nick (car (erc-parse-user (erc-response.sender parsed))))
         (target (car (erc-response.command-args parsed)))
         (msg (erc-response.contents parsed)))
@@ -96,23 +112,81 @@ erc-notifications-PRIVMSG
   ;; Return nil to continue processing by ERC
   nil)
 
+(defun erc-desktop-notifications-skip-untracked (_)
+  "Return non-nil when current buffer's target appears in `erc-track-exclude'."
+  (and (boundp 'erc-track-exclude) (member (erc-target) erc-track-exclude)))
+
+(defun erc-desktop-notifications-skip-fools (_)
+  "Return non-nil if the current message has a \"match type\" of `fool'."
+  (erc-match-get-match 'erc-match-opt-fool))
+
 (defun erc-notifications-notify-on-match (match-type nickuserhost msg)
   (when (eq match-type 'current-nick)
     (let ((nick (nth 0 (erc-parse-user nickuserhost))))
       (unless (or (string-match-p "^Server:" nick)
-                  (when (boundp 'erc-track-exclude)
-                    (member nick erc-track-exclude)))
+                  (and (eq (current-buffer) (window-buffer))
+                       (frame-focus-state) ; t or unknown
+                       (not (memq 'mention
+                                  erc-desktop-notifications-focused-contexts)))
+                  (run-hook-with-args-until-success
+                   'erc-desktop-notifications-skip-predicates
+                   erc-match-highlight-matched))
         (erc-notifications-notify nick msg)))))
 
 ;;;###autoload(autoload 'erc-notifications-mode "erc-desktop-notifications" "" t)
 (define-erc-module notifications nil
   "Send notifications on private message reception and mentions."
   ;; Enable
-  ((add-hook 'erc-server-PRIVMSG-functions #'erc-notifications-PRIVMSG)
-   (add-hook 'erc-text-matched-hook #'erc-notifications-notify-on-match))
+  ((unless erc--updating-modules-p
+     (erc-buffer-do #'erc-desktop-notifications--setup))
+   (add-hook 'erc-mode-hook #'erc-desktop-notifications--setup))
   ;; Disable
-  ((remove-hook 'erc-server-PRIVMSG-functions #'erc-notifications-PRIVMSG)
-   (remove-hook 'erc-text-matched-hook #'erc-notifications-notify-on-match)))
+  ((erc-buffer-do #'erc-desktop-notifications--setup)
+   (remove-hook 'erc-mode-hook #'erc-desktop-notifications--setup)))
+
+(defun erc-desktop-notifications--setup ()
+  (if erc-notifications-mode
+      (progn
+        (add-hook 'erc-match-functions
+                ;; Run after default value to detect fools.
+                  #'erc-desktop-notifications--match-type-query 20 t)
+        (add-hook 'erc-text-matched-hook #'erc-notifications-notify-on-match
+                  20 t))
+    (remove-hook 'erc-match-functions
+                 #'erc-desktop-notifications--match-type-query t)
+    (remove-hook 'erc-text-matched-hook
+                 #'erc-notifications-notify-on-match t)))
+
+(defvar erc-desktop-notifications-match-query-commands '(PRIVMSG)
+  "IRC commands considered in query buffers for notification.
+Omits \"NOTICE\"s by default because they're typically reserved for bots
+and services that you interact with directly.")
+
+(cl-defstruct (erc-desktop-notifications--match-type-query
+               (:constructor erc-desktop-notifications--match-type-query)
+               (:include erc-match-user
+                         (category nil)
+                         (data erc-desktop-notifications-match-query-commands)
+                         (predicate #'erc-desktop-notifications--query-p)
+                         (handler #'erc-desktop-notifications--query-notify)))
+  "Notification match type for queries.")
+
+(defun erc-desktop-notifications--query-p (match)
+  "Return non-nil if MATCH object describes a \"PRIVMSG\" query."
+  (and (erc-query-buffer-p)
+       (or (memq 'query erc-desktop-notifications-focused-contexts)
+           (null (frame-focus-state))
+           (not (eq (current-buffer) (window-buffer))))
+       (memq (erc-match-command match) (erc-match-user-data match))
+       (always (cl-assert (erc-match-nick match)))
+       (not (run-hook-with-args-until-success
+             'erc-desktop-notifications-skip-predicates match))))
+
+(defun erc-desktop-notifications--query-notify (match)
+  ;; No need to pass argument PRIVP because current buffer is correct.
+  (erc-notifications-notify (erc-target)
+                            (erc-match-get-message-body match)))
+
 
 (provide 'erc-desktop-notifications)
 
diff --git a/test/lisp/erc/erc-desktop-notifications-tests.el b/test/lisp/erc/erc-desktop-notifications-tests.el
new file mode 100644
index 00000000000..e1b5aa2ea13
--- /dev/null
+++ b/test/lisp/erc/erc-desktop-notifications-tests.el
@@ -0,0 +1,140 @@
+;;; erc-desktop-notifications-tests.el --- Notifications tests  -*- lexical-binding:t -*-
+
+;; Copyright (C) 2025 Free Software Foundation, Inc.
+
+;; This file is part of GNU Emacs.
+;;
+;; GNU Emacs is free software: you can redistribute it and/or modify
+;; it under the terms of the GNU General Public License as published
+;; by the Free Software Foundation, either version 3 of the License,
+;; or (at your option) any later version.
+;;
+;; GNU Emacs is distributed in the hope that it will be useful, but
+;; WITHOUT ANY WARRANTY; without even the implied warranty of
+;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
+;; General Public License for more details.
+;;
+;; You should have received a copy of the GNU General Public License
+;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.
+
+;;; Commentary:
+;;; Code:
+(require 'erc-desktop-notifications)
+
+(require 'ert-x)
+(eval-and-compile
+  (let ((load-path (cons (ert-resource-directory) load-path)))
+    (require 'erc-tests-common)))
+
+(defun erc-desktop-notifications-tests--perform (test)
+  (erc-tests-common-make-server-buf)
+  (erc-notifications-mode +1)
+  (setq erc-server-current-nick "tester")
+
+  (unwind-protect
+      (cl-letf* ((calls nil)
+                 ((frame-parameter nil 'last-focus-update)
+                  t)
+                 ((symbol-function 'erc-notifications-notify)
+                  (lambda (&rest r) (push r calls))))
+        (with-current-buffer (erc--open-target "#chan")
+          (funcall test (lambda () (prog1 calls (setq calls nil))))))
+
+    (when noninteractive
+      (erc-notifications-mode -1)
+      (erc-tests-common-kill-buffers))))
+
+(defun erc-desktop-notifications-tests--populate-chan (test)
+  (erc-desktop-notifications-tests--perform
+   (lambda (check)
+     (erc-tests-common-add-cmem "bob")
+     (erc-tests-common-add-cmem "alice")
+
+     (erc-tests-common-simulate-line
+      ":irc.foonet.org 353 tester = #chan :alice bob tester")
+     (erc-tests-common-simulate-line
+      ":irc.foonet.org 366 tester #chan :End of NAMES list")
+     (erc-tests-common-simulate-privmsg "bob" "hi tester")
+
+     (should (equal (current-buffer) (get-buffer "#chan")))
+     (should (not (eq (current-buffer) (window-buffer)))) ; *ert* or *scratch*
+     (funcall test check))))
+
+(ert-deftest erc-desktop-notifications-focused-contexts/default ()
+  (should (equal erc-desktop-notifications-focused-contexts '(query mention)))
+
+  (erc-desktop-notifications-tests--populate-chan
+   (lambda (check)
+
+     ;; A private query triggers a notification.
+     (erc-tests-common-simulate-line ":bob!~bob@fsf.org PRIVMSG tester yo")
+     (should (eq (current-buffer) (get-buffer "bob")))
+
+     ;; A NOTICE command doesn't trigger a notification because it's
+     ;; absent from `erc-desktop-notifications-match-query-commands'.
+     (erc-tests-common-simulate-line ":irc.foonet.org NOTICE tester nope")
+
+     (should (equal (funcall check)
+                    '(("bob" "yo")
+                      ("bob" "hi tester\n"))))
+
+     ;; Setting the window to the buffer where insertions are happening
+     ;; makes no difference: notifications are still sent.
+     (erc-tests-common-simulate-line ":bob!~bob@fsf.org PRIVMSG tester ho")
+
+     (set-window-buffer nil (set-buffer "#chan"))
+     (erc-tests-common-simulate-privmsg "alice" "hi tester")
+
+     (should (equal (funcall check)
+                    '(("alice" "hi tester\n")
+                      ("bob" "ho")))))))
+
+(ert-deftest erc-desktop-notifications-focused-contexts/unselected ()
+  (should (equal erc-desktop-notifications-focused-contexts '(query mention)))
+
+  (let ((erc-desktop-notifications-focused-contexts))
+
+    (erc-desktop-notifications-tests--populate-chan
+     (lambda (check)
+       (should (equal (funcall check) '(("bob" "hi tester\n"))))
+
+       ;; Buffer #chan is current and displayed in the selected window,
+       ;; so no notification is sent.
+       (set-window-buffer nil "#chan") ; #chan
+       (erc-tests-common-simulate-privmsg "alice" "hi tester")
+
+       ;; A new query comes in for a buffer that doesn't exist.  The
+       ;; option `erc-receive-query-display' tells ERC to switch to that
+       ;; buffer and show it before insertion.  Therefore, no
+       ;; notification is sent.
+       (let ((erc-receive-query-display 'buffer))
+         (erc-tests-common-simulate-line
+          ":bob!~bob@fsf.org PRIVMSG tester yo"))
+
+       (should-not (funcall check))))))
+
+(ert-deftest erc-desktop-notifications-skip-predicates/fools ()
+  (erc-desktop-notifications-tests--populate-chan
+   (lambda (check)
+
+     ;; A private query triggers a notification.
+     (erc-tests-common-simulate-line ":bob!~bob@fsf.org PRIVMSG tester yo")
+     (should (eq (current-buffer) (get-buffer "bob")))
+
+     (should (equal (funcall check)
+                    '(("bob" "yo")
+                      ("bob" "hi tester\n"))))
+
+     (let ((erc-fools '("bob")))
+
+       ;; A query from is suppressed if bob is a fool.
+       (erc-tests-common-simulate-line ":bob!~bob@fsf.org PRIVMSG tester ho")
+       (should-not (funcall check))
+
+       ;; A mention from bob is suppressed if bob is a fool.
+       (with-current-buffer "#chan"
+         (erc-tests-common-simulate-privmsg "bob" "hi tester")
+         (erc-tests-common-simulate-privmsg "alice" "hi tester")
+         (should (equal (funcall check) '(("alice" "hi tester\n")))))))))
+
+;;; erc-desktop-notifications-tests.el ends here
-- 
2.48.1

