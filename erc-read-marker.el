;;; erc-read-marker.el -- Read marker IRCv3 extension for ERC  -*- lexical-binding: t; -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This extension provides read-progress synchronization among clients.
;; It depends on the `keep-place-indicator' module being enabled and the
;; option `erc-keep-place-indicator-follow' being t.

;;; Code:
(require 'erc-v3)
(require 'erc-goodies)
(require 'erc-batch)


(erc-v3--define-capability read-marker
  "Read progress synchronization among clients.
This extension depends on the `keep-place-indicator' module, which you
must activate separately."
  :aliases '(draft/read-marker)
  :slots ((marks nil :type list :documentation "Alist of (TARGET . STAMP)."))
  (when erc--target
    (if erc-v3--read-marker
        (progn
          (erc--modify-module-flag 'erc--keep-place-indicator-edge-triggered-p
                                   'read-marker 'addp)
          (add-hook 'erc--keep-place-move-hook #'erc-read-marker--report-mark
                    30 t)
          (add-hook 'erc-chathistory--request-finalize-functions
                    #'erc-read-marker--update-indicator-after-fetch 40 t)
          (add-hook 'erc-chathistory--new-query-request-functions
                    #'erc-read-marker--request-mark 20 t))
      (unless
          (erc--modify-module-flag 'erc--keep-place-indicator-edge-triggered-p
                                   'read-marker nil)
        (kill-local-variable 'erc--keep-place-indicator-edge-triggered-p))
      (remove-hook 'erc--keep-place-move-hook #'erc-read-marker--report-mark
                   t)
      (remove-hook 'erc-chathistory--request-finalize-functions
                   #'erc-read-marker--update-indicator-after-fetch t)
      (remove-hook 'erc-chathistory--new-query-request-functions
                   #'erc-read-marker--request-mark t))))

(defun erc-read-marker--set (ts target)
  (setf (alist-get target (erc-v3--read-marker-marks erc-v3--read-marker)
                   nil nil #'equal)
        (erc-span--ensure-time-str ts)))

(defun erc-read-marker--get (target)
  (and-let* ((alist (erc-v3--read-marker-marks erc-v3--read-marker))
             (found (alist-get target alist nil nil #'equal)))
    (erc-span--ensure-time-str found)))

(defun erc-read-marker--move (stamp)
  "Move indicator after STAMP."
  (when-let* ((erc-keep-place-indicator-mode)
              ((not (eql stamp 0)))
              (pos (erc-span--find-inserted-message-at-time stamp))
              (end (erc--get-inserted-msg-end pos)))
    (when-let* (((and (fboundp 'erc-track--redo-from-pos)
                      (erc-track--redo-from-pos end)))
                (buf (window-buffer)))
      (unwind-protect
          (progn
            (set-window-buffer (selected-window) (current-buffer))
            (goto-char (overlay-start erc--keep-place-indicator-overlay))
            (recenter 0))
        (set-window-buffer (selected-window) buf)))
    (let (erc--keep-place-move-hook)
      (erc-keep-place-move end))))

(define-erc-response-handler (MARKREAD)
  "Handle a MARKREAD command." nil
  (pcase-let ((`(,target ,(rx "timestamp=" (let stamp (+ nonl))))
               (erc-response.command-args parsed)))
    (unless (equal stamp (erc-read-marker--get target))
      (erc-read-marker--set stamp target)
      (erc-with-buffer (target proc)
        (erc-read-marker--move stamp)))))

(defun erc-read-marker--report-mark ()
  "Tell server the current buffer's progress timestamp."
  (when-let* ((target (erc-target)) ; ignore server buffers
              ((erc--target-channel-p erc--target))
              ((erc--target-channel-joined-p erc--target)) ; haven't parted
              (ts (erc--get-inserted-msg-prop 'erc--ts))
              (stamp (erc-span--ensure-time-str ts))
              ((not (equal stamp (erc-read-marker--get target)))))
    (erc-read-marker--set ts target)
    (erc-server-send (format "MARKREAD %s timestamp=%s" target stamp))))

(defun erc-read-marker--request-mark (&rest _)
  "Request an update for a query buffer."
  (cl-assert (erc-query-buffer-p))
  (erc-server-send (concat "MARKREAD " (erc-target))))

(defun erc-read-marker--update-indicator-after-fetch (_)
  (cl-assert erc-keep-place-indicator-mode)
  (when-let* ((stamp (erc-read-marker--get (erc-target)))
              (ov erc--keep-place-indicator-overlay)
              (ts (erc--get-inserted-msg-prop 'erc--ts (overlay-start ov)))
              ((not (string= stamp (erc-span--ensure-time-str ts)))))
    (erc-read-marker--move stamp)))

(provide 'erc-read-marker)
;;; erc-read-marker.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
