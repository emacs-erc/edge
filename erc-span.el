;;; erc-span.el -- Local message storage -*- lexical-binding: t; -*-

;; Copyright (C) 2024 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This module is meant for use by other modules, not necessarily
;; built-in ones, which must activate it as needed.  It models a
;; target's canonical message log as an index of non-overlapping
;; intervals and provides utilities to help merge adjacent intervals
;; when their common boundary meets some configurable condition.

;; TODO: explain API.
;; TODO: implement merging.

;;; Code:
(require 'erc)

(defvar erc-span--active-inserter nil)
(defvar erc-span--bookends '(">>> " . "<<< "))

;; Setting this to anything smaller ends up blocking the processing of
;; arriving input.  An older version of this library based on lisp
;; threads, instead of timers, did not have this problem. :/
(defvar erc-span--worker-soon 0.025
  "Delay in seconds between rounds when processing stashed batches.")

(defvar erc-span--use-lisp-threads-p (and (require 'thread nil t) t)
  "Non-nil to experimentally use lisp threads when running inserter.")

(defvar-local erc-span--ranges nil)
(defvar-local erc-span--last-received-time nil)

(defconst erc-span--playback-command-types
  '(PRIVMSG NOTICE JOIN PART QUIT NICK KICK MODE TOPIC))

(define-erc-module span nil
  "Manage visible history ranges in an ERC buffer."
  ((add-function :around
                 (local 'erc-networks--transplant-target-buffer-function)
                 #'erc-span--merge-ranges '((depth . -80)))
   (if erc--target
       (progn
         (erc--restore-initialize-priors erc-span-mode
           erc-span--ranges nil)
         (add-function :around (local 'erc--clear-function)
                       #'erc-span--shorten-truncated-bookend-on-clear
                       '((depth . 60)))
         (add-hook 'erc-connect-pre-hook
                   #'erc-span--ensure-live-on-new-query -80 t))
     (erc--restore-initialize-priors erc-span-mode
       erc-span--last-received-time nil)
     (add-hook 'erc-server-PRIVMSG-functions
               #'erc-span--handle-live-privmsg -80 t)
     (add-hook 'erc-server-NOTICE-functions
               #'erc-span--handle-live-privmsg -80 t)
     (add-hook 'erc-server-JOIN-functions
               #'erc-span--handle-live-join -80 t)
     (add-hook 'erc-server-PART-functions
               #'erc-span--handle-live-part -80 t)
     (add-hook 'erc-server-QUIT-functions
               #'erc-span--handle-live-quit -80 t)
     (add-hook 'erc-server-NICK-functions
               #'erc-span--handle-live-nick -80 t)
     (add-hook 'erc-server-KICK-functions
               #'erc-span--handle-live-kick -80 t)
     (add-hook 'erc-server-MODE-functions
               #'erc-span--handle-live-mode -80 t)
     (add-hook 'erc-server-TOPIC-functions
               #'erc-span--handle-live-topic -80 t)
     (add-hook 'erc-disconnected-hook
               #'erc-span--seal-on-disconnect 20 t)
     (add-hook 'erc-part-hook 'erc-span--seal-on-part 0 t)))
  ((remove-function (local 'erc-networks--transplant-target-buffer-function)
                    #'erc-span--merge-ranges)
   (remove-function (local 'erc--clear-function)
                    #'erc-span--shorten-truncated-bookend-on-clear)
   (remove-hook 'erc-server-PRIVMSG-functions
                #'erc-span--handle-live-privmsg t)
   (remove-hook 'erc-server-NOTICE-functions
                #'erc-span--handle-live-privmsg t)
   (remove-hook 'erc-server-JOIN-functions #'erc-span--handle-live-join t)
   (remove-hook 'erc-server-PART-functions #'erc-span--handle-live-part t)
   (remove-hook 'erc-server-QUIT-functions #'erc-span--handle-live-quit t)
   (remove-hook 'erc-server-NICK-functions #'erc-span--handle-live-nick t)
   (remove-hook 'erc-server-KICK-functions #'erc-span--handle-live-kick t)
   (remove-hook 'erc-server-MODE-functions #'erc-span--handle-live-mode t)
   (remove-hook 'erc-server-TOPIC-functions #'erc-span--handle-live-topic t)
   (remove-hook 'erc-connect-pre-hook #'erc-span--ensure-live-on-new-query t)
   (kill-local-variable 'erc-span--last-received-time)
   (kill-local-variable 'erc-span--ranges))
  'local)


;;;; Helpers

(defun erc-span--ensure-time (time)
  (if (stringp time) (erc-compat--iso8601-to-time time t) time))

(defun erc-span--ensure-time-str (time)
  (if (stringp time) time (format-time-string "%FT%T.%3NZ" time t)))


;;;; Timer-based worker management

(defmacro erc-span--progv (hashmap &rest body)
  "Let-bind keys to their values in HASHMAP and eval BODY.
Keys must be `special-variable-p'.  Afterwards, reassign values to keys
as variables in BODY, and return its result.  Persist the value of items
in HASHMAP after running BODY."
  (declare (indent 1))
  (let ((keys (make-symbol "keys"))
        (values (make-symbol "values"))
        (table (make-symbol "table")))
    `(let ((,table ,hashmap)
           ,keys ,values)
       (maphash (lambda (k v)
                  (when (special-variable-p k)
                    (push k ,keys)
                    (push v ,values)))
                ,table)
       (cl-progv ,keys ,values
         (prog1 (progn ,@body)
           (dolist (k ,keys)
             (puthash k (symbol-value k) ,table)))))))

(defmacro erc-span--with-locals (alist buffers &rest body)
  "Shadow buffer-local vars in BUFFERS around BODY at runtime.
Expect BUFFERS to be a list of forms that evaluate to buffers.  Expect
ALIST, once evaluated, to be an alist with member pairs like (BUFFER
. HASH-TABLE), with each BUFFER also appearing in BUFFERS.  Assume each
key in HASH-TABLE has a local binding in BUFFER, and `let'-bind it in
BUFFER to its HASH-TABLE value around BODY."
  (declare (indent 2))
  (let ((asym (make-symbol "asym"))
        (obuf (make-symbol "obuf"))
        (bufs (let ((i 0))
                (mapcar (lambda (b)
                          (list (make-symbol (format "b%s" (cl-incf i))) b))
                        buffers))))
    (setq body `((with-current-buffer ,obuf ,@body)))
    (dolist (binding bufs)
      (let ((b (car binding)))
        (setq body `((with-current-buffer ,b
                       (erc-span--progv (alist-get ,b ,asym)
                         ,@body))))))
    `(let (,@(setq bufs (nreverse bufs))
           (,obuf (current-buffer))
           (,asym ,alist))
       ,@body)))

;; Modules with modification hooks that keep state between visits may
;; need to persist or shadow local variables.  This module will stash
;; and restore any buffer-local variables listed here.
;;
;; Members should be a symbol or a cons cell like (VAR . INITFN) where
;; INITFN is a function that takes no args and spits out an init value.
;;
;; Inserters may need manage shadowing specially in certain cases.
;; For example, the global var `erc-modified-channels-alist' should
;; not be shadowed when backfilling a buffer with initial history on
;; JOIN, so that users can see if they've been highlighted.  However,
;; this is probably not desired when populating for infinite scroll
;; during a session.
(defvar-local erc-span--target-locals
    `((erc-channel-members . ,(lambda () (make-hash-table :test #'equal)))
      (erc--inhibit-mode-line-update-p . always)
      erc--channel-modes
      erc--mode-line-mode-string
      erc-channel-banlist
      erc-channel-key
      erc-channel-modes
      erc-channel-topic
      erc-channel-user-limit
      ;; FIXME add these dynamically, perhaps via `erc-span-mode-hook'.
      erc-fill--wrap-last-msg
      erc-join-hook
      erc-timestamp-last-inserted
      erc-timestamp-last-inserted-left
      erc-timestamp-last-inserted-right)
  "Special vars local to target buffers to shadow when replaying history.
If a member is a cons, ERC calls its CDR, a function, to initialize it.")

(defvar-local erc-span--server-locals
    `((erc-server-users . ,(lambda () (make-hash-table :test #'equal)))
      (erc-server-current-nick . ,(lambda () erc-server-current-nick))
      (erc-default-nicks . ,(lambda () erc-default-nicks))
      (erc-netsplit-regexp . ,(lambda () regexp-unmatchable))
      (erc-netsplit--mode-regexp . ,(lambda () regexp-unmatchable)))
  "Special vars local to server buffers to shadow when replaying history.")

(defvar-local erc-span--destructive-target-locals
    `((erc--inhibit-clear-p . natnump))
  "Variables local to target buffers for the duration of an insertion.
Each entry is a cons cell of (SYMBOL . SETFN).  ERC calls SETFN with 1
when initializing and -1 when finalizing.  In the latter context, if
SETFN returns nil, ERC kills its local binding.")

(defun erc-span--finalize-destructive-target-locals ()
  "Call `erc-span--destructive-target-locals' functions with a -1 arg."
  (cl-assert erc--target)
  (pcase-dolist (`(,var . ,fn) erc-span--destructive-target-locals)
    (let ((val (funcall fn -1)))
      (if (and (special-variable-p var) val)
          (set (make-local-variable var) val)
        (kill-local-variable var)))))

(defun erc-span--populate-locals (existing &rest buffers)
  "Add overriding local variables to EXISTING.
Expect EXISTING to be an `alist' mapping buffers to hash-tables of
local-variable overrides."
  (dolist (buffer buffers)
    (cl-assert (null (assq buffer existing)))
    (let ((table (make-hash-table)))
      (push (cons buffer table) existing)
      (with-current-buffer buffer
        (when erc--target
          (pcase-dolist (`(,var . ,fn) erc-span--destructive-target-locals)
            (when (special-variable-p var)
              (set (make-local-variable var) (funcall fn +1)))))
        (dolist (var (if erc--target
                         erc-span--target-locals
                       erc-span--server-locals))
          (pcase var
            (`(,var . ,init-fn) (puthash var (funcall init-fn) table))
            (var (when (special-variable-p var)
                   (puthash var nil table))))))))
  existing)

;; Backend interface.

(cl-defgeneric erc-span--inserter-get-length (inserter)
  "Return INSERTER length, a non-negative integer.")

(cl-defgeneric erc-span--inserter-get-name (inserter)
  "Return INSERTER name, a string.")

(cl-defgeneric erc-span--inserter-finalize (inserter)
  "Tear down INSERTER resources.")

(cl-defgeneric erc-span--inserter-set-timer (inserter value)
  "Set INSERTER's timer to VALUE.")

(cl-defgeneric erc-span--inserter-insert-one (inserter)
  "Process one message with INSERTER.")

(cl-defmethod erc-span--run-inserter (buffer inserter)
  (if (buffer-live-p buffer)
      (with-current-buffer buffer
        (erc-compat--with-backtrace-buffer "*erc-span-process-error*"
          (let ((erc-span--active-inserter inserter))
            (erc-span--inserter-insert-one inserter))
          (if (zerop (erc-span--inserter-get-length inserter))
              (erc-span--inserter-finalize inserter)
            (erc-span--inserter-set-timer
             inserter (run-at-time erc-span--worker-soon
                                   nil #'erc-span--run-inserter
                                   buffer inserter)))))
    (erc-with-server-buffer
      (erc-button--display-error-notice-with-keys
       (current-buffer) "ERC span buffer %s missing."
       (erc-span--inserter-get-name inserter)))))

(declare-function make-thread "thread.c" (function &optional name))
(declare-function thread-yield "thread.c" ())

(defun erc-span--run-inserter-in-thread (buffer inserter)
  (cl-assert (buffer-live-p buffer))
  (with-current-buffer buffer
    (erc-compat--with-backtrace-buffer "*erc-span-process-error*"
      (let ((erc-span--active-inserter inserter)
            (inhibit-quit t)) ; MS Windows
        (while (progn
                 (erc-span--inserter-insert-one inserter)
                 (not (zerop (erc-span--inserter-get-length inserter))))
          (if (and (not noninteractive) (input-pending-p))
              (sleep-for erc-span--worker-soon)
            (thread-yield))))
      (erc-span--inserter-finalize inserter))))

(cl-defmethod erc-span--run-inserter
  (buffer inserter &context (erc-span--use-lisp-threads-p (eql t)))
  (cl-assert (buffer-live-p buffer))
  (make-thread (apply-partially #'erc-span--run-inserter-in-thread
                                buffer inserter)
               (erc-span--inserter-get-name inserter)))

(cl-defgeneric erc-span--backfill-on-join ()
  "Maybe populate current cannel buffer when client joins.
Expect code that needs to call `erc-server-JOIN-functions' to do so on a
timer."
  ;; Finalize previous live interval before initialzing last.
  (erc-span--seal-last-live))

;; Finalizers should call this with the range attribute of their
;; inserter object.
(defun erc-span--range-hide-bookend-hi (range)
  (when-let* ((beg (erc-span--range-marker-hi range)))
    (with-current-buffer (marker-buffer beg)
      (when-let* ((bounds (erc--get-inserted-msg-bounds beg))
                  (erc--msg-props (make-hash-table)))
        (save-excursion
          (save-restriction
            (narrow-to-region (car bounds) (cdr bounds))
            (with-silent-modifications
              (erc-span--hide-message-on-insert)
              (put-text-property (car bounds) (1+ (car bounds)) 'erc--hide
                                 (erc--check-msg-prop 'erc--hide)))))))))

;;;; Interval management

;; TODO Provide some way to allow dependent modules, like chathistory,
;; to fill the intervening space after the live interval is
;; initialized, by extending it backward, possibly merging it with the
;; one we've added.

(cl-defstruct erc-span--range
  "A linked-list element representing an interval in the buffer."
  (tweeners nil :type list) ; FIXME explain this
  (display nil :type list)
  (marker-lo (make-marker) :type marker)
  (marker-hi nil :type (or null marker))
  (ts-lo nil :type (or null string))
  (ts-hi nil :type (or null string))
  (prev nil :type (or null erc-span--range))
  (next nil :type (or null erc-span--range)))

(defun erc-span--range-nth-prev (node n) ; used in testing
  "Return Nth previous `erc-span--range' NODE."
  (while (and (setq node (erc-span--range-prev node))
              (not (zerop (cl-decf n)))))
  node)

(defun erc-span--range-get-oldest (ranges)
  (while-let ((prev (erc-span--range-prev ranges))) (setq ranges prev))
  ranges)

(defun erc-span--range-get-newest (ranges)
  (while-let ((next (erc-span--range-next ranges))) (setq ranges next))
  ranges)

(defun erc-span--range-get-live (ranges)
  (setq ranges (erc-span--range-get-newest ranges))
  (and (null (erc-span--range-marker-hi ranges)) ranges))

(defun erc-span--range-find-by-time (node time)
  "Find `erc-span--range' object bounding TIME, beginning at NODE.
Search backward from NODE, and return the oldest range bounding TIME,
inclusive.  Expect TIME to be a server-time stamp."
  (cl-assert (null (erc-span--range-next node)))
  (let (found)
    (while (and node
                (let ((hi (erc-span--range-ts-hi node)))
                  (or (null hi) (not (string-lessp hi time))))
                (or (string-lessp time (erc-span--range-ts-lo node))
                    (setq found node))
                (setq node (erc-span--range-prev node))))
    found))

(defmacro erc-span--range-validate-insertion (left right)
  "Assert would-be adjacent ranges LEFT and RIGHT do not overlap.
Assert that the high timestamp of the LEFT range is strictly less than
the low timestamp of the RIGHT range."
  `(when-let* ((right-lo (erc-span--range-ts-lo ,right))
               (left-hi (erc-span--range-ts-hi ,left)))
     ;; In some contexts, "interval" means overlaps are allowed.
     (cl-assert (string-lessp left-hi right-lo) nil "Overlapping ranges: %S"
                (list :left-hi left-hi
                      :right-lo right-lo
                      :left (erc-span--range-ts-lo ,left)
                      :right (erc-span--range-ts-lo ,right)))))

(defun erc-span--range-insert (ranges new)
  "Splice NEW `erc-span--range' object into existing linked RANGES."
  (cl-assert (null (erc-span--range-prev new)))
  (cl-assert (null (erc-span--range-next new)))
  (let ((id (erc-span--range-ts-lo new)))
    (while-let (((string> id (erc-span--range-ts-lo ranges)))
                (next (erc-span--range-next ranges)))
      (setq ranges next))
    (while-let (((string< id (erc-span--range-ts-lo ranges)))
                (prev (erc-span--range-prev ranges)))
      (setq ranges prev))
    (if (string< id (erc-span--range-ts-lo ranges))
        (let ((existing (erc-span--range-prev ranges)))
          (erc-span--range-validate-insertion new ranges)
          (setf (erc-span--range-prev ranges) new
                (erc-span--range-next new) ranges)
          (when existing
            (erc-span--range-validate-insertion existing new)
            (setf (erc-span--range-next existing) new
                  (erc-span--range-prev new) existing)))
      (let ((existing (erc-span--range-next ranges)))
        (erc-span--range-validate-insertion ranges new)
        (setf (erc-span--range-next ranges) new
              (erc-span--range-prev new) ranges)
        (cl-assert (erc-span--range-marker-hi ranges) nil "Cannot be live")
        (when existing
          (erc-span--range-validate-insertion new existing)
          (setf (erc-span--range-prev existing) new
                (erc-span--range-next new) existing))))))

(defconst erc-message-english-span-range-bookend-begin
  #("RANGE-BEG %y" 0 9 (erc-span--range-bookend 'stamp))
  "English `erc-format-message' template for key `span-range-bookend-begin'.")

(defconst erc-message-english-span-range-bookend-end
  #("RANGE-END %y" 0 9 (erc-span--range-bookend 'stamp))
  "English `erc-format-message' template for key `span-range-bookend-end'.")

(defun erc-span--find-insertion-point (p target-time &optional afterp)
  "Return the oldest message-start position not older than TARGET-TIME.
Start searching from P.  With AFTERP, stop before any messages from
TARGET-TIME."
  (setq target-time (erc-span--ensure-time target-time))
  (while-let ((q (previous-single-property-change (max (1- p) (point-min))
                                                  'erc--ts))
              (qq (erc--get-inserted-msg-beg q))
              (ts (get-text-property qq 'erc--ts))
              ((if afterp
                   ;; While target is older than visited...
                   (time-less-p target-time ts)
                 ;; While visited is newer or equal to target...
                 (not (time-less-p ts target-time)))))
    (setq p qq))
  p)

(defun erc-span--local-time-string (time)
  (format-time-string "%c" time (bound-and-true-p erc-stamp--tz)))

(defun erc-span--make-bookend-propertizer (node &optional init-disp)
  (cl-assert (erc--check-msg-prop 'erc--msg '(span-range-bookend-end
                                              span-range-bookend-begin)))
  (puthash 'erc--ts (erc--current-time-pair) erc--msg-props)
  (puthash 'erc-span--range node erc--msg-props)
  (let* ((span-id (erc-span--range-ts-lo node))
         (ts (erc-span--local-time-string (erc--current-time-pair)))
         (beg (text-property-not-all (point-min) (1- (point-max))
                                     'erc-span--range-bookend nil))
         (end (next-single-property-change beg 'erc-span--range-bookend)))
    (puthash 'erc-span--range-id span-id erc--msg-props)
    (put-text-property beg end 'display ts)
    (if (erc-span--range-display node)
        (setcar (erc-span--range-display node) (or init-disp ""))
      (setf (erc-span--range-display node) (list (or init-disp  ""))))
    (put-text-property end (1- (point-max)) 'display
                       (erc-span--range-display node))))

(defun erc-span--hide-message-on-insert ()
  (erc--hide-message 'span-range-bookend))

(defun erc-span--insert-bookend-hi (node point-or-marker)
  (when-let* ((ts-hi (erc-span--range-ts-hi node))
              (erc--current-time-pair (erc-span--ensure-time ts-hi))
              (erc-notice-prefix (cdr erc-span--bookends))
              (erc--insert-marker (setf (erc-span--range-marker-hi node)
                                        (copy-marker point-or-marker))))
    (cl-assert (null (marker-insertion-type erc--insert-marker)))
    (set-marker-insertion-type erc--insert-marker nil)
    (erc-display-message nil 'notice (current-buffer) 'span-range-bookend-end
                         ?y (erc-span--range-ts-lo node))
    (set-marker-insertion-type erc--insert-marker t)))

(defun erc-span--insert-bookend-lo (node marker) ; moves marker
  (let ((erc-notice-prefix (car erc-span--bookends))
        (erc--insert-marker marker)
        (erc-insert-post-hook (cons #'erc-span--hide-message-on-insert
                                    (ensure-list erc-insert-post-hook)))
        (erc--current-time-pair (erc-span--ensure-time
                                 (erc-span--range-ts-lo node))))
    (setf (erc-span--range-marker-lo node) (copy-marker marker))
    (erc-display-message nil 'notice (current-buffer) 'span-range-bookend-begin
                         ?y (erc-span--range-ts-lo node))
    (set-marker-insertion-type (erc-span--range-marker-lo node) t)))

;; If the current time is less than expiration (car), use (cdr).
;; Otherwise, create a new log.  Assume `echo-message'.

(defun erc-span--range-add (time-lo &optional time-hi)
  "Add range to current buffer's `erc-span--ranges'.
Expect TIME-LO and, if given, TIME-HI to be timestamp strings.
Initialize or update the local variable `erc-span--ranges' to the newest
range."
  (let* ((marker (copy-marker erc-insert-marker))
         (node (make-erc-span--range :ts-lo time-lo :ts-hi time-hi))
         (erc--msg-prop-overrides `((erc--skip stamp)
                                    ,@erc--msg-prop-overrides))
         (propfn (apply-partially #'erc-span--make-bookend-propertizer node
                                  (and (null time-hi) " (live)")))
         (erc-insert-modify-hook (cons propfn erc-insert-modify-hook)))
    (set-marker-insertion-type marker t)
    (if-let* ((erc-span--ranges)
              ;; If there's a `next' node, start scanning from there.
              ((erc-span--range-insert erc-span--ranges node))
              (time-hi)
              (refnode (erc-span--range-next node)))
        (save-excursion
          (goto-char (erc-span--range-marker-lo refnode))
          (forward-line)
          (set-marker marker (erc--get-inserted-msg-beg (point))))
      (unless (erc-span--range-next node) ; unless adding older node
        (setq erc-span--ranges node)))
    ;; Print hi bookend.
    (if time-hi
        (progn
          (set-marker marker (erc-span--find-insertion-point marker time-hi t))
          (erc-span--insert-bookend-hi node marker))
      ;; Make `erc-span--ranges' point to the live node.
      (setq erc-span--ranges node))
    ;; Print lo bookend.
    (set-marker marker (erc-span--find-insertion-point marker time-lo))
    (erc-span--insert-bookend-lo node marker)
    ;; Mark intervening messages so inserters can flow around them.
    (when-let* ((time-hi)
                (start (erc-span--range-marker-hi node))
                (finish (erc-span--range-marker-lo node)))
      (while-let ((beg (erc--get-inserted-msg-beg (1- start)))
                  ((/= beg finish))
                  (time (get-text-property beg 'erc--ts))
                  (m (copy-marker beg)))
        (set-marker-insertion-type m t)
        (setq start beg)
        (push (cons time m) (erc-span--range-tweeners node))))
    (cl-assert erc-span--ranges)
    (cl-assert (null (erc-span--range-next erc-span--ranges)))
    (erc-span--assert-markers)
    node))

(defun erc-span--range-get-insert-marker (range-node message-time)
  (if-let* ((tweeners (erc-span--range-tweeners range-node))
            (head (car tweeners))
            (time (car head))
            (marker (cdr head)))
      (if (time-less-p time message-time)
          (progn
            (set-marker (cdr (pop (erc-span--range-tweeners range-node))) nil)
            (erc-span--range-get-insert-marker range-node message-time))
        (copy-marker marker))
    (copy-marker (erc-span--range-marker-hi range-node))))

(defun erc-span--find-last-playback-msg (limit-beg start)
  (let (bounds)
    (while-let (((null bounds))
                ((> start limit-beg))
                (p (previous-single-property-change start 'erc--cmd))
                (b (erc--get-inserted-msg-bounds p)))
      (setq start (car b))
      (when (memq (get-text-property start 'erc--cmd)
                  erc-span--playback-command-types)
        (setq bounds b)))
    bounds))

(defun erc-span--find-inserted-message-at-time (time)
  "Do a binary search for the oldest inserted message after TIME."
  (when-let*
      ((ranges erc-span--ranges)
       (range (erc-span--range-find-by-time ranges
                                            (erc-span--ensure-time-str time)))
       (ts-hi (or (erc-span--ensure-time (erc-span--range-ts-hi range))
                  (erc--current-time-pair)))
       (ts-lo (erc-span--ensure-time (erc-span--range-ts-lo range)))
       (pos-hi (or (erc-span--range-marker-hi range) erc-insert-marker))
       (pos-lo (erc-span--range-marker-lo range)))
    (setq time (erc-span--ensure-time time))
    (while-let
        ((pos (let ((pos (+ pos-lo (/ (- pos-hi pos-lo) 2))))
                (while (and (/= pos (point-min))
                            (setq pos (erc--get-inserted-msg-beg pos))
                            (not (get-text-property pos 'erc--ts)))
                  (cl-decf pos))
                pos))
         (ts (get-text-property pos 'erc--ts))
         ((or (time-less-p ts-lo ts) (time-equal-p ts-lo ts)))
         ((or (time-less-p ts ts-hi) (time-equal-p ts-hi ts)))
         ((< pos-lo pos))
         ((<= pos pos-hi)))
      (if (time-less-p ts time)
          (setq pos-lo pos ts-lo ts)
        (setq pos-hi pos ts-hi ts)))
    ;; Scan last mile.
    (erc-span--find-insertion-point pos-hi time 'afterp)))

(defun erc-span--seal-last-live ()
  (when-let* ((ranges erc-span--ranges)
              (node (erc-span--range-get-live ranges))
              ((null (erc-span--range-ts-hi node)))
              (bounds (erc-span--find-last-playback-msg
                       (erc-span--range-marker-lo node) erc-insert-marker))
              (last-time (get-text-property (car bounds) 'erc--ts))
              (pfn (apply-partially #'erc-span--make-bookend-propertizer node))
              (erc--msg-prop-overrides `((erc--skip stamp track)
                                         ,@erc--msg-prop-overrides))
              (erc-insert-modify-hook (cons pfn erc-insert-modify-hook))
              (erc-insert-post-hook (cons #'erc-span--hide-message-on-insert
                                          erc-insert-post-hook)))
    (cl-assert (null (erc-span--range-ts-hi node)))
    (cl-assert (null (erc-span--range-marker-hi node)))
    (setf (erc-span--range-ts-hi node) (erc-span--ensure-time-str last-time))
    (erc-span--insert-bookend-hi node (1+ (cdr bounds)))))

(defun erc-span--seal-on-disconnect (&rest _)
  "Seal spans in all target buffers when disconnecting."
  (erc-with-all-buffers-of-server erc-server-process (lambda () erc-span-mode)
    (unless erc--target
      (setq erc-span--last-received-time erc-server-last-received-time))
    (erc-span--seal-last-live)))

(defun erc-span--seal-on-part (buffer)
  (when (buffer-live-p buffer)
    (with-current-buffer buffer
      (erc-span--seal-last-live))))

;; TODO: add live methods for TAGMSG and KICK

(cl-defgeneric erc-span--on-live-privmsg (parsed)
  "Run business code on \"PRIVMSG\" with PARSED `erc--zPRIVMSG' object.")

(cl-defgeneric erc-span--on-live-join (parsed)
  "Run business code on \"JOIN\" with PARSED `erc--zJOIN' object.")

(cl-defgeneric erc-span--on-live-part (parsed)
  "Run business code on \"PART\" with PARSED `erc--zPART' object.")

(cl-defgeneric erc-span--on-live-quit (parsed)
  "Run business code on \"QUIT\" with PARSED `erc--zQUIT' object.")

(cl-defgeneric erc-span--on-live-nick (parsed)
  "Run business code on \"NICK\" with PARSED `erc--zNICK' object.")

(cl-defgeneric erc-span--on-live-kick (parsed)
  "Run business code on \"KICK\" with PARSED `erc--zKICK' object.")

(cl-defgeneric erc-span--on-live-mode (parsed)
  "Run business code on \"MODE\" with PARSED `erc--zMODE' object.")

(cl-defgeneric erc-span--on-live-topic (parsed)
  "Run business code on \"TOPIC\" with PARSED `erc--zTOPIC' object.")

(cl-defgeneric erc-span--mint-live-range-id (timestamp)
  "Return a unique number for the range-id of the \"live\" message stream.
Expect TIMESTAMP to be a string in `server-time' format."
  timestamp)

(defun erc-span--ensure-live-span (time)
  "Create a \"live\" span if none exists."
  (or (and erc-span--ranges (erc-span--range-get-live erc-span--ranges))
      (erc-span--range-add (erc-span--mint-live-range-id time) nil)
      (error "Failed creating live span from %s"
             (erc-span--ensure-time-str time))))

(defun erc-span--assert-markers ()
  (let ((node erc-span--ranges))
    (while-let ((node)
                (mlo (erc-span--range-marker-lo node)))
      (when-let* ((prev (erc-span--range-prev node)))
        ;; ID and markers are strictly greater than previous.
        (cl-assert (string> (erc-span--range-ts-lo node)
                            (erc-span--range-ts-lo prev)))
        (cl-assert (> (erc-span--range-marker-lo node)
                      (erc-span--range-marker-hi prev))))
      ;; Markers reside at inserted message.
      (cl-assert (eq (erc--get-inserted-msg-prop 'erc--msg mlo)
                     'span-range-bookend-begin))
      (when-let* ((mhi (erc-span--range-marker-hi node)))
        (cl-assert (eq (erc--get-inserted-msg-prop 'erc--msg mhi)
                       'span-range-bookend-end))
        ;; Time stamps can be equal.
        (cl-assert (or (string< (erc-span--range-ts-lo node)
                                (erc-span--range-ts-hi node))
                       (string= (erc-span--range-ts-lo node)
                                (erc-span--range-ts-hi node)))))
      (setq node (erc-span--range-prev node)))))

(defun erc-span--handle-live-privmsg (_ parsed)
  "Ensure a live range exists on PRIVMSG before running hooks.
Return the result of calling `erc-span--on-live-privmsg' with PARSED, an
`erc--zPRIVMSG' object."
  (when-let* (((null erc-span--active-inserter))
              (buffer (erc--zPRIVMSG-buffer parsed))
              ((buffer-live-p buffer)))
    (with-current-buffer buffer
      (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
        (erc-span--on-live-privmsg parsed)))))

(defun erc-span--handle-live-join (_ parsed)
  "Ensure a \"live\" range exists when someone else joins a channel.
When the client itself joins, return the result of calling
`erc-span--backfill-on-join' with PARSED, an `erc--zJOIN' object."
  (when-let* (((null erc-span--active-inserter))
              (buffer (erc--zJOIN-buffer parsed))
              ((buffer-live-p buffer)))
    (with-current-buffer buffer
      (if (erc--zJOIN-self-p parsed)
          (ignore (erc-span--backfill-on-join))
        (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
          (erc-span--on-live-join parsed))))))

(defun erc-span--handle-live-part (_ parsed)
  "Ensure a \"live\" range exists when someone else parts a channel.
Then return the result of calling `erc-span--on-live-part'."
  (when-let* (((null erc-span--active-inserter))
              (buffer (erc--zPART-buffer parsed))
              ((buffer-live-p buffer))
              ((not (erc--zPART-self-p parsed))))
    (with-current-buffer buffer
      (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
        (erc-span--on-live-part parsed)))))

(defun erc-span--handle-live-quit (proc parsed)
  "Ensure a \"live\" range exists when someone quits.
Then call `erc-span--on-live-quit' in each target buffer."
  (when-let* (((null erc-span--active-inserter))
              ((not (erc--zQUIT-self-p parsed)))
              (nick (car (erc--zQUIT-nuh parsed)))
              (bufs (erc-buffer-list-with-nick nick proc)))
    (let (rv)
      (dolist (buf bufs)
        (when (buffer-live-p buf)
          (with-current-buffer buf
            (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
              (setq rv (or (erc-span--on-live-quit parsed) rv))))))
      rv)))

(defun erc-span--handle-live-nick (proc parsed)
  "Ensure a \"live\" range exists when a channel user changes their nick.
Then call `erc-span--on-live-nick' in each target buffer and return the
last non-nil return value."
  (when-let* (((null erc-span--active-inserter))
              ((not (erc--zNICK-self-p parsed)))
              (nick (car (erc--zNICK-nuh parsed)))
              (bufs (erc-buffer-list-with-nick nick proc)))
    (let (rv)
      (dolist (buf bufs)
        (when (buffer-live-p buf)
          (with-current-buffer buf
            (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
              (setq rv (or (erc-span--on-live-nick parsed) rv))))))
      rv)))

(defun erc-span--handle-live-kick (_ parsed)
  "Ensure a \"live\" range exists when a user is kicked.
Return the result of calling `erc-span--on-live-kick' with PARSED."
  (when-let* (((null erc-span--active-inserter))
              (buffer (erc--zKICK-buffer parsed)))
    (with-current-buffer buffer
      (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
        (erc-span--on-live-kick parsed)))))

(defun erc-span--handle-live-mode (_ parsed)
  "Ensure a \"live\" range exists when the channel mode changes.
Return the result of calling `erc-span--on-live-mode' with PARSED."
  (when-let* (((null erc-span--active-inserter))
              ((not (erc--zMODE-user-p parsed)))
              (buffer (erc--zMODE-buffer parsed)))
    (with-current-buffer buffer
      (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
        (erc-span--on-live-mode parsed)))))

(defun erc-span--handle-live-topic (_ parsed)
  "Ensure a \"live\" range exists when the channel topic changes.
Return the result of calling `erc-span--on-live-topic' with PARSED."
  (when-let* (((null erc-span--active-inserter))
              (buffer (erc--zTOPIC-buffer parsed)))
    (with-current-buffer buffer
      (when (erc-span--ensure-live-span (erc--zresponse-time-str parsed))
        (erc-span--on-live-topic parsed)))))

(defun erc-span--ensure-live-on-new-query (&rest _)
  "Ensure `erc-span--handle-live-privmsg' runs for new query buffers."
  (when-let* (((null erc--target-priors))
              (parsed erc--parsed-response)
              ((erc--zPRIVMSG-p parsed))
              ((erc-query-buffer-p)))
    ;; `erc-open' hasn't yet found the correct buffer.
    (setf (erc--zPRIVMSG-buffer parsed) (current-buffer))
    (erc-span--handle-live-privmsg erc-server-process parsed)))

;; ERC only merges query buffers when the current client (you) changes
;; nicks.  It doesn't support merging when another user renicks
;; because query buffers for the two different targets involved in the
;; renick can cover overlapping or interleaved time ranges, which
;; makes merging prohibitively complex.  For this to work in practice,
;; a backing store, likely a database, would have to handle the merge
;; and the two buffers replaced entirely by new, combined playback.
(defun erc-span--merge-ranges (orig old-buffer new-buffer)
  (let* ((old-ranges (with-current-buffer old-buffer
                       (erc-span--seal-last-live)
                       erc-span--ranges))
         (newest-old (and old-ranges (erc-span--range-get-newest old-ranges)))
         (trim-marker (with-current-buffer new-buffer (point-min-marker)))
         (rv (funcall orig old-buffer new-buffer)))
    (with-current-buffer new-buffer
      (when-let* (((= ?\n (char-before trim-marker)))
                  (hidden (get-text-property (1- trim-marker) 'invisible)))
        (with-silent-modifications
          (delete-region (1- trim-marker) trim-marker)
          (erc--merge-prop (1- trim-marker) trim-marker 'invisible hidden))))
    (set-marker trim-marker nil)
    (if-let* ((old-ranges)
              (new-ranges (buffer-local-value 'erc-span--ranges new-buffer))
              (oldest-new (erc-span--range-get-oldest new-ranges)))
        (progn
          (cl-assert (string-lessp (erc-span--range-ts-hi newest-old)
                                   (erc-span--range-ts-lo oldest-new)))
          ;; Combine ranges.
          (setf (erc-span--range-next newest-old) oldest-new
                (erc-span--range-prev oldest-new) newest-old))
      (with-current-buffer new-buffer
        (cl-assert (null new-ranges))
        (cl-assert (null erc-span--ranges))
        (setq erc-span--ranges old-ranges
              new-ranges old-ranges)))
    ;; Update markers to point to the transplanted bookends in new buffer.
    (with-current-buffer new-buffer
      (let ((p (point-min))
            (node (and erc-span--ranges
                       (erc-span--range-get-oldest erc-span--ranges))))
        (while-let
            ((node)
             ((null (marker-buffer (erc-span--range-marker-lo node))))
             ((null (marker-buffer (erc-span--range-marker-hi node))))
             (lo (text-property-any p erc-insert-marker 'erc-span--range node))
             (hi (text-property-any (1+ lo) erc-insert-marker
                                    'erc-span--range node)))
          (set-marker (erc-span--range-marker-lo node) lo)
          (set-marker (erc-span--range-marker-hi node) hi)
          (setq node (erc-span--range-next node)
                p hi))))
    rv))

;; Helpers to aid in development.  Delete when no longer needed.
(defun erc-span--range-prop-at-point (&optional point interactivep)
  (interactive (list (pos-bol) t))
  (let ((node (get-text-property (or point (point)) 'erc-span--range)))
    (if interactivep
        (let ((prev (erc-span--range-prev node))
              (next (erc-span--range-next node)))
          (message "ts-lo: %s\nts-hi: %s\nprev: %s\nnext: %s"
                   (erc-span--range-ts-lo node)
                   (erc-span--range-ts-hi node)
                   (and prev (erc-span--range-ts-lo prev))
                   (and next (erc-span--range-ts-lo next))))
      node)))

(defun erc-span--range-prop-goto-other-bookend (point)
  (interactive (list (pos-bol)))
  (when-let* ((node (erc-span--range-prop-at-point point))
              (lo (erc-span--range-marker-lo node))
              (hi (erc-span--range-marker-hi node)))
    (if (= lo point) (when hi (goto-char hi)) (goto-char lo))))

(defun erc-span--shorten-truncated-bookend-on-clear (orig beg end)
  "Move opening bookend of newest contained span to after marker END."
  (if-let* ((ranges erc-span--ranges)
            (node (let ((node (erc-span--range-get-oldest ranges)))
                    (while-let ((node)
                                (hi (erc-span--range-marker-hi node))
                                ((< hi end)))
                      (when (setf node (erc-span--range-next node))
                        (setf (erc-span--range-prev node) nil)))
                    node))
            ((< (erc-span--range-marker-lo node) end))
            (tstr (erc-span--ensure-time-str
                   ;; FIXME just scan for next 'erc--ts' with a limit.
                   (or (erc--get-inserted-msg-prop 'erc--ts (1+ end))
                       (and-let* ; skip past an "informational" msg.
                           ((end (erc--get-inserted-msg-end (1+ end)))
                            ((erc--get-inserted-msg-prop 'erc--ts (1+ end)))))
                       (erc--current-time-pair)))))
      (progn
        (funcall orig beg end)
        (setf (erc-span--range-ts-lo node) tstr
              (erc-span--range-ts-lo node) tstr)
        (when (erc-span--range-marker-hi node)
          (cl-assert (>= (erc-span--range-marker-hi node) end))
          (cl-assert (or (string< tstr (erc-span--range-ts-hi node))
                         (string= tstr (erc-span--range-ts-hi node)))))
        ;; Salvage invisible props (should really be any props of
        ;; interest) from the prior newline to be truncated.  Merge
        ;; them with those from the just-extracted trailing newline.
        (let ((erc--msg-prop-overrides `((erc--skip stamp)))
              ;; Shadow msg props from first message after truncation.
              (erc--msg-props nil)
              (erc-insert-modify-hook
               (cons (apply-partially
                      #'erc-span--make-bookend-propertizer node
                      (and (null (erc-span--range-marker-hi node))
                           " (live)"))
                     (ensure-list erc-insert-modify-hook)))
              (obeg (marker-position beg)))
          (cl-assert (= beg end))
          (erc--with-spliced-insertion obeg
            (erc-span--insert-bookend-lo node end))
          (cl-assert (< obeg beg))
          (cl-assert (= end (erc-span--range-marker-lo node)))
          ;; Allow `beg' and `end' to remain as is for other hooks.
          (set-marker (erc-span--range-marker-lo node) obeg)
          (erc-span--assert-markers)))
    (funcall orig beg end)))

(provide 'erc-span)

;;; erc-span.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
