;;; erc-v3.el --- Core IRCv3 extensions for ERC -*- lexical-binding: t; -*-

;; Copyright (C) 2021-2023 Free Software Foundation, Inc.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file adds IRCv3 capability negotiation [1] in accordance with
;; version 302 of the spec.  It provides a module called `v3' that
;; *must* be present in `erc-modules' when connecting for any IRCv3
;; features to come alive.  All extensions requested by default live
;; in this file.  Other IRCv3-related libraries may exist in the ERC
;; tree, and some may contain multiple related extensions.  Feature
;; names don't contain the string "v3", but you can grep for "(require
;; 'erc-v3)" to find them.
;;
;; As long as this library remains a WIP patch set (which may be
;; forever), it will also be bundled with some throwaway modules meant
;; to demo account-related v3 functionality.  At present, there is one
;; called `eldoc' and another called `names', both housed in separate
;; files.
;;
;; [1] https://ircv3.github.io/specs/core/capability-negotiation.html

(require 'erc)
(eval-when-compile (require 'erc-sasl))

(defgroup erc-v3 nil
  "IRCv3 extensions for ERC."
  :group 'erc
  :package-version '(ERC . "5.7")) ; FIXME sync on release

(defcustom erc-v3-extensions '(account-tag
                               batch
                               echo-message
                               server-time
                               standard-replies)
  "List of extensions to enable for a given session.
In addition to these, ERC attempts to enable all foundational
extensions contained in `erc-v3-base-extensions'.  Members of the
combined set represent implementations of IRCv3 specifications,
some of which may not define a corresponding \"REQ'questable\"
capability.  As such, various aspects of \"CAP\" handling may not
always apply.  Note that ERC copies the value of this option into
a session-only variable on `major-mode' init.  For granular
control, let-bind this option around calls to entry-points like
`erc-tls'."
  :type '(set (const account-tag)
              (const batch)
              (const echo-message)
              (const sasl)
              (const server-time)
              (const standard-replies)
              ;; POCs
              (const labeled-response)
              (const draft/multiline)
              (const draft/chathistory)
              (const draft/event-playback)
              (const draft/read-marker)))

(defconst erc-v3-base-extensions
  '(account-notify
    away-notify
    cap-notify
    chghost
    extended-join
    extended-monitor
    message-tags
    multi-prefix
    setname
    userhost-in-names
    ;; Non-caps
    whox
    monitor)
  "Hard-coded essentials that IRCv3 depends on for basic functionality.
ERC attempts to enable these unconditionally when advertised.")

(defconst erc-v3--step-SEEN (cons 0 (make-symbol "seen")))
(defconst erc-v3--step-OFFERED (cons 1 (make-symbol "offered")))
(defconst erc-v3--step-CONSIDERED (cons 2 (make-symbol "considered")))
(defconst erc-v3--step-REQUESTED (cons 3 (make-symbol "requested")))
(defconst erc-v3--step-ANSWERED (cons 4 (make-symbol "answered")))
(defconst erc-v3--step-RESOLVED (cons 5 (make-symbol "resolved")))

(defun erc-v3--step-p (step)
  (memq step (list erc-v3--step-SEEN
                   erc-v3--step-OFFERED
                   erc-v3--step-CONSIDERED
                   erc-v3--step-REQUESTED
                   erc-v3--step-ANSWERED
                   erc-v3--step-RESOLVED)))

(cl-deftype erc-v3--step ()
  "Stages of a capability's life during an IRC session.
Not limited to connection registration.  CONSIDERED means the `wantedp'
slot of the containing `erc-v3--extension' remains non-nil after any
`erc-v3--subcmd-LS' handlers have reviewed the caps marked as OFFERED,
namely those listed by the server in its initial \"LS\" response.
REQUESTED means whatever request was necessary to attain the needed
state change has been sent.  From the point of view of an
`erc-v3--subcmd-ACK' handler, ANSWERED means that *all* \"ACK\"
responses have been received from the server.  The same goes for
`erc-v3--subcmd-NAK' with \"NAK\" responses.  RESOLVED means a cap and
its dependencies were enabled or that the cap and all its dependents
were disabled, as per `wantedp'."
  '(satisfies erc-v3--step-p))

(cl-defstruct erc-v3--extension
  "\"Abstract class\" for basic info relevant to an IRCv3 extension."
  ( canon nil
    :type symbol ; :allocation class
    :read-only t
    :documentation "Name in specification as a canonical symbol.")
  ( depends nil ; :allocation class
    :type (list-of symbol)
    :documentation "Dependencies as canonical symbols.")
  ( supports nil ; merged with ^ when loading
    :type (list-of symbol)
    :documentation "Optional integrations as canonical symbols.")
  ( aliases nil ; :allocation class
    :type (list-of symbol)
    :documentation "Alternate names for the same extension.")
  ( step erc-v3--step-SEEN
    :type erc-v3--step
    :documentation "One of the `erc-v3--step-*' enum states.")
  ( wantedp nil
    :type boolean
    :documentation "Whether ERC should attempt to enable this extension.")
  ( cmd-hist nil
    :type (list-of symbol)
    :documentation "Sub-commands of handlers that have touched this cap."))

(cl-defstruct (erc-v3--capability (:include erc-v3--extension))
  "\"Abstract class\" for basic info relevant to an IRCv3 capability.
ERC defers to the function-valued slots at various stages of the
negotiation process.  For example, it calls `early-init' during
`erc-v3-mode' setup and add its return value, if non-nil, to
`erc-v3--caps-objects'.  Similarly, it calls `enablep' to set the
value of the capability's state variable, which is normally the
session's lone instance of the struct object bearing its name."
  ( key nil
    :type symbol
    :documentation "Name used by server.  Must be in (CANON . ALIASES).")
  ( val nil
    :type (or string null)
    :documentation "Value advertised by server, if any.")
  ( early-init nil
    :type (or function null)
    :documentation "Alt. constructor to initialize object on `erc-v3-mode'.")
  ;; These only run in the server buffer, but all target buffers also
  ;; have their minor-mode variable's value set to the result returned
  ;; by `enablep'.
  ( enablep #'erc-v3--inactive-p
    :type function
    :documentation "Function called with own object to set var enabled.")
  ( disablep #'always
    :type function
    :documentation "Predicate called with own object to set var disabled."))

(defun erc-v3--inactive-p (object)
  (and-let* ((activation-var (get (erc-v3--extension-canon object)
                                  'erc-v3--extension-class))
             ((not (symbol-value activation-var))))
    (cl-assert (eq activation-var (aref object 0)))
    object))

(defun erc-v3--push-tag (tag object)
  "Record a cons of (TAG . STEP-N) in the `cmd-hist' slot of OBJECT.
Expect TAG to be a symbol for a \"CAP\" subcmd.  Use the current `step'
number for STEP-N."
  (push (cons tag (car (erc-v3--extension-step object)))
        (erc-v3--extension-cmd-hist object)))

(defun erc-v3--sync-mode-value (var value)
  (when-let* ((buf (erc-server-buffer))
              ((or (not (eq buf (current-buffer)))
                   (not value))))
    (set var (and value (buffer-local-value var buf)))))

(defmacro erc-v3--define-extension-subclass (default-super body)
  (pcase-let*
      ((head (pop body))
       (name (if (consp head) (pop head) (prog1 head (setq head nil))))
       (doc (if (stringp (car body))
                (pop body)
              (format "IRCv3 `%s' %s." name
                      (substring (symbol-name default-super)
                                 (length "erc-v3--")))))
       ;; Handle various leading keywords in `body'.
       (`(,keymap ,new-slots . ,rev-overs)
        (let ((info (cdr (cl-struct-slot-info default-super)))
              rev-overs keymap new-slots)
          (while (keywordp (car body))
            (pcase-exhaustive (pop body)
              (:slots (setq new-slots (pop body)))
              (:keymap (setq keymap (pop body)))
              ;; Any remaining de-coloned keywords are overriding init
              ;; form parameters for the child's `:include' option.
              ((and k (let slot (intern-soft (substring (symbol-name k) 1)))
                    (guard (and slot (assq slot info))))
               (push (if (seq-some (lambda (e)
                                     (memq e (ensure-list (car body))))
                                   '(:type :documentation :read-only))
                         (cons slot (pop body)) ; full replacement def
                       (list slot (pop body))) ; init form
                     rev-overs))))
          `(,keymap ,new-slots ,@rev-overs)))
       (defname (intern (format "erc-v3--%s" name)))
       (modname (intern (format "erc-v3--%s-mode" name)))
       (overs (reverse rev-overs))
       (inc-def `(:include ,default-super (canon ',name) ,@overs))
       (options (if (keywordp (car (car-safe head)))
                    (if-let* ((include (alist-get :include head)))
                        (prog1 head
                          (unless (assq 'canon include)
                            (dolist (s `(,@rev-overs (canon ',name)))
                              (push s (cdr (alist-get :include head))))))
                      (push inc-def head))
                  (list inc-def))))
    (unless (assq :constructor options)
      (setq options `(,@options (:constructor ,defname))))
    `(progn
       (cl-defstruct (,defname ,@options)
         ,doc
         ,@new-slots)
       ,@(mapcar (lambda (n)
                   `(put ',n 'erc-v3--extension-class ',defname))
                 ;; Find aliases (bar draft/foo) in (quote (bar draft/foo))
                 (cons name (cadar (alist-get 'aliases overs))))
       (put ',name 'erc-v3--extension-minor-mode ',modname)
       (defvar-local ,defname nil
         ,(format "Local instance of `%s' when enabled." defname))
       (define-minor-mode ,modname
         ,(if (stringp (car body))
              (pop body)
            (format "Internal minor mode for `%s'." defname))
         :interactive nil
         :variable ((buffer-local-value ',defname (erc-server-buffer))
                    . (lambda (v) (erc-v3--sync-mode-value ',defname v)))
         ,@body)
       ,@(and keymap `((setf (alist-get ',defname minor-mode-map-alist)
                             ,keymap))))))

(def-edebug-spec erc-v3-extension
  (&define [&or symbolp (symbolp &rest sexp)]
           [&optional stringp] ; cl-struct doc when present
           [&rest [keywordp sexp]]
           [&optional stringp] ; minor-mode doc when present
           def-body))

(defmacro erc-v3--define-extension (&rest body)
  "Define an `erc-v3--extension'-derived struct and supporting items.
Create a struct type named erc-v3--NAME with an identically named
constructor, and have it \"subclass\" `erc-v3--extension'.  Expect NAME
to be a symbol or, alternatively, a list containing a symbol and
`cl-defstruct' options.  If OPTS contains a `:slots' keyword, interpret
its paired value as an unquoted alist of `cl-defstruct' slot definitions
suitable for opaque application state.  Expect users to favor `:slots'
over local variables because the latter need killing in teardown code.
If OPTS contains an `:aliases' keyword, interpret its value, a form that
evaluates to a list of symbols, as an overriding default (i.e.,
\"initform\", \"sdefault\", etc.) for the resulting class's `aliases'
slot.  Moreover, ensure that each member of the list has an
`erc-v3--extension-class' symbol property set to the struct
constructor's symbol, erc-v3--NAME, so that ERC can create equivalent
objects from equivalent caps, like `draft/my-cap' and `my-cap'.  Add any
other keyword pairs matching existing slots to the superclass'
`:include' form in a similar manner.

Additionally, define a variable sharing the constructor's name, but set
its value to nil, and make it local when set.  From this, create an
\"internal\" minor mode with this as its mode variable, and activate it
in all buffers of the connection.  Make this mode non-interactive, but
allow a `:keymap' argument in OPTS to specify the minor mode's keymap.
Interpret all forms following the last keyword/value pair or second doc
string as comprising the minor-mode's body.  In target buffers, arrange
for this body to run alongside normal local-module activation, where
`erc--target-priors' is populated.  In server buffers, have it run
during connection registration, where `erc--server-reconnecting' is
empty and calls to `erc--restore-initialize-priors' ineffective,
although it can also run later if triggered by a \"CAP NEW\".  Expect
users to know that modules depended on and activated by a v3 extension
should have no issue sustaining persistant data across reconnects
because ERC adds global modules to `erc-modules' and automatically
reactivates local modules.

\(fn NAME [STRUCT-DOC] [OPTS [MODE-DOC]] [MODE-BODY])"
  (declare (doc-string 2) (debug erc-v3-extension) (indent defun))
  `(erc-v3--define-extension-subclass erc-v3--extension ,body))

(defmacro erc-v3--define-capability (&rest body)
  "Define an `erc-v3--capability'-derived struct and supporting items.
If keywords `:early-init', `enablep', or `disablep' appear in
OPTS, add their respective values as slot defaults.

See `erc-v3--define-extension' for more info on the various items
defined, and see the unit test of the same name in the Emacs
source-code repository for detailed usage examples.

\(fn NAME [STRUCT-DOC] [OPTS [MODE-DOC]] [MODE-BODY])"
  (declare (doc-string 2) (debug erc-v3-extension) (indent defun))
  `(erc-v3--define-extension-subclass erc-v3--capability ,body))

(cl-defstruct erc-v3--caps
  "Odds and ends related to capability negotiation."
  ( advertised nil
    :documentation "Cache of advertised CAPs."
    :type (list-of cons))
  ( continued nil
    :documentation "Stashed k/v pairs from multiline LS or LIST reply."
    :type (list-of cons))
  ( extensions nil
    :documentation "Snapshot of desired canonical extensions on init."
    :type (list-of symbol)
    :read-only t)
  ( objects nil ; this may make more sense as a hash table
    :documentation "List of `erc-v3--capability' objects."
    :type (list-of erc-v3--capability))
  ( timer nil
    :documentation "Registration request timer."
    :type (or null timer))
  ( handlers nil
    :documentation "Deferred `erc-v3--subcmd-ACK/NAK' handlers."
    :type (list-of (cons symbol function)))
  ( negotiated-p nil
    :documentation "Non-nil when CAP END has been sent."
    :type bool))

(defvar-local erc-v3--caps nil
  "Unique `erc-v3--caps' object for server process.
It's local to the server buffer.")

(cl-defmethod erc-v3--non-cap-ext-p (canon)
  "Return nil if CANON is a subclass of `erc-v3--capability'."
  (let ((cls (get canon 'erc-v3--extension-class)))
    (and (memq cls cl-struct-erc-v3--extension-tags)
         (not (memq cls cl-struct-erc-v3--capability-tags)))))

(cl-defmethod erc-v3--non-cap-ext-p ((_ erc-v3--capability))
  "Return nil if OBJ is an instance of `erc-v3--capability'."
  nil)

(defvar erc-v3--extension-manifest-review-hook
  '(erc-v3--sasl-on-extension-manifest-review)
  "Hook for bootstraping extensions that conflict with core ERC libraries.")

(defun erc-v3--activate-module ()
  "Perform steps to initialize buffer for `erc-v3-mode'."
  (if erc--target
      (let* ((server-buffer (erc-server-buffer))
             (caps (buffer-local-value 'erc-v3--caps server-buffer)))
        (dolist (o (erc-v3--caps-objects caps))
          (when-let* ((canon (erc-v3--extension-canon o))
                      (variable (get canon 'erc-v3--extension-class))
                      ((buffer-local-value variable server-buffer))
                      (mode (get canon 'erc-v3--extension-minor-mode)))
            (funcall mode +1))))
    ;; Maybe use `cl-union' here.
    (let* ((erc-v3-extensions erc-v3-extensions)
           (exts (progn
                   (run-hooks 'erc-v3--extension-manifest-review-hook)
                   (seq-uniq (append erc-v3-base-extensions erc-v3-extensions)
                             #'eq)))
           objects)
      (dolist (ext exts)
        (require (or (get ext 'erc--feature)
                     (intern (concat "erc-"
                                     (if-let* ((n (symbol-name ext))
                                               ((string-prefix-p "draft/" n)))
                                         (substring n 6)
                                       n))))
                 nil 'no-error)
        (when-let*
            ((constructor (get ext 'erc-v3--extension-class))
             ;; Only capabilities have an `early-init' slot.
             (o (if-let* ((init-fn (erc-v3--get-slot-initform constructor
                                                              'early-init)))
                    (funcall init-fn)
                  (when (erc-v3--non-cap-ext-p ext)
                    (funcall constructor :wantedp t)))))
          (push o objects)))
      (setq exts (mapcar #'erc-v3--canon-from-key exts)
            erc-v3--caps (make-erc-v3--caps :extensions (delq nil exts)
                                            :objects (nreverse objects))))))

(define-erc-module v3 nil
  "IRCv3 extension support for ERC.
When activating this local module, take steps to ensure capability
negotiation will commence and the necessary bookkeeping performed to
carry the protocol dialog to completion.  When deactivating, print an
error message and destroy the entire ERC session.

Note that in server buffers, `erc-v3-mode-hook' runs before extension
activation variables of the form erc-v3--FOO have been set, unless they
specify an `early-init' function.  The trade-offs between this hook and
\"ACK\" and \"NAK\" sub-command methods are subtle and sometimes
nonexistent."
  ((unless erc--target
     (pcase erc-tags-format
       ('legacy
        (erc--warn-once-before-connect 'erc-v3-mode
          "Module `v3' is incompatible with an `erc-tags-format' value of"
          " `legacy'.  Disabling module now...")
        (setq erc-v3-mode nil))
       ('overridable
        (erc--warn-once-before-connect 'erc-v3-mode
          "Module `v3' overrides legacy `erc-parse-tags' behavior."
          " Customize `erc-tags-format' to nil to silence this message."))))
   (unless (eq erc-tags-format 'legacy)
     (erc-v3--activate-module)))
  ;; XXX tearing this down properly is likely not worth implementing
  ;; because it would require extensions to provide a cap-negation
  ;; implementation.  (It's much easier to just destroy the session
  ;; and reconnect.)  To make this work, you'd have to REQ -foo all
  ;; extensions and wait until they're ACK'd before killing relevant
  ;; local vars, all while ensuring operations can be run repeatedly.
  ;; There's also the question of the local snapshot of
  ;; `erc-v3-extensions'.  Should you honor it when reconnecting or
  ;; start from scratch?
  ((unless erc--target
     (kill-local-variable 'erc-v3--caps)
     (kill-local-variable 'erc-v3--353-work)
     (erc-button--display-error-notice-with-keys-and-warn
      (erc-server-buffer)
      "Disabling `erc-v3-mode' is not currently supported."
      (and (memq 'v3 erc-modules)
           " Please remove `v3' from `erc-modules' if unwanted.")
      (and (erc-server-process-alive)
           (progn (delete-process erc-server-process)
                  " Disconnecting now.")))))
  localp)

(cl-defmethod erc--register-connection (&context (erc-v3-mode (eql t)))
  "Start capability negotiation with server."
  (erc-server-send "CAP LS 302")
  (erc-login))

(defun erc-v3--partition-cap-tokens (tokens)
  "Partition TOKENS into cons cells.
Return an alist with elements like (CAP . VAL) or (CAP), where CAP is a
symbol and VAL is a string or nil."
  (let (result)
    (dolist (item tokens (nreverse result))
      (let* ((pos (string-search "=" item))
             (key (intern (if pos (substring item nil pos) item)))
             (val (and pos (substring item (1+ pos)))))
        (push (cons key val) result)))))

(defun erc-v3--on-registration-time-out (buffer caps)
  (require 'button)
  (when (buffer-live-p buffer)
    (with-current-buffer buffer
      (run-hook-with-args 'erc-quit-hook erc-server-process)
      (delete-process erc-server-process)
      (let ((erc-active-buffer buffer))
        (erc-button--display-error-notice-with-keys
         buffer
         "Failed to resolve extensions. Please report via \\[erc-bug]."
         (format " CAP states: %S"
                 (mapcar (lambda (o) (cons (erc-v3--extension-canon o)
                                           (erc-v3--extension-step o)))
                         (erc-v3--caps-objects caps))))))))

(defvar erc-v3--request-timeout-seconds 30)

(defun erc-v3--send-requests ()
  "Send all \"considered\" requests in one or more \"CAP REQ\" commands.
If there aren't any, do nothing and return nil.  Otherwise
create, store, and return a request timer."
  (when (cl-find erc-v3--step-CONSIDERED (erc-v3--caps-objects erc-v3--caps)
                 :key #'erc-v3--extension-step)
    (let (todo pending)
      (dolist (o (erc-v3--caps-objects erc-v3--caps))
        (when (eq (erc-v3--extension-step o) erc-v3--step-CONSIDERED)
          (setf (erc-v3--extension-step o) erc-v3--step-REQUESTED)
          (push (erc-v3--capability-key o) pending)
          (push (symbol-name (erc-v3--capability-key o)) todo)))
      (while-let ((todo) ; 29 has `seq-split'
                  (this (take 10 todo)))
        (erc-server-send (format "CAP REQ :%s" (string-join this " ")))
        (setq todo (nthcdr 10 todo))))
    (cl-assert (not (erc-v3--caps-timer erc-v3--caps)))
    (setf (erc-v3--caps-timer erc-v3--caps)
          (run-at-time erc-v3--request-timeout-seconds nil
                       #'erc-v3--on-registration-time-out
                       (current-buffer) erc-v3--caps))))

(defun erc-v3--run-subcmds-piecemeal (pairs subcmd)
  "Run all individual \"CAP\" sub-command handlers.
PAIRS is an alist of (KEY . VALUE), where KEY is a symbol and
VALUE is a string or nil.  SUBCMD is the sub-command."
  (pcase-dolist (`(,key . ,_) pairs)
    (when-let* ((canon (erc-v3--canon-from-key key))
                (o (cl-find canon (erc-v3--caps-objects erc-v3--caps)
                            :key #'erc-v3--extension-canon)))
      (funcall subcmd o))))

(defun erc-v3--server-CAP-finish ()
  "Finalize \"CAP\" dialog."
  (unless (cl-find erc-v3--step-REQUESTED (erc-v3--caps-objects erc-v3--caps)
                   :key #'erc-v3--extension-step)
    (when-let* ((timer (erc-v3--caps-timer erc-v3--caps)))
      (cancel-timer timer)
      (setf (erc-v3--caps-timer erc-v3--caps) nil))
    (unless (erc-v3--caps-negotiated-p erc-v3--caps)
      (erc-server-send "CAP END")
      (setf (erc-v3--caps-negotiated-p erc-v3--caps) t))))

(cl-defmethod erc--server-CAP (_proc parsed &context (erc-v3-mode (eql t)))
  "Initialize, evolve, and terminate a \"CAP\" dialog.
Handle \"CAP\" sub-commands, though perhaps not immediately.  Do
this throughout an IRC session, not only during connection
registration.  Don't run handlers if the response lacks a
trailing argument.  Always send a \"CAP END\" during registration
if no requests are pending (possibly undesirable in some cases).
Always return nil."
  ;; Run aggregate handlers.
  (let ((subcmd (intern (nth 1 (erc-response.command-args parsed)))))
    (when-let* ((toks (split-string (erc-response.contents parsed)))
                (pairs (erc-v3--partition-cap-tokens toks)))
      (erc-v3--dispatch-subcmd pairs parsed subcmd)))
  ;; Send requests or end dialog.
  (unless (or (erc-v3--caps-continued erc-v3--caps)
              (erc-v3--send-requests))
    (erc-v3--server-CAP-finish))
  nil)

(define-erc-response-handler (CAP)
  "Handle \"CAP\" response or server-initiated command.
Return nil to allow `erc-call-hooks' to keep going."
  nil)

(defvar erc-message-english-s410 "Invalid CAP subcommand: %s"
  "English `erc-format-message' template for key `s410'.")

;; FIXME destroy session here.
(define-erc-response-handler (410)
  "Protocol error 410 (ERR_INVALIDCAPCMD) for unknown subcommand." nil
  (erc-display-message parsed '(notice error) (erc-server-buffer)
                       's410 ?s (cadr (erc-response.command-args parsed))))


;;;; Dependency sorting

(define-error 'erc-v3-dependency-error "IRCv3 extension resolution error")

(defun erc-v3--deps-plumb (objects object)
  "Collect `erc-v3--extension' OBJECT's dependencies.
Do it by exploring OBJECTS, depth-first."
  (let (out)
    (named-let again ((o object))
      (dolist (dep (erc-v3--extension-depends o))
        (let ((depo (cl-find dep objects :key #'erc-v3--extension-canon)))
          (unless depo
            (signal 'erc-v3-dependency-error
                    (list :missing dep
                          :wanted-by (erc-v3--extension-canon o))))
          (cl-pushnew depo out)
          (again depo))))
    out))

(defun erc-v3--deps-gather-dependents (objects object)
  "Return members of OBJECTS that depend on OBJECT."
  (let (out)
    (dolist (o objects out)
      (let ((depos (erc-v3--deps-plumb objects o)))
        (when (memq object depos)
          (cl-pushnew o out))))))

(defun erc-v3--deps-build-dag (objects &optional softp)
  "Return an alist of nodes.
With SOFTP, include soft dependencies from OBJECTS' `support' slots.
Represent each as an \"adjacency list\" of the form (U V1 ... Vn)
with out-degree n > 0, where all the UVi's are directed edges.
Omit sinks."
  (let (graph)
    (dolist (o objects graph)
      (let ((v (erc-v3--extension-canon o)))
        (dolist (u (if softp ; would be nice to iterate lazily
                       (append (erc-v3--extension-supports o)
                               (erc-v3--extension-depends o))
                     (erc-v3--extension-depends o)))
          (cl-pushnew v (alist-get u graph)))))))

;; We may also need a means of doing this with "classes" (struct
;; def-symbols) rather than their instances.
(defun erc-v3--deps-tsort (objects graph)
  "Return OBJECTS sorted by their dependencies.
OBJECTS is a list of `erc-v3--extension' instances."
  (let ((visited (make-hash-table))
        (todo objects)
        ;;
        stack)
    (while-let ((o (pop todo))
                (u (erc-v3--extension-canon o)))
      (unless (gethash u visited)
        (named-let doit ((u u))
          (puthash u t visited)
          (dolist (v (alist-get u graph))
            (unless (gethash v visited)
              (doit v)))
          (let ((o (cl-find u objects :key #'erc-v3--extension-canon)))
            (unless o
              (signal 'erc-v3-dependency-error (list :missing u)))
            (push o stack)))))
    stack))


;;;; Sub-command helpers

(define-inline erc-v3--cap-negated-p (cap-sym)
  "Indicate whether CAP-SYM is negated (has a leading hyphen)."
  (inline-quote (eq ?- (aref (symbol-name ,cap-sym) 0))))

(defun erc-v3--check-continued (alist response)
  "Update the `continued' slot of `erc-v3--caps' from ALIST.
Return the updated value if RESPONSE is terminal and not to be continued."
  (let ((caps (cl-callf (lambda (existing) (nconc existing alist))
                  (erc-v3--caps-continued erc-v3--caps))))
    (unless (string= "*" (nth 2 (erc-response.command-args response)))
      (setf (erc-v3--caps-continued erc-v3--caps) nil)
      caps)))

(defun erc-v3--update-advertised-caps (alist)
  (let (new)
    (dolist (cap alist)
      (if-let* ((found (assq (car cap)
                             (erc-v3--caps-advertised erc-v3--caps))))
          (setcdr found (cdr cap))
        (push cap new)))
    (when new
      (cl-callf (lambda (val) (nconc val (nreverse new)))
          (erc-v3--caps-advertised erc-v3--caps)))))

(defun erc-v3--find-capability-by-key (key)
  "Return `erc-v3--capability' matching KEY."
  (cl-find (erc-v3--canon-from-key key) (erc-v3--caps-objects erc-v3--caps)
           :key #'erc-v3--extension-canon))

(defun erc-v3--add-offered-caps (alist tag)
  "Add an object for all (K . V) in ALIST to `erc-v3--caps-objects'.
Expect TAG to be a symbol used internally for debugging.  Set `wantedp'
for caps in `erc-v3--caps-extensions' to t."
  (pcase-dolist (`(,k . ,v) alist)
    (cl-assert (not (erc-v3--cap-negated-p k)))
    (let (o)
      ;; Ignore undefined caps.
      (when-let* ((constructor (get k 'erc-v3--extension-class)))
        (if (setq o (erc-v3--find-capability-by-key k))
            ;; Object was somehow initialized earlier.  Ignore plain
            ;; extensions, objects not created by an `early-init' func,
            ;; and those for caps not offered or already offered.
            (unless
                (or (erc-v3--non-cap-ext-p o)
                    (null (erc-v3--capability-early-init o))
                    (not (eq (erc-v3--extension-step o) erc-v3--step-SEEN)))
              ;; Preserve order by moving early birds to the end.
              (erc-v3--push-tag tag o)
              (setf (erc-v3--caps-objects erc-v3--caps)
                    (delq o (erc-v3--caps-objects erc-v3--caps)))
              (setf (erc-v3--extension-step o) erc-v3--step-OFFERED
                    (erc-v3--capability-key o) k
                    (erc-v3--capability-val o) v)
              (push o (erc-v3--caps-objects erc-v3--caps)))
          ;; This is a capability that wasn't initialized early.
          (setq o
                (funcall constructor :step erc-v3--step-OFFERED :key k :val v))
          (erc-v3--push-tag tag o)
          (push o (erc-v3--caps-objects erc-v3--caps)))
        (when (and o (memq (erc-v3--capability-canon o)
                           (erc-v3--caps-extensions erc-v3--caps)))
          (setf (erc-v3--capability-wantedp o) t))))))

(defvar erc-v3--capability-cached-initforms (make-hash-table :test #'equal)
  "Hash table with composite (CONSTRUCTOR . SLOT) keys and boxed (VALUE)s.")

(defun erc-v3--get-slot-initform (constructor slot)
  "Return default value for SLOT in CONSTRUCTOR's struct.
Return nil if not found or if CONSTRUCTOR does not belong to an
`erc-v3--capability' class."
  ;; Though slower, this seems less brittle than drilling down via
  ;; something like:
  ;;
  ;;   (and-let* ((n (cl-struct-slot-offset constructor slot))
  ;;              (class (cl-find-class constructor))
  ;;              (slots (cl--struct-class-slots class)))
  ;;     (cl--slot-descriptor-initform (aref slots (1- n))))
  ;;
  (car (with-memoization (gethash (cons constructor slot)
                                  erc-v3--capability-cached-initforms)
         (list (and-let* ((info (cl-struct-slot-info constructor))
                          ((assq 'key info))
                          (val (car (alist-get slot info)))) ; cap-p
                 (pcase val
                   (`(quote ,v) v)
                   (`(function ,v) v)
                   (v v)))))))

(defun erc-v3--canon-from-key (key)
  "Return canonical capability name for KEY."
  (when (erc-v3--cap-negated-p key)
    (setq key (intern (substring (symbol-name key) 1))))
  (and-let* ((constructor (get key 'erc-v3--extension-class)))
    (erc-v3--get-slot-initform constructor 'canon)))

(defun erc-v3--consider-offered (alist tag)
  "Mark members of ALIST as being wanted.
But only do so for those whose canonical name appears in
`erc-v3--caps-extensions'.  Expect TAG to be a symbol representing a
sub-command."
  (cl-assert (memq tag '(LS NEW)))
  (let (haystack wanted)
    (dolist (o (erc-v3--caps-objects erc-v3--caps))
      (when (and (erc-v3--capability-p o)
                 (erc-v3--capability-wantedp o)
                 (eq (erc-v3--extension-step o) erc-v3--step-OFFERED)
                 (assq (erc-v3--capability-key o) alist))
        (push o wanted)
        (setf (erc-v3--extension-step o) erc-v3--step-CONSIDERED)
        (erc-v3--push-tag tag o)))
    (setq haystack
          (if (eq 'NEW tag) (erc-v3--caps-objects erc-v3--caps) wanted))
    (dolist (o wanted)
      (condition-case err
          (erc-v3--deps-plumb haystack o)
        (erc-v3-dependency-error
         (erc-button--display-error-notice-with-keys
          (current-buffer)
          (format "Problem resolving dependencies: %s." err)
          (format " Disabling capability '%s' for this session."
                  (erc-v3--capability-key o))
          " Please see option `erc-v3-extensions'.")
         (setf (erc-v3--extension-step o) erc-v3--step-OFFERED
               (erc-v3--capability-wantedp o) nil))))))

;; Each extension EXT has a local variable named erc-v3--EXT that's
;; normally set to the session's instance of the extension's own
;; `erc-v3--extension'-derived object.  In special cases, it can also
;; be another a non-nil object.  During module setup, `erc-v3-mode'
;; activation code copies this variable's value from the server buffer
;; to the newly created target buffer (via the extension's minor-mode
;; activation toggle).  The following few functions set and unset
;; these variables.  The last two call the `enablep' and `disablep'
;; slots of the relevant extension object to determine the variable's
;; value (and whether to proceed).

(defun erc-v3--mark-extension-enabled (canon &optional value)
  "Set active extension instance variable for symbol CANON to VALUE.
On success, mark the working `erc-v3--extension' instance for CANON as
`resolved', and return non-nil.  Without VALUE, set the active instance
of CANON's extension class to the working instance rather than
instantiate a new one via `erc-v3--capability-enablep'.  In all cases,
expect CANON's object to have a non-nil `wantedp' slot, and expect to be
called only for special cases where `erc-v3--ensure-enabled' doesn't
make sense."
  (cl-assert (erc--server-buffer-p))
  ;; `objects' can be empty.
  (when-let* ((o (cl-find canon (erc-v3--caps-objects erc-v3--caps)
                          :key #'erc-v3--extension-canon))
              (var (get canon 'erc-v3--extension-class))
              (mode (get canon 'erc-v3--extension-minor-mode)))
    (cl-assert (erc-v3--extension-wantedp o))
    (cl-assert (not (eq (or value o) (symbol-value var))))
    (set var (or value o))
    (erc-with-all-buffers-of-server erc-server-process nil
      (funcall mode +1))
    (setf (erc-v3--extension-step o) erc-v3--step-RESOLVED)))

(defun erc-v3--mark-extension-disabled (canon &optional new-state)
  "Disable the active instance variable for symbol CANON.
Set this instance's step to `offered' or, when provided, NEW-STATE.

The default NEW-STATE is `offered' because there's no reason to disable
an extension that was merely SEEN.  And the rationale for not setting
`wantedp' to nil is that caps can be advertised after registration with
\"CAP NEW\" and \"ISUPPORT\"-based extensions after MOTD's end via
one-off \"005\"s."
  (cl-assert (erc--server-buffer-p))
  (let ((obj (cl-find canon (erc-v3--caps-objects erc-v3--caps)
                      :key #'erc-v3--extension-canon))
        (var (get canon 'erc-v3--extension-class))
        (mode (get canon 'erc-v3--extension-minor-mode)))
    (cl-assert (boundp var))
    (cl-assert (fboundp mode))
    (set var nil)
    (erc-with-all-buffers-of-server erc-server-process nil
      (funcall mode +1))
    (setf (erc-v3--extension-step obj) (or new-state erc-v3--step-OFFERED))))

(defun erc-v3--ensure-enabled (candidates)
  "Enable CANDIDATES and their dependencies.
Do so in topo-sorted order so that the least dependent extensions
come alive first.  For each `enablep' function that returns
non-nil, set its \"extension variable\" to whatever it returned,
and set its state to `resolved'."
  (let* ((gather-fn (apply-partially #'erc-v3--deps-plumb
                                     (erc-v3--caps-objects erc-v3--caps)))
         (gathered (mapcan gather-fn candidates))
         (gathered (delete-dups `(,@gathered ,@candidates)))
         (graph (erc-v3--deps-build-dag gathered 'softp))
         (wanted (erc-v3--deps-tsort gathered graph)))
    (dolist (o wanted)
      (when-let* ((variable (get (erc-v3--extension-canon o)
                                 'erc-v3--extension-class))
                  (value (funcall (erc-v3--capability-enablep o) o))
                  (mode (get (erc-v3--extension-canon o)
                             'erc-v3--extension-minor-mode)))
        (set variable value)
        (erc-with-all-buffers-of-server erc-server-process nil
          (funcall mode +1))
        (setf (erc-v3--extension-step o) erc-v3--step-RESOLVED)))))

(defun erc-v3--ensure-disabled (candidates new-state)
  "Disable CANDIDATES and their dependencies.
Do so in reverse topo-sorted order so that the most dependent
objects go down first.  Set all states to NEW-STATE."
  (cl-assert new-state)
  (let* ((gather-fn (apply-partially #'erc-v3--deps-gather-dependents
                                     (erc-v3--caps-objects erc-v3--caps)))
         (gathered (mapcan gather-fn candidates))
         (gathered (delete-dups `(,@candidates ,@gathered)))
         (graph (erc-v3--deps-build-dag gathered))
         (doomed (erc-v3--deps-tsort gathered graph)))
    (dolist (o (nreverse doomed))
      (when-let* ((variable (get (erc-v3--extension-canon o)
                                 'erc-v3--extension-class))
                  ((symbol-value variable))
                  ((funcall (erc-v3--capability-disablep o) o))
                  (mode (get (erc-v3--extension-canon o)
                             'erc-v3--extension-minor-mode)))
        (set variable nil)
        (erc-with-all-buffers-of-server erc-server-process nil
          (funcall mode -1)))
      (setf (erc-v3--extension-step o) new-state))))

;; ACK handling is complicated
;;
;; > If an ACK reply originating from the server is spread across
;; > multiple lines, a client MUST NOT change capabilities until the
;; > last ACK of the set is received.
;;
;; What this means is that a server may not send all ACKs for one or
;; more REQs in a single message.  This shouldn't be confused with
;; multi-line LS and LIST responses.
;;
;; ACK methods for non-negated extensions should just specialize on
;; EXT as normal.

(defun erc-v3--run-deferred-subcmd-handlers (subcmd)
  "Run all deferred sub-command handlers for SUBCMD.
Set value for key SUBCMD in `erc-v3--caps-handlers' to nil."
  ;; A handler may modify the `step' slot to do things like inhibit a
  ;; request from being issued.
  (cl-callf (lambda (handlers)
              (dolist (handler (nreverse handlers))
                (funcall handler)))
      (alist-get subcmd (erc-v3--caps-handlers erc-v3--caps))))

(defun erc-v3--act-on-answered (&rest subcmds)
  "Run ACK handlers and enable extensions when responses have arrived.
Also run NAK handlers and disable rejected extensions."
  (unless (cl-find erc-v3--step-REQUESTED (erc-v3--caps-objects erc-v3--caps)
                   :key #'erc-v3--extension-step)
    (dolist (subcmd subcmds)
      ;; Run all enqueued handlers.
      (erc-v3--run-deferred-subcmd-handlers subcmd))
    ;; Disable all negated caps after disabling their dependents.
    (erc-v3--ensure-disabled
     (seq-filter (lambda (o)
                   (and (erc-v3--capability-p o)
                        (<= (car erc-v3--step-ANSWERED)
                            (car (erc-v3--extension-step o)))
                        (not (erc-v3--capability-wantedp o))))
                 (erc-v3--caps-objects erc-v3--caps))
     erc-v3--step-OFFERED)
    ;; Enable all other caps after their dependencies.
    (erc-v3--ensure-enabled
     (seq-filter (lambda (o)
                   (and (erc-v3--capability-p o)
                        (<= (car erc-v3--step-ANSWERED)
                            (car (erc-v3--extension-step o)))
                        (erc-v3--capability-wantedp o)))
                 (erc-v3--caps-objects erc-v3--caps)))
    nil)) ; don't run individual handlers

(defun erc-v3--flip-extension-goal (object)
  "Invert `wantedp' slot of extension OBJECT."
  (cl-callf not (erc-v3--extension-wantedp object)))

(defun erc-v3--mark-answered (alist tag on-found-fn)
  "Handle common \"ACK\" and \"NAK\" business."
  (pcase-dolist (`(,k . ,v) alist)
    (when-let* ((o (erc-v3--find-capability-by-key k)))
      (cl-assert (not v))
      (erc-v3--push-tag tag o)
      (setf (erc-v3--extension-step o) erc-v3--step-ANSWERED)
      (cl-assert (eq (erc-v3--cap-negated-p k)
                     (not (erc-v3--extension-wantedp o))))
      (when on-found-fn
        (funcall on-found-fn o)))))


;;;; Sub-command handlers

(defun erc-v3--handle-subcmd-del (alist tag)
  "Perform deletion as directed by a \"DEL\" subcommand.
Expect items in ALIST to be cons cells representing capabilities."
  (let (need-disabled)
    ;; Individual handlers for each deleted cap (or the "catch-all"
    ;; handler) must do the rest.
    (pcase-dolist (`(,k . ,_) alist)
      (cl-assert (not (erc-v3--cap-negated-p k)))
      (cl-callf (lambda (val)
                  (if-let* ((found (assq k val))) (remq found val) val))
          (erc-v3--caps-advertised erc-v3--caps))
      (when-let* ((o (erc-v3--find-capability-by-key k)))
        (erc-v3--push-tag tag o)
        (push o need-disabled)))
    (when need-disabled
      (erc-v3--ensure-disabled need-disabled erc-v3--step-SEEN))))

(defun erc-v3--handle-subcmd-list (alist response)
  "Print the RESPONSE to a \"LIST\" query.
Expect items in ALIST to be cons cells representing enabled capabilities."
  (when-let* ((caps (erc-v3--check-continued alist response))
              (paired (mapcar (pcase-lambda (`(,k . ,v))
                                (cl-assert (null v))
                                (symbol-name k))
                              caps))
              (msg (concat "Enabled CAPs: " (string-join paired ", "))))
    (erc-display-message response 'notice 'active msg)))

(defun erc-v3--dispatch-subcmd (alist response subcmd)
  "Process SUBCMD taking one or more caps in a terminal argument.
Expect ALIST to be a cons of (KEY . VALUE) where VALUE is a
string or nil and KEY is a symbol representing a capability on
the server.  Expect RESPONSE to be an `erc-response' object."
  (pcase-exhaustive subcmd
    ;; Handle a "LS" response if it's been received in its entirety.
    ('LS (when-let* ((caps (erc-v3--check-continued alist response)))
           (setf (erc-v3--caps-advertised erc-v3--caps) caps)
           (erc-v3--add-offered-caps caps 'LS)
           (erc-v3--run-subcmds-piecemeal caps #'erc-v3--subcmd-LS)
           (erc-v3--consider-offered caps 'LS)))
    ;; Remember when a parameter is negated so it can be disabled once
    ;; all outstanding requests have arrived.
    ('ACK (erc-v3--mark-answered alist 'ACK nil)
          (erc-v3--run-subcmds-piecemeal alist #'erc-v3--subcmd-ACK)
          (erc-v3--act-on-answered 'NAK 'ACK))
    ;; When a parameter is negated, assume its capability is enabled
    ;; or slated to be and that the request that elicited this
    ;; response asked for it to be disabled.
    ('NAK (erc-v3--mark-answered alist 'NAK #'erc-v3--flip-extension-goal)
          (erc-v3--run-subcmds-piecemeal alist #'erc-v3--subcmd-NAK)
          (erc-v3--act-on-answered 'ACK 'NAK))
    ;; TODO find out if `NEW' can arrive during connection registration.
    ('NEW (erc-v3--add-offered-caps alist 'NEW)
          (erc-v3--update-advertised-caps alist)
          (erc-v3--run-subcmds-piecemeal alist #'erc-v3--subcmd-NEW)
          (erc-v3--consider-offered alist 'NEW))
    ('DEL (erc-v3--run-subcmds-piecemeal alist #'erc-v3--subcmd-DEL)
          (erc-v3--handle-subcmd-del alist 'DEL))
    ('LIST (erc-v3--handle-subcmd-list alist response))))

(cl-defmethod erc-v3--subcmd-LS (capability)
  "Handle a \"CAP LS\" response for `erc-v3--capability' CAPABILITY.
Unless modified by another method, expect CAPABILITY's `state' slot to
be `erc-v3--step-OFFERED'.  To suppress a subsequent \"REQ\" for CAPABILITY,
set its `wantedp' slot to nil."
  capability)

(cl-defmethod erc-v3--subcmd-ACK (capability)
  "Handle a \"CAP ACK\" subcommand for `erc-v3--capability' CAPABILITY.
Unless modified by another method, expect CAPABILITY's mode variable,
like `erc-v3--cap-notify', to be unset and its `state' slot to be
`erc-v3--step-ANSWERED'."
  capability)

(cl-defgeneric erc-v3--subcmd-NAK (capability)
  "Handle a \"CAP NAK\" subcommand for `erc-v3--capability' CAPABILITY.
Unless modified by another method, expect CAPABILITY's mode variable,
like `erc-v3--cap-notify', to be unset and its `state' slot to be
`erc-v3--step-ANSWERED'."
  capability)

(cl-defmethod erc-v3--subcmd-ACK :around (cap)
  "Defer \"ACK\" handling until all requests have been answered."
  (push (lambda () (erc-v3--push-tag 'ACK cap) (cl-call-next-method))
        (alist-get 'ACK (erc-v3--caps-handlers erc-v3--caps))))

(cl-defmethod erc-v3--subcmd-NAK :around (cap)
  "Defer \"NAK\" handling until all requests have been answered."
  (push (lambda () (erc-v3--push-tag 'NAK cap) (cl-call-next-method))
        (alist-get 'NAK (erc-v3--caps-handlers erc-v3--caps))))

(cl-defmethod erc-v3--subcmd-NEW (capability)
  "Handle a \"CAP NEW\" subcommand for `erc-v3--capability' CAPABILITY."
  capability)

(cl-defmethod erc-v3--subcmd-DEL (capability)
  "Handle a \"CAP DEL\" subcommand for `erc-v3--capability' CAPABILITY."
  capability)


;;;; Extension: cap-notify

(erc-v3--define-capability cap-notify)


;;;; Extension: multi-prefix

(erc-v3--define-capability multi-prefix)

(defvar erc-message-english-s352-v3 "%-11c %-10n %-3s %-2a %u@%h (%f)"
  "English `erc-message-template' template for key `s352-v3'.")

(defun erc-v3--multi-prefix--partition (name statuses)
  "Parse NAME a status-prefixed nick or full NUH.
Use STATUSES to determine the set of valid channel-membership prefixes.
Return a list of (STATUS SUBSET NUH), where STATUS is the numeric union
of all applicable statuses, SUBSET is the string of enabled status
chars, and NUH is the nick or full NUH suffixing NAME.  Note that this
function is backwards compatible."
  (let ((status 0)
        (count 0))
    (catch 'done
      (erc--doarray (c name)
        (if-let* ((p (erc--strpos c statuses)))
            (setq status (logior status (ash 1 p))
                  count (1+ count))
          (throw 'done t))))
    (list status (substring name 0 count) (substring name count))))

(defun erc-v3--multi-prefix-split-flags (flag-string)
  "Return a list of parts from RPL_WHOREPLY 352 or 354 FLAG-STRING.
Ensure value looks like (AWAY OPER-P (STATUS . PREFIX) REST),
where AWAY is the away char ?H, for here, or ?G, for gone; OPER-P
is a boolean indicating whether the user is an operator; STATUS
is an internal number representing PREFIX; and REST is a list of
remaining chars, like ?B for bot or ?r for registered (varies per
IRCd)."
  (unless (string-empty-p flag-string)
    (let* ((away (aref flag-string 0)) ; either ?H or ?G
           (oper (and (> (length flag-string) 1) (eq ?* (aref flag-string 1))))
           (alist (erc--parsed-prefix-alist (erc--parsed-prefix)))
           (statuses (erc--parsed-prefix-statuses (erc--parsed-prefix)))
           (remaining (substring flag-string (if oper 2 1))))
      ;; Here, `rest' is usually something like (?B ?r) or (?s)
      (pcase-let* ((`(,status ,prefix ,rest)
                    (erc-v3--multi-prefix--partition remaining statuses))
                   (letters (apply #'string
                                   (mapcar (lambda (c) (car (rassq c alist)))
                                           prefix))))
        (list away oper (cons status letters) (append rest nil))))))

(defun erc-v3--update-server-user (nick host full-name login account info)
  "Add or update `erc-server-users' entry with params.
Pass INFO to `erc--ensure-server-user-info' before committing."
  (if-let* ((server-user (erc-get-server-user nick)))
      (erc-update-user server-user nick host login full-name)
    (erc-add-server-user nick (make-erc-server-user :nickname nick
                                                    :host host
                                                    :full-name full-name
                                                    :login login
                                                    :account account)
                         info)))

;; This applies to basic /WHO #chan, etc.  Automated WHOs sent by this
;; module are of the WHOX variety.
(cl-defmethod erc--server-352
  (_proc parsed &context (erc-v3--multi-prefix erc-v3--multi-prefix))
  "Inhibit `erc-server-352' and handle PARSED RPL_WHOREPLY.
Act like the default handler but attempt to parse multi-prefixes.
Update the server user instead when channel is an asterisk."
  (pcase-let* ((`(,channel ,login ,host ,_server ,nick ,flags ,hop-real)
                (cdr (erc-response.command-args parsed)))
               (`(,away-flag ,_oper (,status . ,letters) ,_rest)
                (erc-v3--multi-prefix-split-flags flags))
               ;; Discard hops in trailing arg.
               (full-name (and (string-match (rx (: bot (+ (any "0-9")) " ")
                                                 (group (* nonl)) eot)
                                             hop-real)
                               (match-string 1 hop-real))))
    (if (equal channel "*")
        (erc-v3--update-server-user nick host full-name login nil nil)
      (erc-with-buffer (channel)
        (erc--update-current-channel-member
         (erc-get-channel-member nick) status nil nick host login full-name)
        (erc-display-message parsed 'notice (current-buffer) 's352-v3
                             ?c channel ?n nick ?a (string away-flag)
                             ?s letters
                             ?u login ?h host ?f full-name))))
  nil)


;;;; Extension: userhost-in-names

(erc-v3--define-capability userhost-in-names)


;;;; Extension: message-tags

(erc-v3--define-capability message-tags
  :slots ((seen (make-hash-table :test #'equal :weakness 'key)
                :type hash-table :documentation "Per-network msgid store."))
  (if erc-v3--message-tags
      (add-hook 'erc-insert-pre-hook
                #'erc-v3--message-tags-add-message-id-text-prop nil t)
    (remove-hook 'erc-insert-pre-hook
                 #'erc-v3--message-tags-add-message-id-text-prop t)))

(defconst erc-v3--message-tags-tag-escapes
  '((";" . "\\:")
    (" " . "\\s")
    ("\\" . "\\\\")
    ("\r" . "\\r")
    ("\n" . "\\n")))

(defconst erc-v3--message-tags-tag-escaped-regexp
  (rx (or ?\; ?\  ?\\ ?\r ?\n)))

(defconst erc-v3--message-tags-tag-unescaped-regexp
  (rx (or "\\:" "\\s" "\\\\" "\\r" "\\n"
          (seq "\\" (or string-end (not (or ":" "s" "n" "r" "\\")))))))

;; XXX these operations are not inverses; unescaping may degenerate the
;; original by dropping stranded/misplaced backslashes

;; Unescaping like this is slightly slower than using a char table
;; keyed on the last elt (":" for "\\:") when running interpreted but
;; the same or slightly faster when compiled.

(defun erc-v3--message-tags-unescape-tag-value (str)
  "Undo substitution of char placeholders in raw tag value STR."
  (replace-regexp-in-string erc-v3--message-tags-tag-unescaped-regexp
                            (lambda (s)
                              (or (car (rassoc
                                        s erc-v3--message-tags-tag-escapes))
                                  (substring s 1)))
                            str t t))

(defun erc-v3--message-tags-escape-tag-value (str)
  "Swap out banned chars in tag value STR with message representation."
  (replace-regexp-in-string erc-v3--message-tags-tag-escaped-regexp
                            (lambda (s)
                              (cdr (assoc s erc-v3--message-tags-tag-escapes)))
                            str t t))

;; It might make sense to store the span ID alongside the string to
;; speed up scans.
(defun erc-v3--message-tags-add-message-id-text-prop (string)
  (when-let* ((tags (get-text-property 0 'tags string))
              (found (alist-get 'msgid tags))
              (table (erc-v3--message-tags-seen erc-v3--message-tags)))
    (puthash found found table)
    (puthash 'erc--msgid found erc--msg-props)))

(defun erc-v3--message-tags-locate-by-id (string)
  "Locate message for `msgid' STRING in current buffer."
  (text-property-any (point-min) erc-insert-marker 'erc--msgid
                     (gethash string (erc-v3--message-tags-seen
                                      erc-v3--message-tags))))

(cl-defgeneric erc--handle-message-tag (tag value parsed)
  "Handle a specific TAG with VALUE and `erc-response' object PARSED.")

(cl-defmethod erc--handle-message-tag (_ _ _) nil)

(cl-defmethod erc--handle-parsed-tags
  (parsed &context (erc-v3--message-tags erc-v3--message-tags))
  (pcase-dolist (`(,key . ,val) (erc-response.tags parsed))
    (erc--handle-message-tag key val parsed)))

;; We could specialize on &context (erc-v3-mode) but meh, i.e.,
;; require this file and you're stuck with this.
;;
;; > If a method object already exists that agrees with the new one on
;; > parameter specializers and qualifiers, the new method object
;; > replaces the old one. - CLHS
;;
;; TODO find out whether it's bad to do this (clobber).  If not, we
;; can lose the 'string' type specifier.

(cl-defmethod erc--parse-message-tags (raw &context (erc-v3-mode (eql t)))
  "Parse RAW string into an alist of cons cells of symbols and strings."
  (cl-assert (not (eq 'legacy erc-tags-format)))
  (let (tags)
    (dolist (part (split-string raw ";") (nreverse tags))
      (let ((pos (string-search "=" part)))
        ;; Clobber existing items.
        (setf (alist-get (intern (if pos (substring part 0 pos) part)) tags)
              (and-let* ((pos)
                         (value (substring part (1+ pos)))
                         ((not (string-empty-p value))))
                (erc-v3--message-tags-unescape-tag-value
                 (decode-coding-string value 'utf-8 t))))))))

(erc--define-zresponse (TAGMSG)
  :include erc--zstatused
  ( buffer (erc-get-buffer (erc--zstatused-target parsed) erc-server-process)
    :type buffer))

;; FIXME make this useful.
(define-erc-response-handler (TAGMSG)
  "Handle a TAGMSG command." nil
  (let ((msg (erc-response.contents parsed))
        (buffer (erc--zTAGMSG-buffer parsed)))
    (ignore parsed 'notice buffer msg)))

(defun erc-v3--merge-client-tags (tags)
  "Reassemble client TAGS in preparation for sending."
  (let (parts)
    (dolist (tag tags)
      (let* ((raw-value (cdr tag))
             (val (and raw-value (not (string-empty-p raw-value))
                       (erc-v3--message-tags-escape-tag-value raw-value))))
        (push (concat (symbol-name (car tag)) (and val "=") val) parts)))
    (encode-coding-string (concat "@" (string-join (nreverse parts) ";") " ")
                          'utf-8)))

(defvar erc-v3--unescaped-client-tags nil
  "Client tags for current outgoing request.")

(cl-defmethod erc--server-send
  (string force target &context (erc-v3--unescaped-client-tags cons))
  (setq string
        (concat (erc-v3--merge-client-tags erc-v3--unescaped-client-tags)
                string))
  (cl-call-next-method string force target))


;;;; Extension: server-time

;; For znc.in/server-time-iso, override `erc-stamp--current-time'.

(erc-v3--define-capability server-time
  (if erc-v3--server-time
      (progn
        (when (and erc--target (or (memq 'button erc-modules)
                                   (bound-and-true-p erc-button-mode)))
          (require 'erc-button)
          (add-hook 'erc-button--phantom-users-mode-hook
                    #'erc-v3--server-time-on-phantom-users-mode nil t))
        (add-function :before-until (local 'erc--current-time-pair-function)
                      #'erc-v3--current-time-pair '((depth . 40))))
    (remove-function (local 'erc--current-time-pair-function)
                     #'erc-v3--current-time-pair)
    (kill-local-variable 'erc-v3--server-time-display-latest)
    (remove-hook 'erc-button--phantom-users-mode-hook
                 #'erc-v3--server-time-on-phantom-users-mode t)))

(defvar-local erc-v3--server-time-display-latest nil)
(defvar erc-v3--server-time-display-nested-p nil)
(defvar erc-v3--server-time-pulse-late-arrivals-p (not noninteractive))

(defun erc-v3--find-insertion-point (p target-time)
  (while-let ((q (previous-single-property-change (1- p) 'erc--ts))
              (qq (erc--get-inserted-msg-beg q))
              (ts (get-text-property qq 'erc--ts))
              ((time-less-p target-time ts)))
    (setq p qq))
  p)

(defun erc-v3--server-time-pulse ()
  (pulse-momentary-highlight-region (point-min) (point-max)))

(cl-defmethod erc--insert-line
  (_ &context (erc-v3--server-time erc-v3--server-time))
  "Insert late-arriving messages up-buffer."
  (if-let* (((null erc--insert-marker))
            ((not (buffer-narrowed-p)))
            ((not erc-v3--server-time-display-nested-p))
            (current (erc--current-time-pair)) ; used in else form
            (last-seen erc-v3--server-time-display-latest)
            ((time-less-p current last-seen))
            (p (erc--get-inserted-msg-beg erc-insert-marker))
            (pt (erc-v3--find-insertion-point p current)))
      (let ((erc-v3--server-time-display-nested-p t)
            (erc-insert-post-hook
             (if (and erc-v3--server-time-pulse-late-arrivals-p
                      (eq (window-buffer) (current-buffer)))
                 `(,@erc-insert-post-hook erc-v3--server-time-pulse)
               erc-insert-post-hook)))
        (erc--with-spliced-insertion pt
          (cl-call-next-method)))
    (unless (or erc-v3--server-time-display-nested-p
                (erc--check-msg-prop 'erc--msg 'datestamp))
      (setq erc-v3--server-time-display-latest current))
    (cl-call-next-method)))

(defun erc-v3--current-time-pair ()
  "Return a lisp time stamp from time tag or call ORIG."
  (cond ((erc--zresponse-p erc--parsed-response)
         (erc--zresponse-time erc--parsed-response))
        (erc--parsed-response
         (erc--time-pair-from-time-tag erc--parsed-response))))

(defvar erc-button--phantom-users-mode)

(defun erc-v3--server-time-on-phantom-users-mode ()
  (if erc-button--phantom-users-mode
      (add-function :filter-return (local 'erc--cmem-from-nick-function)
                    #'erc-v3--server-time-update-phantom-cmem '((depth . 20)))
    (remove-function (local 'erc--cmem-from-nick-function)
                     #'erc-v3--server-time-update-phantom-cmem)))

(defun erc-v3--server-time-update-phantom-cmem (cmem)
  (when-let* ((cusr (cdr cmem))
              ((eq 'erc--phantom-channel-user (aref cusr 0))))
    (cl-assert (erc-v3--server-time-p erc-v3--server-time))
    (cl-assert (erc-response-p erc--parsed-response))
    (cl-assert (erc-response.tags erc--parsed-response))
    (setf (erc-channel-user-last-message-time cusr)
          (erc--current-time-pair)))
  cmem)


;;;; Extension: echo-message

(erc-v3--define-capability echo-message
  (if erc-v3--echo-message
      (progn
        (add-function :override (local 'erc--send-action-function)
                      #'erc--send-action-perform-ctcp)
        (add-function :around (local 'erc--send-message-nested-function)
                      #'erc-v3--echo-message-inhibit-insert-on-send-message)
        (add-hook 'erc--input-review-functions
                  #'erc-v3--echo-message-on-pre-send 75 t))
    (remove-function (local 'erc--send-action-function)
                     #'erc--send-action-perform-ctcp)
    (remove-function (local 'erc--send-message-nested-function)
                     #'erc-v3--echo-message-inhibit-insert-on-send-message)
    (remove-hook 'erc--input-review-functions
                 #'erc-v3--echo-message-on-pre-send t)))

(defun erc-v3--echo-message-on-pre-send (input)
  "Suppress insertion of `erc-input' object INPUT."
  (cl-assert erc-v3--echo-message)
  (setf (erc-input-insertp input) nil))

(cl-defmethod erc--send-message-external
  (line force &context (erc-v3--echo-message erc-v3--echo-message))
  (erc-message "PRIVMSG" (concat (erc-target) " " line) force)
  t)

;; Echoing of command lines for /SAY, /SV, etc. would normally be
;; suppressed on account of their being slash commands.  However,
;; since they handle their own insertion, additional steps are
;; required.  /ME is different because it uses `erc-display-message'
;; instead of `erc-display-msg', even though it's basically identical
;; to an outgoing chat message.

(defvar erc-v3--echo-message-allow-noncommands-p nil
  "Allow non-commands, such as /SAY, /SV, etc. to emit labeled messages.
This does not concern original, submitted command lines
themselves but rather replacement text those commands generate,
which is sent via nested calls to `erc--send-input-lines'.")

(defun erc-v3--echo-message-inhibit-insert-on-send-message (orig &rest args)
  (cl-assert erc-v3--echo-message)
  (if erc-v3--echo-message-allow-noncommands-p
      (apply orig args)
    (let ((erc-pre-send-functions (cons #'erc-v3--echo-message-on-pre-send
                                        erc-pre-send-functions)))
      (apply orig args))))


;;;; Extension: Monitor

(erc-v3--define-extension monitor
  :slots (( targets nil :type (list-of string)
            :documentation "Targets with active monitor jobs.")
          ( count 0 :type natnum
            :documentation "Number of monitored targets.")
          ( max 0 :type natnum
            :documentation "Max number of active monitor jobs."))
  (if erc--target
      (if erc-v3--monitor
          (progn
            (add-hook 'erc-connect-pre-hook #'erc-v3--monitor-on-new-query 40 t)
            (add-hook 'erc-kill-buffer-hook #'erc-v3--monitor-forget 40 t)
            (add-function :override (local 'erc--forget-server-user-function)
                          #'erc--forget-server-user))
        (remove-hook 'erc-connect-pre-hook #'erc-v3--monitor-on-new-query t)
        (remove-hook 'erc-kill-buffer-hook #'erc-v3--monitor-forget t)
        (remove-function (local 'erc--forget-server-user-function)
                         #'erc--forget-server-user))
    (when (erc--module-active-p 'querypoll)
      (erc-button--display-error-notice-with-keys
       (current-buffer)
       "The `monitor' extension obsoletes module `querypoll'."
       " Disabling the latter.")
      (when (fboundp 'erc-querypoll-mode)
        (erc-querypoll-mode -1)))))

;; Another way of implementing a capability that "extends" an
;; ISUPPORT-based extension, would be to formally depend on the
;; latter.  In this case, that's plain `monitor', which is normally
;; only discovered and enabled after negotiation ends.  This would
;; mean deferring via an `:early-init' and then adding a local
;; `erc-v3--monitor-mode-hook' member to mark the extension as wanted
;; and send a CAP REQ.  Probably not worth the added complexity.
(erc-v3--define-capability extended-monitor)

(cl-defmethod erc--server-005 :after
  (_proc _parsed &context (erc-v3-mode (eql t)))
  "Activate new WHOX session in server buffer."
  (when-let* ((param (erc--get-isupport-entry 'MONITOR))
              (val (or (and (cdr param) (string-to-number (cadr param)))
                       most-positive-fixnum)))
    (unless erc-v3--monitor
      (erc-v3--mark-extension-enabled 'monitor))
    (setf (erc-v3--monitor-max erc-v3--monitor) val)))

(cl-defmethod erc--queries-current-p
  (&context (erc-v3--monitor erc-v3--monitor))
  t)

;; FIXME make this an option.
(defvar erc-v3--monitor-exclude-regexp
  (rx bot (or "NickServ" "ChanServ" "MemoServ" "BouncerServ" "SaslServ") eot)
  "Pattern to ignore common services and bots you regularly query.")

(defun erc-v3--monitor-on-new-query (&rest _)
  (when (and erc-v3--monitor (erc-query-buffer-p))
    (let* ((target (erc-target))
           (nickd (erc-downcase target)))
      (unless (or (member nickd (erc-v3--monitor-targets erc-v3--monitor))
                  (string-match erc-v3--monitor-exclude-regexp target)
                  (not (erc-is-valid-nick-p target))
                  (erc-current-nick-p nickd)
                  (>= (erc-v3--monitor-count erc-v3--monitor)
                      (erc-v3--monitor-max erc-v3--monitor)))
        (push nickd (erc-v3--monitor-targets erc-v3--monitor))
        (cl-incf (erc-v3--monitor-count erc-v3--monitor))
        (erc-server-send (concat "MONITOR + " target) nil target)))))

(defun erc-v3--monitor-forget ()
  (when (erc-query-buffer-p)
    (let ((target (erc-target)))
      (when (and erc-server-connected
                 (not erc-networks--target-transplant-in-progress-p))
        (erc-server-send (concat "MONITOR - " target) nil target))
      (cl-decf (erc-v3--monitor-count erc-v3--monitor))
      (setf (erc-v3--monitor-targets erc-v3--monitor)
            (delete (erc-downcase target)
                    (erc-v3--monitor-targets erc-v3--monitor))))))

(defvar erc-message-english-s730 "%n is online"
  "English `erc-format-message' template for key `s730'.")

(defvar erc-message-english-s731 "%n is offline"
  "English `erc-format-message' template for key `s731'.")

(define-erc-response-handler (730) "RPL_MONONLINE." nil
  (dolist (added (split-string (erc-response.contents parsed) ","))
    (when-let* ((nuh (erc-parse-user added))
                (nick (car nuh))
                (nickd (erc-downcase nick))
                (buffer (erc-get-buffer nick erc-server-process)))
      (with-current-buffer buffer
        (cl-assert (member nickd (erc-v3--monitor-targets erc-v3--monitor)))
        (unless (erc-get-channel-member nick)
          (erc-update-current-channel-member nick nick t nil nil nil nil nil
                                             (nth 2 nuh) (nth 1 nuh))
          (unless (eq 's730 (erc--get-inserted-msg-prop 'erc--msg))
            (erc-display-message parsed 'notice (current-buffer)
                                 's730 ?n nick))))))
  nil)

(define-erc-response-handler (731) "RPL_MONOFFLINE." nil
  (dolist (removed (split-string (erc-response.contents parsed) ","))
    (if-let* ((nuh (erc-parse-user removed))
              (nick (car nuh))
              (nickd (erc-downcase nick))
              (buffer (erc-get-buffer nick erc-server-process)))
        (with-current-buffer buffer
          (when (and (member nickd (erc-v3--monitor-targets erc-v3--monitor))
                     (or (erc-get-channel-member nick)
                         ;; User is not in any other target buffer.
                         (null (erc-get-server-user nick))))
            (unless (eq 's731 (erc--get-inserted-msg-prop 'erc--msg))
              (erc-display-message parsed 'notice (current-buffer)
                                   's731 ?n nick))
            (erc-remove-current-channel-member nick)))
      (unless buffer
        (cl-assert nickd)
        (cl-decf (erc-v3--monitor-count erc-v3--monitor))
        (setf (erc-v3--monitor-targets erc-v3--monitor)
              (delete nickd (erc-v3--monitor-targets erc-v3--monitor))))))
  nil)


;;;; Extension: WHOX

;; This would seem pretty necessary for full account support, but it
;; stood apart for decades before becoming an official extension [1].
;; The older specs appear to be more extensive [2].  The quakenet one
;; says the "flags" field always contains all prefixes, as if
;; `multi-prefix' were always enabled:
;;
;; > flags will contain all the information about a user on a channel.
;; > @ for op'd, + for voiced, and ! for zombie.
;;
;; However, as of June 2024, there's some ambiguity as to whether this
;; should be the case when `multi-prefix' is disabled, whether by
;; omission or otherwise.  It's been reported that Solanum behaves as
;; if it's enabled whenever WHOX is advertised.
;;
;; [1] https://ircv3.net/specs/extensions/whox (official as of 2022)
;; [2] https://github.com/quakenet/snircd/raw/master/doc/readme.who
;;     https://faerion.sourceforge.net/doc/irc/whox.var
;;     https://ircu.sourceforge.net/release.2.10.01-who.html

(erc-v3--define-extension whox
  :slots (( token 0 :type natnum
            :documentation "Counter for generated tokens.")
          ( requests nil :type (list-of erc-v3--whox-request)
            :documentation "List of pending jobs.")
          ( req-seen nil :type list
            :documentation "Finished jobs removed from `requests'.")
          ( penalty 10 :type natnum
            :documentation "Back-off duration in seconds for delay timer.")
          ( timer nil :type (or null vector)
            :documentation "Delay timer."))
  (unless erc--target
    ;; When `erc-server-flood-queue' is empty, requesting WHO info on
    ;; JOIN is too early.
    (if erc-v3--whox
        (progn
          (add-hook 'erc-server-329-functions
                    #'erc-v3--request-who-after-mode 40 t)
          (add-hook 'erc-server-315-functions #'erc-v3--whox-handle-315 -20 t))
      (remove-hook 'erc-server-329-functions
                   #'erc-v3--request-who-after-mode t)
      (remove-hook 'erc-server-315-functions #'erc-v3--whox-handle-315 t))))

(cl-defstruct erc-v3--whox-request
  "WHOX request object."
  (token (cl-callf (lambda (v) (% (1+ v) 1024))
             (erc-v3--whox-token erc-v3--whox))
         :type natnum)
  (target (error "Required `:target' keyword missing") :type string)
  (fields (error "Required `:fields' keyword missing") :type string)
  (handler (error "Required `:handler' keyword missing") :type function))

(defun erc-v3--whox-delete (token)
  "Remove request for TOKEN from `requests' slot of `erc-v3--whox'."
  (setf (erc-v3--whox-requests erc-v3--whox)
        (cl-delete token (erc-v3--whox-requests erc-v3--whox)
                   :key #'erc-v3--whox-request-token)))

(defun erc-v3--whox-make-request (request)
  "Prepare a \"WHO\" REQUEST."
  (pcase-exhaustive request
    ((cl-struct erc-v3--whox-request token target fields)
     (cl-assert token)
     (setq fields (concat (and (/= ?t (aref fields 0)) "t") fields ","
                          (number-to-string token)))
     (erc-server-send (concat "WHO " target " %" fields) nil target))))

(defun erc-v3--whox-enqueue (target fields handler)
  "Schedule a \"WHO\" request for immediate or deferred sending."
  (let ((existing (erc-v3--whox-requests erc-v3--whox))
        (new (make-erc-v3--whox-request :target target
                                        :fields fields
                                        :handler handler)))
    (push new (erc-v3--whox-requests erc-v3--whox))
    (unless existing
      (erc-v3--whox-make-request new))))

(defvar erc-v3--whox-on-354-functions nil)

(defun erc-v3--whox-handle-channel-user (parsed parts)
  "Update channel user from WHOX reply with fields \"cuhnflar\"."
  ;; FIXME explain why `idle' is ignored here.
  (pcase-let* ((`(,channel ,login ,host ,nick ,flags ,_idle ,account) parts)
               (account (and (not (string= account "0")) account))
               (full-name (erc-response.contents parsed))
               (`(,away ,_oper (,status . ,_) ,_rest)
                (erc-v3--multi-prefix-split-flags flags)))
    (setq away (if (eq away ?G) "" nil))
    (erc-with-buffer (channel)
      (if-let* ((cmem (erc-get-channel-user nick)))
          (erc--update-current-channel-member
           cmem status nil nick host login full-name `(away . ,away) account)
        (erc--create-current-channel-member
         nick status nil nick host login full-name `(away . ,away) account))
      (run-hooks 'erc-v3--whox-on-354-functions))))

(defun erc-v3--request-who-after-mode (_ parsed)
  (when-let* ((channel (cadr (erc-response.command-args parsed))))
    (erc-with-buffer (channel)
      (erc-v3--whox-on-join)))
  nil)

;; FIXME reduce the amount of fields, especially those we just ignore.
(cl-defmethod erc-v3--whox-on-join ()
  "Request WHO info on \"JOIN\" for WHOX-aware target buffers.
Specify fields parameter as \"cuhnflar\" for channel, user, host, nick,
flags, idle, account, and real-name respectively."
  (erc-v3--whox-enqueue (erc-target) "cuhnflar"
                        #'erc-v3--whox-handle-channel-user))

;; Restrict WHOX to `erc-v3-mode' because this module performs
;; essential setup for account awareness.
(cl-defmethod erc--server-005 :after
  (_proc _parsed &context (erc-v3-mode (eql t)) (erc-v3--whox null))
  "Activate new WHOX session in server buffer."
  (when (erc--get-isupport-entry 'WHOX)
    (erc-v3--mark-extension-enabled 'whox)))

(define-erc-response-handler (354) "WHOX RPL_WHOSPCRPL." nil)

(cl-defmethod erc--server-354
  (_proc parsed &context (erc-v3--whox erc-v3--whox))
  (pcase-let*
      ;; The CAR is normally our current nick.
      ((`(,_ ,token . ,parts) (erc-response.command-args parsed))
       (token (and token
                   (string-match-p (rx bot (+ (any "0-9")) eot) token)
                   (string-to-number token)))
       (seen (cl-find token (erc-v3--whox-req-seen erc-v3--whox)
                      :key #'erc-v3--whox-request-token))
       (new (or seen (cl-find token (erc-v3--whox-requests erc-v3--whox)
                              :key #'erc-v3--whox-request-token))))
    ;; At the very least, server sends one 354 with your info.
    (unless seen
      (push new (erc-v3--whox-req-seen erc-v3--whox))
      (setf (erc-v3--whox-requests erc-v3--whox)
            (delq new (erc-v3--whox-requests erc-v3--whox))))
    (funcall (erc-v3--whox-request-handler new) parsed parts))
  nil)

(defun erc-v3--whox-request-delayed (buffer)
  (with-current-buffer buffer
    (setf (erc-v3--whox-timer erc-v3--whox) nil)
    (cl-assert (erc-v3--whox-requests erc-v3--whox))
    (when (erc-server-process-alive buffer)
      (erc-v3--whox-make-request
       (car (last (erc-v3--whox-requests erc-v3--whox)))))))

;; We can't override with an internal method this because it would
;; also run on 318, etc.
(defun erc-v3--whox-handle-315 (_proc parsed )
  (pcase-let* ((`(,_ ,target . ,_) (erc-response.command-args parsed))
               (found (cl-find target (erc-v3--whox-req-seen erc-v3--whox)
                               :key #'erc-v3--whox-request-target
                               :test #'string=)))
    (when (erc-v3--whox-requests erc-v3--whox)
      (if found
          (erc-v3--whox-make-request
           (cl-find (1+ (erc-v3--whox-request-token found))
                    (erc-v3--whox-requests erc-v3--whox)
                    :key #'erc-v3--whox-request-token))
        ;; We could spin for a bit here, but that could hide an actual
        ;; concurrency problem.  Better to know if the user did
        ;; something strange, like quit immediately upon joining a
        ;; channel.
        ;;
        ;; Seems `target' is an asterisk if we've just received a 263.
        (unless (erc-v3--whox-timer erc-v3--whox)
          (erc-button--display-error-notice-with-keys
           (erc-server-buffer)
           "Missing responses for " target " %S. Please report."
           (list :seen (erc-v3--whox-req-seen erc-v3--whox)
                 :reqs (erc-v3--whox-requests erc-v3--whox)))))))
  nil)

(defvar erc-message-english-s263-rate-limit
  "Rate limit exceeded retrieving user data for %t. Next attempt in %us."
  "English `erc-format-message' template for key `s263-rate-limit'.")

(cl-defmethod erc--server-263 (_proc parsed
                                     &context (erc-v3--whox erc-v3--whox))
  (pcase (erc-response.command-args parsed)
    ((and `(,_ "WHO" ,_)
          (let `(,oldest . ,_) (last (erc-v3--whox-requests erc-v3--whox))))
     (erc-display-message parsed 'notice (current-buffer) 's263-rate-limit
                          ?t (erc-v3--whox-request-target oldest)
                          ?u (erc-v3--whox-penalty erc-v3--whox))
     (when (and (null (erc-v3--whox-timer erc-v3--whox))
                (erc-v3--whox-requests erc-v3--whox))
       (setf (erc-v3--whox-timer erc-v3--whox)
             (run-at-time (erc-v3--whox-penalty erc-v3--whox) nil
                          #'erc-v3--whox-request-delayed
                          (current-buffer))
             ;;
             (erc-v3--whox-penalty erc-v3--whox)
             (* 2 (erc-v3--whox-penalty erc-v3--whox))))))
  nil)


;;;; Extension: extended-join

(erc-v3--define-capability extended-join
  (if erc-v3--extended-join
      (when erc--target
        (add-hook 'erc-join-hook #'erc-v3--send-mode-on-join 30 t))
    (remove-hook 'erc-join-hook #'erc-v3--send-mode-on-join t)))

(defvar erc-message-english-JOIN-X "%n (%a: %u@%h) has joined channel %c"
  "English `erc-format-message' template for key `JOIN-X'.")

;; Move "MODE #chan" to a hook so modules like chathistory car process
;; JOINs for the client's nick.
(defun erc-v3--send-mode-on-join ()
  (erc-server-send (concat "MODE " (erc-target))))

(defvar erc-v3--create-unknown-channel-buffer-when-others-join-p nil
  "When non-nil, create a buffer for unknown targets joined by others.")

(cl-defmethod erc--server-JOIN
  (_ parsed &context (erc-v3--extended-join erc-v3--extended-join))
  "Handle a JOIN command with extra arguments.
Unlike `erc-server-JOIN', do nothing when the buffer has already been
joined.  Also, don't bother with `erc-channel-begin-receiving-names'
because the default handler for \"353\" calls it when needed, and
extensions may prevent names from automatically being sent altogether."
  (pcase-let*
      ((`(,chnl ,account ,full) (erc-response.command-args parsed))
       (`(,nick ,login ,host) (erc--zJOIN-nuh parsed))
       (account (unless (string= account "*") account))
       (nickp (erc--zJOIN-self-p parsed))
       (buffer (erc--zJOIN-buffer parsed)))
    (when (and (null buffer)
               erc-v3--create-unknown-channel-buffer-when-others-join-p
               (erc-channel-p chnl))
      (setq buffer (save-excursion (erc--open-target chnl))))
    (when buffer
      (with-current-buffer buffer
        (unless (and nickp (erc--target-channel-joined-p erc--target))
          (erc-update-current-channel-member nick nick t nil nil nil nil nil
                                             host login full nil nil account)
          (when nickp
            (setf (erc--target-channel-joined-p erc--target) t)
            (erc-update-mode-line)
            (run-hooks 'erc-join-hook))
          (let ((args (cond
                       (nickp (list 'JOIN-you ?c chnl))
                       (account (list 'JOIN-X ?a account
                                      ?n nick ?u login ?h host ?c chnl))
                       (t (list 'JOIN ?n nick ?u login ?h host ?c chnl)))))
            (apply #'erc-display-message parsed 'notice buffer args))))))
  nil)


;;;; Extension: account-tag

;; Some servers send `account-tag' whether you ask for it or not,
;; courtesy of `message-tags'.  Assuming `extended-join' and `whox'
;; are provided, negotiating `account-tags' may only buy you account
;; knowledge for queries from those you don't share a channel with.
;; It's unclear whether this also helps in chat history batches, since
;; they arrive before channel membership is announced and contain
;; speakers who've since left.

(erc-v3--define-capability account-tag)

(cl-defmethod erc--handle-message-tag
  ((_ (eql account)) account parsed
   &context (erc-v3--account-tag erc-v3--account-tag))
  (pcase-let ((`(,nick ,login ,host)
               (erc-parse-user (erc-response.sender parsed))))
    (erc-v3--update-account account nick login host)))


;;;; Extension: account-notify

(erc-v3--define-capability account-notify)

(defvar erc-message-english-ACCOUNT "Account status for %a: %s as %n"
  "English `erc-format-message' template for key `ACCOUNT'.")

;; When a user logs out of an account, all messages from that nick
;; within currently lose their association to that user.  This is
;; likely not ideal.

(defun erc-v3--update-account (account nick &optional login host)
  "Update ACCOUNT field for NICK's `erc-server-user' object."
  (cl-assert (not (string-empty-p account)))
  (if-let* ((user (erc-get-server-user nick)))
      (erc-update-user user nil nil nil nil nil account)
    ;; `erc-update-current-channel-member' will still honor an ADD
    ;; argument so long as nothing yet exists in `erc-channel-users'.
    (when (string= account "*")
      (setq account nil))
    (erc-add-server-user nick (make-erc-server-user :nickname nick
                                                    :host host
                                                    :login login
                                                    :account account))))

(define-erc-response-handler (ACCOUNT)
  "Handle an ACCOUNT message." nil
  (pcase-let* ((account (car (erc-response.command-args parsed)))
               (`(,nick ,login ,host)
                (erc-parse-user (erc-response.sender parsed))))
    (erc-v3--update-account account nick login host)
    (when (erc-current-nick-p nick)
      (erc-display-message parsed 'notice (current-buffer)
                           'ACCOUNT ?a account ?n nick ?s "logged in"))))


;;;; Extension: away-notify

(erc-v3--define-capability away-notify)

(define-erc-response-handler (AWAY)
  "Handle an AWAY message.
See also RPL_AWAY 301." nil
  (pcase-let* ((msg (erc-response.contents parsed))
               (`(,nick ,_login ,_host)
                (erc-parse-user (erc-response.sender parsed))))
    (when (string-empty-p msg)
      (setq msg nil))
    (erc-update-user-nick nick nil nil nil nil (cons 'away msg) nil))
  nil)


;;;; Extension: chghost

(erc-v3--define-capability chghost)

(define-erc-response-handler (CHGHOST)
  "Handle a CHGHOST message." nil
  (pcase-let* ((`(,login ,host) (erc-response.command-args parsed))
               (nick (erc-extract-nick (erc-response.sender parsed))))
    (erc-v3--update-server-user nick host nil login nil nil))
  nil)


;;;; Extension: standard-replies

;; ERC handles arbitrary (unknown) replies regardless of whether the
;; capability has been negotiated because servers can send them
;; regardless.  https://github.com/ircv3/ircv3-specifications/pull/506

(erc-v3--define-capability standard-replies)

(defvar erc-message-english-FAIL "%c command failed. %m: %n"
  "English `erc-format-message' template for key `FAIL'.")

(defvar erc-message-english-WARN "Warning re %c command. %m: %n"
  "English `erc-format-message' template for key `WARN'.")

(defvar erc-message-english-NOTE "Note re %c command. %m: %n"
  "English `erc-format-message' template for key `NOTE'.")

(cl-defgeneric erc-v3--standard-replies-handle (type command code rest parsed)
  "Handle a `standard-replies' message of type TYPE.
Expect CODE to be an error code defined in an IRCv3 spec, for example,
\"MULTILINE_INVALID\" from `draft/multiline'.  Expect COMMAND to be the
offending command originally sent by ERC.  Expect REST to be the
remaining args and PARSED to be an `erc-repsonse' object.")

(cl-defmethod erc-v3--standard-replies-handle (type command code rest parsed)
  "Emit a `standard-replies' catch-all message to server buffer."
  (let ((msg-type (pcase type
                    ('NOTE 'notice)
                    ('WARN '(notice error))
                    ('FAIL '(t notice error)))))
    (erc-display-message parsed msg-type (erc-server-buffer)
                         type ?c command ?m code
                         ?n (string-join rest " "))))

(defun erc-v3--standard-replies-dispatch (parsed)
  (pcase-let* ((`(,maybe-cmd ,code . ,rest) (erc-response.command-args parsed))
               (command (and (not (equal "*" maybe-cmd)) (intern maybe-cmd)))
               (code (intern code))
               (type (intern (erc-response.command parsed))))
    (erc-v3--standard-replies-handle type command code rest parsed)))

(define-erc-response-handler (FAIL)
  "Handle a FAIL `standard-replies' message." nil
  (erc-v3--standard-replies-dispatch parsed))

(define-erc-response-handler (WARN)
  "Handle a WARN `standard-replies' message." nil
  (erc-v3--standard-replies-dispatch parsed))

(define-erc-response-handler (NOTE)
  "Handle a NOTE `standard-replies' message." nil
  (erc-v3--standard-replies-dispatch parsed))


;;;; Extension: setname

;; FIXME set `max' from NAMELEN.
(erc-v3--define-capability setname
  :slots ((max nil :type (or null integer))))

(defvar erc-message-english-name-too-long
  "Full-name length (%i) exceeds NAMELEN (%l) defined by the server."
  "English `erc-format-message' template for overlong full name.")

(defun erc-cmd-SETNAME (full-name)
  "Tell server to set client's full name to FULL-NAME."
  (when-let* ((max (erc-v3--setname-max erc-v3--setname))
              (current (length full-name))
              ((string> current max)))
    (erc-display-message nil 'notice 'active 'name-too-long current max))
  (erc-server-send (concat "SETNAME :" (string-remove-prefix " " full-name))))
(put 'erc-cmd-SETNAME 'do-not-parse-args t)

(define-erc-response-handler (SETNAME)
  "Handle a \"SETNAME\" message." nil
  (pcase-let ((full-name (erc-response.contents parsed))
              (`(,nick ,login ,host)
               (erc-parse-user (erc-response.sender parsed))))
    (erc-v3--update-server-user nick host full-name login nil nil))
  nil)


;;;; Extension: sasl

;; The following is glue to adapt the `sasl' module to our `erc-v3'
;; extension framework.  Note that there's no need to add `sasl' to
;; `erc-modules' because this extension manages it for you.

(declare-function erc-sasl--destroy "erc-sasl" (proc))
(declare-function erc-sasl--mechanism-offered-p "erc-sasl" (offered))
(declare-function erc-sasl--state-client "erc-sasl" (cl-x))
(declare-function erc-sasl-mode "erc-sasl" (&optional arg))
(declare-function sasl-client-mechanism "sasl" (client))
(declare-function sasl-mechanism-name "sasl" (mechanism))
(defvar erc-sasl--options)
(defvar erc-sasl-mode)

(erc-v3--define-capability sasl
  :enablep #'erc-v3--sasl-initiate
  :early-init #'erc-v3--sasl-on-early-init
  (cl-assert (featurep 'erc-sasl))
  (unless erc-v3--sasl
    (remove-hook 'erc-sasl-mode-hook #'erc-v3--sasl-on-minor-mode t)
    (erc-sasl-mode -1)))

(defun erc-v3--sasl-on-extension-manifest-review ()
  "Ensure `sasl' present in session's copy of `erc-v3-extensions'."
  (when (or (bound-and-true-p erc-sasl-mode)
            (and (memq 'sasl erc-modules) (require 'erc-sasl)))
    (cl-pushnew 'sasl erc-v3-extensions)))

(defun erc-v3--sasl-on-minor-mode ()
  "Inhibit CL methods specializing on `erc-sasl-mode' `eql'ing t."
  (when erc-sasl-mode
    (setq erc-sasl-mode 'v3)))

(defun erc-v3--sasl-on-early-init (&rest plist)
  "Manage dependent module `sasl'."
  (cl-assert (not erc--target))
  (add-hook 'erc-sasl-mode-hook #'erc-v3--sasl-on-minor-mode nil t)
  (require 'erc-sasl)
  (cond ((erc-v3--sasl-on-minor-mode))
        ((memq 'sasl erc-modules))
        (t (erc-sasl-mode +1)))
  (apply #'erc-v3--sasl plist))

(cl-defmethod erc-v3--subcmd-LS ((cap erc-v3--sasl))
  (cl-assert (not erc--target))
  (when (and (eq erc-v3--step-OFFERED (erc-v3--extension-step cap))
             (erc-v3--capability-wantedp cap))
    (cl-assert erc-sasl-mode)
    (let ((val (erc-v3--capability-val cap)))
      (unless (or (null val) (erc-sasl--mechanism-offered-p val))
        (erc-button--display-error-notice-with-keys
         "Chosen SASL mechanism "
         (upcase (symbol-name (alist-get 'mechanism erc-sasl--options)))
         " not among " val ". Aborting.")
        (setf (erc-v3--capability-wantedp cap) nil)
        (erc-v3--mark-extension-disabled 'sasl)
        (erc-sasl--destroy erc-server-process)))))

(cl-defmethod erc-v3--subcmd-ACK
  ((_ erc-v3--sasl) &context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  ;; Extend timer by 10 seconds.
  (timer-inc-time (erc-v3--caps-timer erc-v3--caps) 10))

(cl-defmethod erc--register-connection
  :around (&context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  (cl-assert (memq 'sasl (erc-v3--caps-extensions erc-v3--caps)))
  (let ((erc-session-password
         (and erc-session-password
              (not (eq :password (alist-get 'password erc-sasl--options)))
              erc-session-password))
        (erc-session-username
         ;; The SASL username may contain a colon or a space.
         (if (eq :user (alist-get 'user erc-sasl--options))
             (erc-current-nick)
           erc-session-username)))
    (cl-call-next-method)))

(defun erc-v3--sasl-initiate (cap)
  "Initialize a new `erc-v3--sasl' session.
Roll back the state of the extension object to prevent the
finalizer from running."
  (cl-assert (eq erc-v3--step-ANSWERED (erc-v3--extension-step cap)))
  (defvar erc-sasl--state)
  (let ((client (erc-sasl--state-client erc-sasl--state)))
    ;; This isn't a hack; it's what `enablep' exists for.
    (setf (erc-v3--extension-step cap) erc-v3--step-REQUESTED)
    ;; When mechanism is unknown this fails with a wrong-type error.
    (let ((m (sasl-mechanism-name (sasl-client-mechanism client))))
      (erc-server-send (format "AUTHENTICATE %s" m))))
  nil)

(cl-defmethod erc--server-903
  (_ _ &context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  "Call default finalizer to conclude capability negotiation."
  (erc-v3--mark-extension-enabled 'sasl)
  (erc-v3--server-CAP-finish)
  nil)

(cl-defmethod erc--server-902
  (_ _ &context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  (erc-v3--mark-extension-disabled 'sasl)
  (cl-call-next-method))

(cl-defmethod erc--server-904
  (_ _ &context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  (erc-v3--mark-extension-disabled 'sasl)
  (cl-call-next-method))

(cl-defmethod erc--server-908
  (_ _ &context ((bound-and-true-p erc-sasl-mode) (eql v3)))
  (erc-v3--mark-extension-disabled 'sasl)
  (cl-call-next-method))


;;;; Extension-agnostic facilities

;; This first set of handlers and utilities provide smart(er)
;; collecting of nicknames, using `multi-prefix' and
;; `userhost-in-names', when applicable.

;; FIXME make this a user option
(defvar erc-v3--353-names-message-size 512)

(defvar erc-message-english-s353-v3 "Users on %c (%d): %u"
  "English `erc-format-message' template for key `s353-v3'.")

(defvar-local erc-v3--353-work nil
  "Target-local state for processing large name dumps in chunks.")

(cl-defstruct erc-v3--353-work
  "Pending state while receiving names."
  disp count names self)

(cl-defmethod erc--partition-prefixed-names (name &context
                                                  (erc-v3-mode (eql t)))
  "Return a list of (STATUS NICK LOGIN HOST).
Act like the base method, except handle multiple prefix chars
before NAME, and perform the side effect of populating the
`names' slot of `erc-v3--353-work'."
  (pcase-let*
      ((statuses (erc--parsed-prefix-statuses (erc--parsed-prefix)))
       (`(,status ,pfx ,nuh) (erc-v3--multi-prefix--partition name
                                                              statuses))
       (`(,nick ,login ,host) (erc--parse-nuh nuh)))
    (when (and (or nick ; this macro returns nick
                   (cl-multiple-value-setq (nick login host)
                     (erc--shuffle-nuh-nickward nick login host))))
      ;; Add to list.
      (push (concat pfx nick) (erc-v3--353-work-names erc-v3--353-work))
      `(,status ,nick ,login ,host))))

(declare-function erc-button--deemphasize-plebs "erc-button" (nick-object))

(defun erc-v3--print-names (parsed channel names disp)
  (add-function :filter-return (local 'erc-button--modify-nick-function)
                #'erc-button--deemphasize-plebs '((depth . 95)))
  ;; Don't bother with case tables since this is only for show.
  (let ((sort-folded  (lambda (a b) (string< (downcase a) (downcase b)))))
    ;; Since we wait to print 353s in one go (on 366), spoof the
    ;; former command so users who ignore it can opt out.
    (unless (equal (erc-response.command parsed) "353")
      (setf parsed (copy-erc-response parsed)
            (erc-response.command parsed) "353"))
    (erc-display-message parsed 'notice (current-buffer)
                         's353-v3
                         ?c channel
                         ?d (propertize "   " 'display disp)
                         ?u (string-join (sort names sort-folded) " ")))
  (remove-function (local 'erc-button--modify-nick-function)
                   #'erc-button--deemphasize-plebs))

(cl-defmethod erc--server-353 (proc parsed &context (erc-v3-mode (eql t)))
  "Override 353 default handler for RPL_NAMREPLY, processing PARSED for PROC.
Insert entries in buffer, ensuring correct buttonizing."
  ;; The first arg may be an asterisk instead of the current nick.
  (let ((channel (nth 2 (erc-response.command-args parsed)))
        (users (erc-response.contents parsed)))
    (erc-with-buffer (channel proc)
      (unless erc-v3--353-work
        (setq erc-v3--353-work
              (make-erc-v3--353-work :count 0 :disp (list "0")))
        (setf (erc-v3--353-work-self erc-v3--353-work)
              (erc-get-channel-user (erc-current-nick))))
      (erc-channel-receive-names users)
      (when (> (- (hash-table-count erc-channel-users)
                  (erc-v3--353-work-count erc-v3--353-work))
               erc-v3--353-names-message-size)
        (erc-v3--print-names parsed channel
                             (erc-v3--353-work-names erc-v3--353-work)
                             (erc-v3--353-work-disp erc-v3--353-work))
        (setf (erc-v3--353-work-names erc-v3--353-work) nil
              ;;
              (erc-v3--353-work-count erc-v3--353-work)
              (hash-table-count erc-channel-users)))
      ;; Mutate value in place
      (setf (car (erc-v3--353-work-disp erc-v3--353-work))
            (format "%d" (hash-table-count erc-channel-users)))))
  nil)

;; TODO find out why `erc-channel-end-receiving-names' treats
;; `erc-channel-new-member-names' like a subset of
;; `erc-channel-users'. Aren't they equal (for initial NAMES)?

(defun erc-v3--bake-display-count (beg end val)
  "Find regions with a `display' property `eq' to VAL from BEG to END.
Replace each with the car of VAL.  Expect VAL to be a list containing a
single string."
  (let ((pos beg)
        (inhibit-read-only t)
        (inhibit-field-text-motion t))
    (save-excursion
      (while (setq pos (text-property-any pos end 'display val))
        (goto-char pos)
        (delete-region (point) (+ 3 (point)))
        (insert-and-inherit (car val))
        (setq pos (point))))))

(cl-defmethod erc--server-366 (proc parsed &context (erc-v3-mode (eql t)))
  "Perform same procedure as `erc-channel-begin-receiving-names'."
  ;; :test.libera.chat 366 someNick #emacs :End of /NAMES list
  ;; FIXME use `erc--skip' for this instead.
  (defvar erc-track-exclude)
  (let* ((channel (cadr (erc-response.command-args parsed)))
         (erc-track-exclude (list channel)))
    (erc-with-buffer (channel proc) ; this downcases
      (setf (erc-v3--353-work-count erc-v3--353-work)
            (hash-table-count erc-channel-users))
      (erc-v3--print-names parsed channel
                           (erc-v3--353-work-names erc-v3--353-work)
                           (erc-v3--353-work-disp erc-v3--353-work))
      ;; FIXME leave a marker instead of scanning the entire buffer.
      (erc-v3--bake-display-count (point-min) erc-insert-marker
                                  (erc-v3--353-work-disp erc-v3--353-work))
      (setq erc-v3--353-work nil)))
  (cl-call-next-method))

;; These last two handlers are account aware.

(cl-defmethod erc--server-900 (_proc parsed &context (erc-v3-mode (eql t)))
  "Mark this session's user as being logged in."
  (pcase-let* ((`(,nick ,userhost ,account)
                (erc-response.command-args parsed))
               (`(,uh-nick ,login ,host)
                (when (and userhost
                           (not (string= userhost "*")))
                  (erc-parse-user userhost))))
    ;; Servers may (be configured to) ignore the first two fields.
    (when (or (not nick) (string= nick "*"))
      (setq nick (or uh-nick (erc-current-nick))))
    (cl-assert nick) ; erase me
    (erc-v3--update-account account nick login host)
    (erc-display-message parsed 'notice (current-buffer)
                         (erc-response.contents parsed)))
  nil)

(define-erc-response-handler (901)
  "Handle \"RPL_LOGGEDOUT\" server command." nil
  ;; Some servers send * for target (first arg).
  (pcase-let* ((`(,nick ,userhost) (erc-response.command-args parsed))
               (`(,uh-nick ,login ,host)
                (when (and userhost (not (string= userhost "*")))
                  (erc-parse-user userhost)))
               (account))
    (when (or (not nick) (string= nick "*"))
      (setq nick (or uh-nick (erc-current-nick))))
    (cl-assert nick) ; erase me
    (setq account (erc-server-user-account (erc-get-server-user nick)))
    (erc-v3--update-account "*" nick login host)
    (erc-display-message parsed 'notice (current-buffer)
                         'ACCOUNT ?a account ?n nick ?s "logged out")))


;;;; Helpers

(defun erc-v3--cap-to-slot-values (object convert-fn)
  "Assign k/v pairs in OBJECT's `erc-v3--capability-val' to other slots.
Call CONVERT-FN with key as a symbol and value as a string or nil."
  (let ((class (get (erc-v3--extension-canon object)
                    'erc-v3--extension-class)))
    (cl-assert (eq class (aref object 0)))
    (dolist (pair (split-string (erc-v3--capability-val object) ","))
      (let* ((found (string-search "=" pair))
             (key (intern (substring pair 0 found))))
        (setf (cl-struct-slot-value class key object)
              (funcall convert-fn key
                       (and found (substring pair (1+ found))))))))
  object)

(defun erc-v3--format-prefix (cusr &optional lettersp)
  "When applicable, return string of membership prefixes or letters.
Expect CUSR to be an `erc-channel-user' object.  With LETTERSP,
return letters.  Add `help-echo' descriptions to all characters."
  (and (not (zerop (erc-channel-user-status cusr)))
       (let ((alist (erc--parsed-prefix-alist (erc--parsed-prefix))))
         (concat
          (and (erc-channel-user-owner cusr)
               (propertize (if lettersp "q" (string (alist-get ?q alist)))
                           'help-echo "owner"))
          (and (erc-channel-user-admin cusr)
               (propertize (if lettersp "a" (string (alist-get ?a alist)))
                           'help-echo "admin"))
          (and (erc-channel-user-op cusr)
               (propertize (if lettersp "o" (string (alist-get ?o alist)))
                           'help-echo "op"))
          (and (erc-channel-user-halfop cusr)
               (propertize (if lettersp "h" (string (alist-get ?h alist)))
                           'help-echo "half-op"))
          (and (erc-channel-user-voice cusr)
               (propertize (if lettersp "v" (string (alist-get ?v alist)))
                           'help-echo "voice"))))))

(defun erc-v3--assemble-server-user-info-string (user)
  "Format an informative string to display in the echo area.
This is basically `erc-nick-at-point' with some extra v3
nonsense."
  (let* ((away-msg (erc--server-user-away-p user))
         ;; Some users don't have a userhost
         (parts `(,@(when-let* ((login (erc-server-user-login user))
                                (host (erc-server-user-host user)))
                      (list login "@" host))
                  ,(when-let* ((full-name (erc-server-user-full-name user))
                               ((not (string-empty-p full-name))))
                     (format " %S" full-name))
                  ,(when away-msg " G")
                  ,(when away-msg
                     (unless (string-empty-p away-msg)
                       (format ": %s" away-msg))))))
    (apply #'concat parts)))

(defun erc-v3--warn-about-edge-migration ()
  (unless (or (and (fboundp 'ert-running-test) (ert-running-test))
              (null (string-search "snapshot49860" erc-version))
              erc--target)
    (erc-button--display-error-notice-with-keys
     (current-buffer)
     "\C-bWARNING\C-b: The ERC devel package erc-49860 is changing."
     " It will soon only contain basic IRCv3 functionality and extensions."
     " Its PoC features will be moved to a new package: erc-edge."
     " If migrating, uninstall all versions of erc-49860.")))

(add-hook 'erc-v3-mode-hook #'erc-v3--warn-about-edge-migration)

(provide 'erc-v3)

;;; erc-v3.el ends here
;;
;; Local Variables:
;; generated-autoload-file: "erc-loaddefs.el"
;; End:
