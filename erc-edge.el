;;; erc-edge.el --- An Emacs Internet Relay Chat client  -*- lexical-binding:t -*-

;; Copyright (C) 1997-2025 Free Software Foundation, Inc.

;; Author: Alexander L. Belikoff <alexander@belikoff.net>
;; Maintainer: Amin Bandali <bandali@gnu.org>, F. Jason Park <jp@neverwas.me>
;; Contributors: Sergey Berezin (sergey.berezin@cs.cmu.edu),
;;               Mario Lang (mlang@delysid.org),
;;               Alex Schroeder (alex@gnu.org)
;;               Andreas Fuchs (afs@void.at)
;;               Gergely Nagy (algernon@midgard.debian.net)
;;               David Edmondson (dme@dme.org)
;;               Michael Olson (mwolson@gnu.org)
;;               Kelvin White (kwhite@gnu.org)
;; Version: 5.7alpha0.20250312.11848
;; Package-Requires: ((emacs "27.1") (compat "29.1.4.5"))
;; Keywords: IRC, chat, client, Internet
;; URL: https://www.gnu.org/software/emacs/erc.html

;; This is a GNU ELPA :core package.  Avoid functionality that is not
;; compatible with the version of Emacs recorded above.

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; ERC is a powerful, modular, and extensible IRC client for Emacs.
;; For more information, visit the ERC page at
;; <https://www.gnu.org/software/emacs/erc.html>.

;; Configuration:

;; Use M-x customize-group RET erc RET to get an overview
;; of all the variables you can tweak.

;; Usage:

;; To connect to an IRC server, do
;;
;; M-x erc RET
;;
;; or
;;
;; M-x erc-tls RET
;;
;; to connect over TLS (encrypted).  Once you are connected to a
;; server, you can use C-h m or have a look at the ERC menu.

;;; Code:
;;; erc-edge.el ends here
